slopes =zeros(2,7);
% ColOrd = get(gca,'ColorOrder')
ColOrd = [0 0 0
          0 0 1.0000
          1.0000 0 0
          0 1.0000 0
          .7 .5 0 
          1.0000 0.1034 0.7241
          1.0000 0.8276 0
          0 0.3448 0
         ];
[m,n] = size(ColOrd);

figure(1)
clf
for l = 1:2
    for i = 1:7
        if(l == 1)
            a = dir(sprintf(['Cerr-g-*-q-%i-%i.txt'],[i+1 i]));
        end
        if(l == 2)
            a = dir(sprintf(['err-g-*-q-%i-%i.txt'],[i+1 i]));
        end
        k = i;
        ColRow = rem(k,m);
        if ColRow == 0
            ColRow = m;
        end
        % Get the color
        Col = ColOrd(ColRow,:);
        U = [];
        for j = 1:length(a)
            u = load(a(j).name);
            U = [U; [u(end,7) u(end,3) u(end,8)]];
        end
        U2 = sort(U,1);
        h = U2(:,3);
        if(l == 1)
            loglog(h,U2(:,2),'d--','color',Col,'linewidth',2)
        else
            loglog(h,U2(:,2),'*-','color',Col,'linewidth',2)
        end
        hold on
        c = [ones(size(h)) log(h)]\log(U2(:,2));
        slopes(l,i) = c(2);
    end
end

set(gca,'fontsize',18)
legend('C21','C32','C43','C54','C65','C76','C87','U21','U32','U43','U54','U65','U76','U87','Location','NorthWest')
xlabel('XXXXX')
ylabel('YYYYY')
axis([0.1 3 1e-13 0.3])

disp(['\begin{table}[]'])
disp(['\caption{Estimated rates of convergence for Central and ' ...
      'upwind flux using $q_u = q_v+1$.  \label{Tab:Stoneley_Wilcox_diff_order}}'])
disp(['\begin{center}'])
disp(['\begin{tabular}{|l|c|c|c|c|c|c|c|c|}'])
disp(['\hline'])
disp(sprintf(['$q_u$ & %i    & %i    & %i    & %i    & %i    & %i    & %i \\\\ '],2:8))
disp(sprintf(['C     & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f \\\\ '],slopes(1,:)))
disp(sprintf(['U     & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f \\\\ '],slopes(2,:)))
disp(['\hline'])
disp(['\end{tabular}'])
disp(['\end{center}'])
disp(['\end{table} '])

figure(2)
clf
for l = 1:2
    for i = 1:7
        if(l == 1)
            a = dir(sprintf(['Cerr-g-*-q-%i-%i.txt'],[i+1 i+1]));
        end
        if(l == 2)
            a = dir(sprintf(['err-g-*-q-%i-%i.txt'],[i+1 i+1]));
        end
        k = i;
        ColRow = rem(k,m);
        if ColRow == 0
            ColRow = m;
        end
        % Get the color
        Col = ColOrd(ColRow,:);
        U = [];
        for j = 1:length(a)
            u = load(a(j).name);
            U = [U; [u(end,7) u(end,3) u(end,8)]];
        end
        U2 = sort(U,1);
        h = U2(:,3);
        if(l == 1)
            loglog(h,U2(:,2),'d--','color',Col,'linewidth',2)
        else
            loglog(h,U2(:,2),'*-','color',Col,'linewidth',2)
        end
        hold on
        c = [ones(size(h)) log(h)]\log(U2(:,2));
        slopes(l,i) = c(2);
    end
end
set(gca,'fontsize',18)
legend('C22','C33','C44','C55','C66','C77','C88','U22','U33','U44','U55','U66','U77','U88','Location','NorthWest')
xlabel('XXXXX')
ylabel('YYYYY')
axis([0.1 3 1e-13 0.3])

disp(['\begin{table}[]'])
disp(['\caption{Estimated rates of convergence for Central and ' ...
      'upwind flux using $q_u = q_v$.  \label{Tab:Stoneley_Wilcox_diff_order}}'])
disp(['\begin{center}'])
disp(['\begin{tabular}{|l|c|c|c|c|c|c|c|c|}'])
disp(['\hline'])
disp(sprintf(['$q_u$ & %i    & %i    & %i    & %i    & %i    & %i    & %i \\\\ '],2:8))
disp(sprintf(['C     & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f \\\\ '],slopes(1,:)))
disp(sprintf(['U     & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f \\\\ '],slopes(2,:)))
disp(['\hline'])
disp(['\end{tabular}'])
disp(['\end{center}'])
disp(['\end{table} '])



figure(1)
print -depsc2 stoneley_error_different.eps
figure(2)
print -depsc2 stoneley_error_same.eps





