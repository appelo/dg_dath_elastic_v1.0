module problemSetup
  ! This module solves the elastic wave equation for the Stoneley wave
  ! The example is the same as in Lucas paper 
  !
  use doublePrecision
  implicit none
  integer, parameter :: nvar = 2
  integer, parameter :: q_u = 10
  integer, parameter :: q_v = 10

  real(kind = dp), parameter :: pi = acos(-1.d0)  

  ! Wilcox et al parameters
  real(kind = dp), parameter :: lam_1 = 3.d0
  real(kind = dp), parameter :: mu_1  = 3.d0
  real(kind = dp), parameter :: rho_1 = 10.d0
  real(kind = dp), parameter :: lam_2 = 1.d0
  real(kind = dp), parameter :: mu_2  = 1.d0
  real(kind = dp), parameter :: rho_2 = 1.d0
  real(kind = dp), parameter :: c_stoneley = 0.5469813242138840d0
  COMPLEX(kind = dp), parameter :: B1 = ( 0.d0              , -0.2952173626624d0)
  COMPLEX(kind = dp), parameter :: B2 = (-0.6798795208473d0 ,  0.d0)
  COMPLEX(kind = dp), parameter :: B3 = ( 0.d0              ,  0.5220044931212d0)
  COMPLEX(kind = dp), parameter :: B4 = (-0.9339639688697d0 ,  0.d0)  

  ! Duru & Virta expressed in the form used in Wilcox.
!  real(kind = dp), parameter :: lam_1 = 1.d0
!  real(kind = dp), parameter :: mu_1  = 1.d0
!  real(kind = dp), parameter :: rho_1 = 1.d0
!  real(kind = dp), parameter :: lam_2 = 3.d0
!  real(kind = dp), parameter :: mu_2  = 2.d0
!  real(kind = dp), parameter :: rho_2 = 1.98d0

!  real(kind = dp), parameter :: c_stoneley = 0.999240140585103d0
!  COMPLEX(kind = dp), parameter :: B1 = ( 0.d0                , -0.1717413023501018d0)
!  COMPLEX(kind = dp), parameter :: B2 = ( 0.4531825922343313d0,  0.d0)
!  COMPLEX(kind = dp), parameter :: B3 = ( 0.d0                , -0.2783469123517910d0)
!  COMPLEX(kind = dp), parameter :: B4 = ( 0.8292487320141568d0,  0.d0)  
    
  real(kind = dp), parameter :: omega_0  = c_stoneley
  real(kind = dp), parameter :: k_0  = omega_0/c_stoneley
  real(kind = dp), parameter :: b1p = sqrt(1.d0-c_stoneley**2/(2.d0*mu_1+lam_1)*rho_1)
  real(kind = dp), parameter :: b1s = sqrt(1.d0-c_stoneley**2/mu_1*rho_1)
  real(kind = dp), parameter :: b2p = sqrt(1.d0-c_stoneley**2/(2.d0*mu_2+lam_2)*rho_2)
  real(kind = dp), parameter :: b2s = sqrt(1.d0-c_stoneley**2/mu_2*rho_2)

  ! Value v1 and v2

  integer, parameter :: nint = q_u+6
  real(kind = dp), parameter :: CFL = 2.5d0/sqrt(2.d0*mu_1+lam_1)/(dble(q_u)+1.5d0)**2	
  real(kind = dp), parameter :: tend = 1.d0
  real(kind = dp) :: bc_al_be(10:99,2,nvar)

  integer, parameter :: nplot = 2
  logical, parameter :: upwind = .true.
  integer, parameter :: rkstages = 4
  logical, parameter :: plot = .true.
  integer, parameter :: plot_freq = 100

  ! character(100), parameter :: element_name = '../../GEOMETRY/HALF_GRIDS/halfspace--e.txt'
  ! character(100), parameter :: node_name    = '../../GEOMETRY/HALF_GRIDS/halfspace--n.txt'
  character(100), parameter :: element_name = 'sgrid_4_e.txt'
  character(100), parameter :: node_name    = 'sgrid_4_n.txt'

contains

  subroutine set_bc
    implicit none
    !
    ! This routine is used to set boundary conditions
    ! on boundary curve 10,11,...
    ! The boundary conditions are on the form
    ! \alpha v + \beta \Nabla u\cdot {\bf n} = 0,
    ! with \alpha^2+\beta^2 = 1.0
    !
    
    bc_al_be(:,1,:) = 1.d0  ! All Dirichlet
!    bc_al_be(10,1,:) = 0.d0  ! All Traction
!    bc_al_be(11,1,:) = 0.d0  ! All Traction
!    bc_al_be(12,1,:) = 0.d0  ! All Traction
!    bc_al_be(13,1,:) = 0.d0  ! All Traction
    bc_al_be(:,2,:) = sqrt(1.d0-bc_al_be(:,1,:)**2)

  end subroutine set_bc
  
  subroutine compute_bc_forcing(v_bc_force,traction_bc_force,x,y,t,&
       ivar,bc_number,nint)
    use doubleprecision
    implicit none
    integer  :: nint,ivar,bc_number
    real(kind = dp) :: v_bc_force(nint),traction_bc_force(nint),x(nint),y(nint),t
    real(kind = dp) :: init_v(nint)
    
    v_bc_force = 0.d0
    traction_bc_force = 0.d0
    
    if(maxval(y).gt.1.0d-5) then ! As the function is continuous at y=0 this should be ok
       if (ivar .eq. 1) then
          v_bc_force = real(((0.d0,1.d0)*k_0*B1*dexp(-k_0*b1p*y) &
               + k_0*b1s*B2*dexp(-k_0*b1s*y))&
               *(-omega_0*(0.d0,1.d0))*cdexp((0.d0,1.d0)*(k_0*x-omega_0*t)))
       else
          v_bc_force = real((-k_0*b1p*B1*dexp(-k_0*b1p*y) &
               + (0.d0,1.d0)*k_0*B2*dexp(-k_0*b1s*y))&
               *(-omega_0*(0.d0,1.d0))*cdexp((0.d0,1.d0)*(k_0*x-omega_0*t)))
       end if
    else
       if (ivar .eq. 1) then
          v_bc_force = real(((0.d0,1.d0)*k_0*B3*dexp(k_0*b2p*y) &
               - k_0*b2s*B4*dexp(k_0*b2s*y))&
               *(-omega_0*(0.d0,1.d0))*cdexp((0.d0,1.d0)*(k_0*x-omega_0*t)))
       else
          v_bc_force = real((k_0*b2p*B3*dexp(k_0*b2p*y) &
               + (0.d0,1.d0)*k_0*B4*dexp(k_0*b2s*y))&
               *(-omega_0*(0.d0,1.d0))*cdexp((0.d0,1.d0)*(k_0*x-omega_0*t)))
       end if
    end if

    return
    
  end subroutine compute_bc_forcing
  
  real(kind = dp) function init_u(x,y,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y,t
    t = 0.d0
    if(y.gt.0.d0) then ! As the function is continuous at y=0 this should be ok
       if (ivar .eq. 1) then
          init_u = real(((0.d0,1.d0)*k_0*B1*dexp(-k_0*b1p*y) &
               + k_0*b1s*B2*dexp(-k_0*b1s*y))&
               *cdexp((0.d0,1.d0)*(k_0*x-omega_0*t)))
       else
          init_u = real((-k_0*b1p*B1*dexp(-k_0*b1p*y) &
               + (0.d0,1.d0)*k_0*B2*dexp(-k_0*b1s*y))&
               *cdexp((0.d0,1.d0)*(k_0*x-omega_0*t)))
       end if
    else
       if (ivar .eq. 1) then
          init_u = real(((0.d0,1.d0)*k_0*B3*dexp(k_0*b2p*y) &
               - k_0*b2s*B4*dexp(k_0*b2s*y))&
               *cdexp((0.d0,1.d0)*(k_0*x-omega_0*t)))
       else
          init_u = real((k_0*b2p*B3*dexp(k_0*b2p*y) &
               + (0.d0,1.d0)*k_0*B4*dexp(k_0*b2s*y))&
               *cdexp((0.d0,1.d0)*(k_0*x-omega_0*t)))
       end if
    end if
    return
  end function init_u
  
  real(kind = dp) function exact_u(x,y,t,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y,t
    if(y.gt.0.d0) then ! As the function is continuous at y=0 this should be ok
       if (ivar .eq. 1) then
          exact_u = real(((0.d0,1.d0)*k_0*B1*dexp(-k_0*b1p*y) &
               + k_0*b1s*B2*dexp(-k_0*b1s*y))&
               *cdexp((0.d0,1.d0)*(k_0*x-omega_0*t)))
       else
          exact_u = real((-k_0*b1p*B1*dexp(-k_0*b1p*y) &
               + (0.d0,1.d0)*k_0*B2*dexp(-k_0*b1s*y))&
               *cdexp((0.d0,1.d0)*(k_0*x-omega_0*t)))
       end if
    else
       if (ivar .eq. 1) then
          exact_u = real(((0.d0,1.d0)*k_0*B3*dexp(k_0*b2p*y) &
               - k_0*b2s*B4*dexp(k_0*b2s*y))&
               *cdexp((0.d0,1.d0)*(k_0*x-omega_0*t)))
       else
          exact_u = real((k_0*b2p*B3*dexp(k_0*b2p*y) &
               + (0.d0,1.d0)*k_0*B4*dexp(k_0*b2s*y))&
               *cdexp((0.d0,1.d0)*(k_0*x-omega_0*t)))
       end if
    end if
    return
  end function exact_u

  real(kind = dp) function init_v(x,y,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y,t
    
    t = 0.d0
    if(y.gt.0.d0) then ! As the function is continuous at y=0 this should be ok
       if (ivar .eq. 1) then
          init_v = real(((0.d0,1.d0)*k_0*B1*dexp(-k_0*b1p*y) &
               + k_0*b1s*B2*dexp(-k_0*b1s*y))&
               *(-omega_0*(0.d0,1.d0))*cdexp((0.d0,1.d0)*(k_0*x-omega_0*t)))
       else
          init_v = real((-k_0*b1p*B1*dexp(-k_0*b1p*y) &
               + (0.d0,1.d0)*k_0*B2*dexp(-k_0*b1s*y))&
               *(-omega_0*(0.d0,1.d0))*cdexp((0.d0,1.d0)*(k_0*x-omega_0*t)))
       end if
    else
       if (ivar .eq. 1) then
          init_v = real(((0.d0,1.d0)*k_0*B3*dexp(k_0*b2p*y) &
               - k_0*b2s*B4*dexp(k_0*b2s*y))&
               *(-omega_0*(0.d0,1.d0))*cdexp((0.d0,1.d0)*(k_0*x-omega_0*t)))
       else
          init_v = real((k_0*b2p*B3*dexp(k_0*b2p*y) &
               + (0.d0,1.d0)*k_0*B4*dexp(k_0*b2s*y))&
               *(-omega_0*(0.d0,1.d0))*cdexp((0.d0,1.d0)*(k_0*x-omega_0*t)))
       end if
    end if
    return
  end function init_v

  subroutine pis(xy,s,xy_start,xy_end,curve_type)

    use doublePrecision
    implicit none
    real(kind=dp) :: xy(2),xy_start(2),xy_end(2),s
    real(kind=dp) :: theta_s, theta_e
    integer :: curve_type
    ! This problem only has straight lines...

    xy = xy_start + 0.5d0*(1.d0 + s)*(xy_end - xy_start)

  end subroutine pis

end module problemSetup
