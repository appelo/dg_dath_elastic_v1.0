clear

% Grids 1,2,3
% This is the sqrt of the smallest element.
%heff1=[0.242 0.115 5.52E-002];
heff=[0.3902 0.196 0.09813];
% This corresponds to straight scaling.
%heff = [4 2 1];

a1 = dir('err-up-.false.-lam-100.d0-q-*-grid-1.txt');
a2 = dir('err-up-.true.-lam-100.d0-q-*-grid-1.txt');
b1 = dir('err-up-.false.-lam-100.d0-q-*-grid-2.txt');
b2 = dir('err-up-.true.-lam-100.d0-q-*-grid-2.txt');
d1 = dir('err-up-.false.-lam-100.d0-q-*-grid-3.txt');
d2 = dir('err-up-.true.-lam-100.d0-q-*-grid-3.txt');

EU=[]; EC=[];
for i = 1:length(a2)
    c1 = load(a1(i).name);
    u1 = load(a2(i).name);
    c2 = load(b1(i).name);
    u2 = load(b2(i).name);
    c3 = load(d1(i).name);
    u3 = load(d2(i).name);
    EU = [EU ; [u1(end,4) u2(end,4) u3(end,4)]];
    EC = [EC ; [c1(end,4) c2(end,4) c3(end,4)]];
end
figure(1)
q = 1:length(a1);
semilogy(q,EU,'s-',q,EC,'o-','linewidth',2)

EU100 = EU;
EC100 = EC;
disp('$L_2$-errors after one period $\lambda = 100, \, \mu = 1$ \\')
disp('Upwind flux \\')
disp(sprintf(['$q_u$ &%4i        & %1i        & %1i        & %1i        ' ...
              '& %1i        & %1i        & %1i        & %1i        ' ...
              '& %1i        \\\\ '],1:9))
disp(sprintf(['$h$   &   %5.2e & %5.2e & %5.2e & %5.2e & %5.2e & %5.2e & %5.2e & ' ...
'%5.2e & %5.2e \\\\ \n'],EU100))
disp('Central flux \\')
disp(sprintf(['$q_u$ &%4i        & %1i        & %1i        & %1i        ' ...
              '& %1i        & %1i        & %1i        & %1i        ' ...
              '& %1i        \\\\ '],1:9))
disp(sprintf(['$h$   &   %5.2e & %5.2e & %5.2e & %5.2e & %5.2e & %5.2e & %5.2e & ' ...
'%5.2e & %5.2e \\\\ \n'],EC100))

RU = zeros(length(EU(:,1)),2);
RC = zeros(length(EC(:,1)),2);
for  i = 1:length(RU(:,1))
    RU(i,:) = log2(EU(i,1:2)./EU(i,2:3))./log2(heff(1:2)./heff(2:3));
    RC(i,:) = log2(EC(i,1:2)./EC(i,2:3))./log2(heff(1:2)./heff(2:3));
end

RATE1 = [RU RC];
disp(sprintf(['%3i %5.2f %5.2f %5.2f %5.2f \n'],[(2:10)' RU RC]'))





a1 = dir('err-up-.false.-lam-1.d0-q-*-grid-1.txt');
a2 = dir('err-up-.true.-lam-1.d0-q-*-grid-1.txt');
b1 = dir('err-up-.false.-lam-1.d0-q-*-grid-2.txt');
b2 = dir('err-up-.true.-lam-1.d0-q-*-grid-2.txt');
d1 = dir('err-up-.false.-lam-1.d0-q-*-grid-3.txt');
d2 = dir('err-up-.true.-lam-1.d0-q-*-grid-3.txt');

EU=[]; EC=[];
for i = 1:length(a2)
    c1 = load(a1(i).name);
    u1 = load(a2(i).name);
    c2 = load(b1(i).name);
    u2 = load(b2(i).name);
    c3 = load(d1(i).name);
    u3 = load(d2(i).name);
    EU = [EU ; [u1(end,4) u2(end,4) u3(end,4)]];
    EC = [EC ; [c1(end,4) c2(end,4) c3(end,4)]];
end
q = 1:length(a1);
figure(2)
semilogy(q,EU,'s-',q,EC,'o-','linewidth',2)

disp('$L_2$-errors after one period $\lambda = 1, \, \mu = 1$ \\')
disp('Upwind flux \\')
disp(sprintf(['$q_u$ &%4i        & %1i        & %1i        & %1i        ' ...
              '& %1i        & %1i        & %1i        & %1i        ' ...
              '& %1i        \\\\ '],1:9))
disp(sprintf(['$h$   &   %5.2e & %5.2e & %5.2e & %5.2e & %5.2e & %5.2e & %5.2e & ' ...
'%5.2e & %5.2e \\\\ \n'],EU))
disp('Central flux \\')
disp(sprintf(['$q_u$ &%4i        & %1i        & %1i        & %1i        ' ...
              '& %1i        & %1i        & %1i        & %1i        ' ...
              '& %1i        \\\\ '],1:9))
disp(sprintf(['$h$   &   %5.2e & %5.2e & %5.2e & %5.2e & %5.2e & %5.2e & %5.2e & ' ...
'%5.2e & %5.2e \\\\ \n'],EC))

RU = zeros(length(EU(:,1)),2);
RC = zeros(length(EC(:,1)),2);
for  i = 1:length(RU(:,1))
    RU(i,:) = log2(EU(i,1:2)./EU(i,2:3))./log2(heff(1:2)./heff(2:3));
    RC(i,:) = log2(EC(i,1:2)./EC(i,2:3))./log2(heff(1:2)./heff(2:3));
end
disp(sprintf(['%3i %5.2f %5.2f %5.2f %5.2f \n'],[(2:10)' RU RC]'))
RATE1 = [(2:10)' RU RC RATE1];


disp(sprintf(['%3i & %5.2f & %5.2f & %5.2f & %5.2f & %5.2f & %5.2f ' ...
              '& %5.2f & %5.2f \\\\ \n'],RATE1'))


figure(1)
set(gca,'Fontsize',18)
legend('AAA1','AAA2','AAA3','AAA4','AAA5','AAA6')
xlabel('XXXXX')
ylabel('YYYYY')
axis([-0.1 9.1 1e-15 10])
print -depsc2 annulus_errors_l100.eps


figure(2)
set(gca,'Fontsize',18)
legend('AAA1','AAA2','AAA3','AAA4','AAA5','AAA6')
xlabel('XXXXX')
ylabel('YYYYY')
axis([-0.1 9.1 1e-10 10])
print -depsc2 annulus_errors_l1.eps
