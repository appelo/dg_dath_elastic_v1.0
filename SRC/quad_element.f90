module quad_element
  use doublePrecision
  implicit none
  !
  type quad
     !  
     ! We envsion solving 
     !    MV v_t + SU u = Fv  
     !    MU v_t + SV u = Fu  
     !    
     ! The variables u and v represents displaments and velocities
     ! The degree on the element for v is (0:q)^2 and u is (0:q+1)^2
     ! The third dimension of u and v refers to how many variables in
     ! dissplacement and velocity the governing equations have, e.g.
     ! the wave equation has nvar = 1, the elastic wave eq. has nvar = 2.
     ! 
     ! Thus the mass matrices MV and MU are of dimension (nvar*(q+1)^2)^2 and (nvar*(q+2)^2)^2 
     ! and the stiffness matrix SU is (nvar*(q+1)^2) x (nvar*(q+2)^2) 
     ! and the stiffness matrix SV is (nvar*(q+2)^2) x (nvar*(q+1)^2) 
     !
     ! We actually store the LU decomposition of the mass matrices
     ! 
     ! 
     ! Each quad can be mapped to the reference element [-1,1]^2
     ! The ordering of the nodes and faces are as follows:
     !        1
     !    2-------1
     !    |       |
     !  2 |       | 4
     !    |       |
     !    3-------4
     !        3
     !
     ! The Gordon-Hall mapping used to compute the metric
     ! assumes down-up and left-right orientaiton of the sides.
     !

     !%% Geometric information
     ! Number of quadrature points
     integer :: n_gll
     real(kind=dp) :: xy(4,2)
     real(kind=dp), dimension(:,:), allocatable :: jac, rx, ry, sx, sy
     real(kind=dp), dimension(:,:), allocatable :: x, y, lam_arr, mu_arr
     !%% Connectivity information
     ! element #
     integer :: my_ind
     ! Neighbours quad nr, and what is their face?
     integer :: nbr(4,2)
     integer :: nbr_orientation(4)

     ! Boundary condition type > 0 is other element < 0 is physical BC
     integer :: bc_type(4)
     logical :: has_physical_bc

     !%% Information of the approximation
     integer :: q_u,q_v,nvar
     real(kind=dp), dimension(:,:,:), allocatable :: u,v,fu,fv
     real(kind=dp), dimension(:,:),   allocatable :: MV,MU,SV,SU
     integer,       dimension(:,:),   allocatable :: IPIVV,IPIVU

     ! Arrays for states on the quad to be used in the flux computation. 
     ! The velocity, gradient of F, the outward pointing normals and
     ! the line element on each face
     real(kind=dp), dimension(:,:,:), allocatable :: v_in, wx_in, wy_in
     real(kind=dp), dimension(:,:,:), allocatable :: v_out, wx_out, wy_out
     real(kind=dp), dimension(:,:)  , allocatable :: nx_in, ny_in, dl_face

     ! Extra equation.
     integer :: extra_eq_1(3), extra_eq_2(3), extra_eq_3(3)
          
  end type quad

contains

  subroutine allocate_quad(qd,q_u,q_v,n_gll,nvar)
    implicit none
    integer :: n_gll,q_u,q_v,nvar
    type(quad) :: qd
    qd%n_gll = n_gll
    qd%q_u = q_u
    qd%q_v = q_v
    qd%nvar = nvar

    ! Allocate approximation
    allocate(qd%u(0:q_u,0:q_u,nvar))
    allocate(qd%v(0:q_v,0:q_v,nvar))
    allocate(qd%fu(0:q_u,0:q_u,nvar))
    allocate(qd%fv(0:q_v,0:q_v,nvar))
    allocate(qd%MU((q_u+1)**2*nvar,(q_u+1)**2*nvar))
    allocate(qd%MV((q_v+1)**2*nvar,(q_v+1)**2*nvar))
    allocate(qd%SU((q_v+1)**2*nvar,(q_u+1)**2*nvar))
    allocate(qd%SV((q_u+1)**2*nvar,(q_v+1)**2*nvar))
    allocate(qd%IPIVU((q_u+1)**2*nvar,(q_u+1)**2*nvar))
    allocate(qd%IPIVV((q_v+1)**2*nvar,(q_v+1)**2*nvar))

    ! Allocate metric
    allocate(qd%jac(n_gll,n_gll))
    allocate(qd%rx(n_gll,n_gll))
    allocate(qd%ry(n_gll,n_gll))
    allocate(qd%sx(n_gll,n_gll))
    allocate(qd%sy(n_gll,n_gll))

    ! Allocate grid and Lame parameters
    allocate(qd%x(n_gll,n_gll))
    allocate(qd%y(n_gll,n_gll))
    allocate(qd%lam_arr(n_gll,n_gll))
    allocate(qd%mu_arr(n_gll,n_gll))

    ! Allocate face data
    allocate(qd%v_in(n_gll,4,nvar))
    allocate(qd%wx_in(n_gll,4,nvar))
    allocate(qd%wy_in(n_gll,4,nvar))
    allocate(qd%v_out(n_gll,4,nvar))
    allocate(qd%wx_out(n_gll,4,nvar))
    allocate(qd%wy_out(n_gll,4,nvar))
    allocate(qd%nx_in(n_gll,4))
    allocate(qd%ny_in(n_gll,4))
    allocate(qd%dl_face(n_gll,4))

  end subroutine allocate_quad

  subroutine deallocate_quad(qd)
    implicit none
    type(quad) :: qd
    ! DeAllocate approximation
    deallocate(qd%u)
    deallocate(qd%v)
    deallocate(qd%fu)
    deallocate(qd%fv)
    deallocate(qd%su)
    deallocate(qd%sv)
    deallocate(qd%mu)
    deallocate(qd%mv)
    deallocate(qd%ipivu)
    deallocate(qd%ipivv)

    ! DeAllocate metric
    deallocate(qd%jac)
    deallocate(qd%rx)
    deallocate(qd%ry)
    deallocate(qd%sx)
    deallocate(qd%sy)

    ! DeAllocate grid and lame
    deallocate(qd%x)
    deallocate(qd%y)
    deallocate(qd%lam_arr)
    deallocate(qd%mu_arr)

    ! DeAllocate face data
    deallocate(qd%v_in)
    deallocate(qd%wx_in)
    deallocate(qd%wy_in)
    deallocate(qd%v_out)
    deallocate(qd%wx_out)
    deallocate(qd%wy_out)
    deallocate(qd%nx_in)
    deallocate(qd%ny_in)
    deallocate(qd%dl_face)

  end subroutine deallocate_quad

end module quad_element
