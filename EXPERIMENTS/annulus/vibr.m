function void = vibr

l = 1;
m = 1;

Ra = 1/2;
Rb = 1;

% Start values
for alpha = linspace(0.01,100,10);
    B = -besselj(1,alpha*Ra)/bessely(1,alpha*Ra);
    urb = besselj(1,alpha*Rb) + B*bessely(1,alpha*Rb);
    tr = l/Rb*urb+alpha*(2*m+l)*(dbj1(alpha*Rb)+B*dby1(alpha*Rb));
    plot(alpha,tr,'k*')
    hold on
end
%return

r = linspace(Ra,Rb,1000);

% Start values
%alpha = 3.5;
alpha = 25.;
B = -besselj(1,alpha*Ra)/bessely(1,alpha*Ra);
urb = besselj(1,alpha*Rb) + B*bessely(1,alpha*Rb);
tr = l/Rb*urb+alpha*(2*m+l)*(dbj1(alpha*Rb)+B*dby1(alpha*Rb));
alpha0 = alpha;
tr0 = tr;
%alpha1 = 3.4;
alpha1 = 26.;

for i = 1:9
    alpha = alpha1;
    B = -besselj(1,alpha*Ra)/bessely(1,alpha*Ra);
    urb = besselj(1,alpha*Rb) + B*bessely(1,alpha*Rb);
    tr = l/Rb*urb+alpha*(2*m+l)*(dbj1(alpha*Rb)+B*dby1(alpha*Rb));
    tr1 = tr;
    dalpha = tr1/(tr1-tr0)*(alpha1-alpha0);
    alpha0 = alpha1;
    tr0 = tr1;
    alpha1 = alpha1 - dalpha; 
    disp([i alpha1 B dalpha])
end

alpha = alpha1;
B = -besselj(1,alpha*Ra)/bessely(1,alpha*Ra);
ur = besselj(1,alpha*r) + B*bessely(1,alpha*r);
clf
plot(r,ur)

function  y = dby1(r)
y = 0.5*(bessely(0,r)-bessely(2,r));

function  y = dbj1(r)
y = 0.5*(besselj(0,r)-besselj(2,r));