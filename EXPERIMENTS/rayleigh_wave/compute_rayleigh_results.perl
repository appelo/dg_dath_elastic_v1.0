#!/usr/apps/bin/perl
#
# perl program to run convergence test for the harmonic vibration in an annulus
use POSIX;

# Here is the generic file
$cmdFile="./problemsetup_rayleigh.f90.T";

$q=9;
$cfl = "0.5d0";
for( $g=2; $g <= 8; $g = 2*$g){
  
  $qu=$q;
  $qv=$q;
  
  open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
  open(OUTFILE,">./problemsetup_rayleigh.f90") || die "cannot open file $file!" ;
  # Setup the outfile
  while( <FILE> )
    {
      $_ =~ s/\bQQQU\b/$qu/;
      $_ =~ s/\bQQQV\b/$qv/;
      $_ =~ s/GGG/$g/;
      $_ =~ s/CCCFFFLLL/$cfl/;
      print OUTFILE $_;
    }
  close( OUTFILE );
  close( FILE );
  # Compose the output file
  system("make clean -s ; make -s; rm -rf multivar.* c0* ; ./dg_dath.x");
  system("mv err.txt Rayerr-grid-$g-q-$qu-$qv-.txt");
}


$q = 3;
$cfl = "1.0d0";
for( $g=5; $g <= 20; $g = 2*$g){
  
  $qu=$q;
  $qv=$q;
  
  open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
  open(OUTFILE,">./problemsetup_rayleigh.f90") || die "cannot open file $file!" ;
  # Setup the outfile
  while( <FILE> )
    {
      $_ =~ s/\bQQQU\b/$qu/;
      $_ =~ s/\bQQQV\b/$qv/;
      $_ =~ s/GGG/$g/;
      $_ =~ s/CCCFFFLLL/$cfl/;
      print OUTFILE $_;
    }
  close( OUTFILE );
  close( FILE );
  # Compose the output file
  system("make clean -s ; make -s; rm -rf multivar.* c0* ; ./dg_dath.x");
  system("mv err.txt Rayerr-grid-$g-q-$qu-$qv-.txt");
}


