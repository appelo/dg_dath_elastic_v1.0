module problemSetup
  ! This module solves the elastic wave equation in a square cavity
  ! with manufactured solution according to example 5.2.1 in Duru, Kreiss & Mattsson
  !
  use doublePrecision
  implicit none
  integer, parameter :: nvar = 2
  integer, parameter :: q_u = 13
  integer, parameter :: q_v = 13

  real(kind = dp), parameter :: pi = acos(-1.d0)
  real(kind = dp), parameter :: lam_0 = 2.d0
  real(kind = dp), parameter :: mu_0  = 1.d0
  real(kind = dp), parameter :: omega_0  = 13.137063197233d0
  real(kind = dp), parameter :: k_0   = 2.d0*pi
  real(kind = dp), parameter :: cs_0  = sqrt(mu_0)
  real(kind = dp), parameter :: cp_0  = sqrt(2.d0*mu_0+lam_0)
  real(kind = dp), parameter :: B_1   = 126.1992721468d0
  real(kind = dp), parameter :: B_2   = 53.88807700007d0 
  real(kind = dp), parameter :: p_0 = sqrt(omega_0**2/(2.d0*mu_0+lam_0)-k_0**2)
  real(kind = dp), parameter :: q_0 = sqrt(omega_0**2/mu_0-k_0**2)

  ! Value v1 and v2

  integer, parameter :: nint = q_u+6
  real(kind = dp), parameter :: CFL = 2.5/sqrt(2.d0*mu_0+lam_0)/(dble(q_u)+1.5d0)**2	
  real(kind = dp), parameter :: tend = 1.d0
  real(kind = dp) :: bc_al_be(10:99,2,nvar)

  integer, parameter :: nplot = 10
  logical, parameter :: upwind =  .true.
  integer, parameter :: rkstages = 4
  logical, parameter :: plot = .true.
  integer, parameter :: plot_freq = 10

  ! A single element or four
  character(100), parameter :: element_name = 'lambgrid_4_e.txt'
  character(100), parameter :: node_name    = 'lambgrid_4_n.txt'

contains

  subroutine set_bc
    implicit none
    !
    ! This routine is used to set boundary conditions
    ! on boundary curve 10,11,...
    ! The boundary conditions are on the form
    ! \alpha v + \beta \Nabla u\cdot {\bf n} = 0,
    ! with \alpha^2+\beta^2 = 1.0
    !

    bc_al_be(:,1,:) = 1.d0  ! All Dirichlet
    
    ! bc_al_be(10,1,:) = 0.d0  ! Traction
    ! bc_al_be(11,1,:) = 0.d0  ! Traction 
    bc_al_be(12,1,:) = 0.d0  ! Traction
    bc_al_be(13,1,:) = 0.d0  ! Traction

    bc_al_be(:,2,:) = sqrt(1.d0-bc_al_be(:,1,:)**2)

  end subroutine set_bc

  subroutine compute_bc_forcing(v_bc_force,traction_bc_force,x,y,t,&
       ivar,bc_number,nint)
    use doubleprecision
    implicit none
    integer  :: nint,ivar,bc_number,i
    real(kind = dp) :: v_bc_force(nint),traction_bc_force(nint),x(nint),y(nint),t
    
 
    traction_bc_force = 0.d0
    if(ivar.eq.1) then
       do i = 1,nint
          v_bc_force(i) = lamb_wave_vel(x(i),y(i),t,1)
       end do
    else
       do i = 1,nint
          v_bc_force(i) = lamb_wave_vel(x(i),y(i),t,2)
       end do
    end if
    if(bc_number .eq. 12) then
       v_bc_force = 0.d0
    end if
    if(bc_number .eq. 13) then
       v_bc_force = 0.d0
    end if
  end subroutine compute_bc_forcing

  real(kind = dp) function init_u(x,y,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y
    if (ivar .eq. 1) then
       init_u = lamb_wave_dis(x,y,0.d0,1)
    else
       init_u = lamb_wave_dis(x,y,0.d0,2)
    end if
    return
  end function init_u

  real(kind = dp) function init_v(x,y,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y
    ! This is actually used for the mms forcing for v!
    if (ivar .eq. 1) then
       init_v = lamb_wave_vel(x,y,0.d0,1)
    else
       init_v = lamb_wave_vel(x,y,0.d0,2)
    end if
    return
  end function init_v

  real(kind = dp) function lamb_wave_dis(x,y,t,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y,t
       
    if (ivar .eq. 1) then
       lamb_wave_dis = (-k_0*B_1*cos(p_0*y)-q_0*B_2*cos(q_0*y))*sin(k_0*x-omega_0*t)
    else 
       lamb_wave_dis = (-p_0*B_1*sin(p_0*y)+k_0*B_2*sin(q_0*y))*cos(k_0*x-omega_0*t)
    end if
    return
  end function lamb_wave_dis

  real(kind = dp) function lamb_wave_vel(x,y,t,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y,t

        if (ivar .eq. 1) then
       lamb_wave_vel = -omega_0*(-k_0*B_1*cos(p_0*y)-q_0*B_2*cos(q_0*y))*cos(k_0*x-omega_0*t)
    else 
       lamb_wave_vel = +omega_0*(-p_0*B_1*sin(p_0*y)+k_0*B_2*sin(q_0*y))*sin(k_0*x-omega_0*t)
    end if
    return
  end function lamb_wave_vel

  subroutine pis(xy,s,xy_start,xy_end,curve_type)

    use doublePrecision
    implicit none
    real(kind=dp) :: xy(2),xy_start(2),xy_end(2),s
    real(kind=dp) :: theta_s, theta_e
    integer :: curve_type
    ! This problem only has straight lines...

    xy = xy_start + 0.5d0*(1.d0 + s)*(xy_end - xy_start)

  end subroutine pis

end module problemSetup
