program dg_dath
  use doublePrecision
  use quad_element
  use problemSetup
  use chebfuns
  use silooutput
  implicit none
  type(quad), dimension(:), allocatable :: qds
  integer :: i,j,iy,k,it,ix,l,ivar
  integer :: num_quads,m_size_u,m_size_v,INFO
  real(kind=dp) :: weights(nint), xnodes(nint),diffmat(nint,nint)
  real(kind=dp) :: BFWeights(nint,2),integrand(nint,nint)
  real(kind=dp) :: fint(nint)
  real(kind=dp) :: area,mc
  real(kind=dp), dimension(:,:),   allocatable :: chebmat,dchebmat
  real(kind=dp), dimension(:,:,:), allocatable :: xplot,yplot
  real(kind=dp), dimension(:,:,:,:), allocatable :: uplot,vplot
  real(kind=dp), dimension(:,:),   allocatable :: chebmat_plot
  ! For Runge-Kutta
  real(kind=dp) :: dt,h_eff,t,ctw
  integer ::  nsteps
  real(kind=dp), dimension(:,:,:,:)  ,   allocatable :: vm,um,uexact
  real(kind=dp), dimension(:,:,:,:,:),   allocatable :: kv,ku
  ! For comparison to exact solution
  real(kind=dp), dimension(:,:,:,:)  ,   allocatable :: fvmms
  real(kind=dp) :: time1,time2,maxv,rn(2),l2_err,l2_norm,max_err

  !
  interface
     subroutine Set_up_elements(qds,num_quads)
       use quad_element
       implicit none
       type(quad), dimension(:), allocatable :: qds
       integer :: num_quads
     end subroutine Set_up_elements
  end interface

  open(29,file = 'err.txt')
  
  ! Weights for quadrature and differentiation on the elements.
  call lglnodes(xnodes,weights,nint-1)
  ! Differentiation matrix for the metric.
  do i = 1,nint
     call weights1(xnodes(i),xnodes,nint-1,nint-1,1,BFWEIGHTS)
     DiffMat(i,:) = BFWEIGHTS(:,2)
  end do
  ! Basis and derivative of basis at the quadrature nodes.
  allocate(chebmat(nint,0:q_u),dchebmat(nint,0:q_u))
  do i = 1,nint
     do k = 0,q_u
        chebmat(i,k)   = cheb(xnodes(i),k)
        dchebmat(i,k)  = dercheb(xnodes(i),k)
     end do
  end do

  ! Read and process the grid. On exit qds will have
  ! been assigned nodes, connectivity, boundary type, etc.
  call Set_up_elements(qds,num_quads)
  ! For output.
  allocate(vplot(0:nplot-1,0:nplot-1,num_quads,nvar),&
       uplot(0:nplot-1,0:nplot-1,num_quads,nvar),&
       xplot(0:nplot-1,0:nplot-1,num_quads),&
       yplot(0:nplot-1,0:nplot-1,num_quads))
  allocate(chebmat_plot(0:nplot-1,0:q_u))

  ! Plot stuff.
  do i = 1,num_quads
     call xy_plot_grid(qds(i),xplot(0,0,i),yplot(0,0,i),nplot)
  end do
  do i = 0,nplot-1
     do k = 0,q_u
        chebmat_plot(i,k) = cheb(-1.d0 + 2.d0*dble(i)/dble(nplot-1),k)
     end do
  end do

  ! Compute and store the metric on each quad.
  do i = 1,num_quads
     call set_metric(qds(i),xnodes,diffmat,nint)
  end do
  ! Compute and store the metric on each quad.
  do i = 1,num_quads
        qds(i)%lam_arr = lam_0  
        qds(i)%mu_arr = mu_0
  end do
  
  ! Estimate the smallest h from the area of each cell
  h_eff = 1.d16
  do i = 1,num_quads
     do iy = 1,nint
        fint(iy) = sum(weights*qds(i)%jac(1:nint,iy))
     end do
     area = sum(weights*fint)
     if (area .lt. 0.d0) then
        write(*,*) "Negative area, stopping..."
        stop 123
     end if
     if (sqrt(area) .lt. h_eff) h_eff = sqrt(area)
  end do
  !
  dt = cfl*h_eff
  nsteps = floor(tend/dt)+1
  dt = tend/dble(nsteps)
  write(*,*) 'The timestep and h_eff are: ',dt,h_eff

  call set_bc
  allocate(um(0:q_u,0:q_u,nvar,num_quads),vm(0:q_v,0:q_v,nvar,num_quads),&
       ku(0:q_u,0:q_u,nvar,rkstages,num_quads),&
       kv(0:q_v,0:q_v,nvar,rkstages,num_quads),&
       uexact(0:q_u,0:q_u,nvar,num_quads))

  ! Assemble mass and stiffness matrices
  ! then factor the mass matrices
  ! Also, set the initial data.
  !$OMP PARALLEL DO PRIVATE(I,m_size_u,m_size_v)
  do i = 1,num_quads
     call assemble(qds(i),nint,chebmat,dchebmat,weights)
     m_size_u = (qds(i)%q_u+1)**2*qds(i)%nvar
     m_size_v = (qds(i)%q_v+1)**2*qds(i)%nvar
     CALL DGETRF(m_size_v,m_size_v,qds(i)%MV,&
          m_size_v,qds(i)%IPIVV,INFO)
     if (info .ne. 0) write(*,*) "Warning LU factorization of MV in element "&
          , i, " did not work. INFO = ",INFO
     CALL DGETRF(m_size_u,m_size_u,qds(i)%MU,&
          m_size_u,qds(i)%IPIVU,INFO)
     if (info .ne. 0) then
        write(*,*) "Warning LU factorization of MU in element "&
          , i, " did not work. INFO = ",INFO
        stop 123
     end if
     call set_initial_data(qds(i),nint)
  end do
  ! Method of manufactured solution
  allocate(fvmms(0:q_v,0:q_v,nvar,num_quads))
  fvmms = 0.d0
  do i = 1,num_quads
     uexact(:,:,:,i) = qds(i)%u(:,:,:)
     do iy = 1,nint
        do ix = 1,nint
           integrand(ix,iy) = mms_vt(qds(i)%x(ix,iy),qds(i)%y(ix,iy),1)
        end do
     end do
     do l = 0,q_v
        do k = 0,q_v
           ! Integrate in r for each s
           do iy = 1,nint
              fint(iy) = sum(weights*qds(i)%jac(1:nint,iy)&
                   *integrand(1:nint,iy)&
                   *chebmat(1:nint,k)*chebmat(iy,l)) 
           end do
           ! Then integrate in s
           fvmms(k,l,1,i) = sum(weights*fint)
        end do
     end do
     do iy = 1,nint
        do ix = 1,nint
           integrand(ix,iy) = mms_vt(qds(i)%x(ix,iy),qds(i)%y(ix,iy),2)
        end do
     end do
     do l = 0,q_v
        do k = 0,q_v
           ! Integrate in r for each s
           do iy = 1,nint
              fint(iy) = sum(weights*qds(i)%jac(1:nint,iy)&
                   *integrand(1:nint,iy)&
                   *chebmat(1:nint,k)*chebmat(iy,l)) 
           end do
           ! Then integrate in s
           fvmms(k,l,2,i) = sum(weights*fint)
        end do
     end do
  end do
  m_size_u = (qds(1)%q_u+1)**2*qds(1)%nvar
  m_size_v = (qds(1)%q_v+1)**2*qds(1)%nvar  

  it = 0
  call print_solution(qds,uplot,vplot,xplot,yplot,&
       chebmat_plot,q_u,q_v,num_quads,nplot,it,nvar)
  l2_norm = 0.d0
  l2_err = 0.d0
  do ivar = 1,nvar
     do i = 1,num_quads
        integrand = 0.d0
        do l = 0,q_u
           do k = 0,q_u
              mc = qds(i)%u(k,l,ivar)
              do iy = 1,nint
                 do ix = 1,nint
                    integrand(ix,iy) = integrand(ix,iy) &
                         + mc*chebmat(ix,k)*chebmat(iy,l)
                 end do
              end do
           end do
        end do
        ! Integrate in r for each s
        do iy = 1,nint
           fint(iy) = sum(weights*qds(i)%jac(1:nint,iy)*integrand(1:nint,iy)**2)
        end do
        ! Then integrate in s
        l2_norm = l2_norm + abs(sum(weights*fint))
     end do
  end do
  write(29,'(5E12.4,2(X,I5,X),2I7.5)') &
       it*dt,h_eff,maxval(abs(vplot)),maxval(abs(uplot)),&
       sqrt(l2_err)/sqrt(l2_norm),q_u,q_v,it,nsteps
  write(*,'(5E12.4,2(X,I5,X),2I7.5)') &
       it*dt,h_eff,maxval(abs(vplot)),maxval(abs(uplot)),&
       sqrt(l2_norm),q_u,q_v,it,nsteps

  !
  ! Timestep Stuff
  !

  do it = 1,nsteps
     t = dble(it-1)*dt
     ! Now evolve the solution using RK4.
     !$OMP PARALLEL DO PRIVATE(I,ivar,l,k)
     do i = 1,num_quads
        do ivar = 1,nvar
           do l = 0,q_u
              do k = 0,q_u
                 um(k,l,ivar,i) = qds(i)%u(k,l,ivar)
              end do
           end do
           do l = 0,q_v
              do k = 0,q_v
                 vm(k,l,ivar,i) = qds(i)%v(k,l,ivar)
              end do
           end do
        end do
     end do
     ! This sets the interior states.
     !$OMP PARALLEL DO PRIVATE(I)
     do i = 1,num_quads
        call compute_my_face_states(qds(i),nint,chebmat,dchebmat)
     end do
     ! This sets the exterior states based on adjacent elements
     ! or physical boundary conditions.
     call compute_outside_face_states(qds,num_quads,t)
     ! On exit the right hand sides will be computed.
     !$OMP PARALLEL DO PRIVATE(I)
     do i = 1,num_quads
        call compute_surface_integrals(qds(i),nint,chebmat,dchebmat,weights)
     end do
     ! Now we are ready to compute time-derivatives.
     !$OMP PARALLEL DO PRIVATE(I)
     do i = 1,num_quads
        ! Add on Twilight
        qds(i)%fv(:,:,:) = qds(i)%fv(:,:,:) + cos(omega_0*t)*fvmms(:,:,:,i)  
        ! The v-equation.
        call DGEMV ('N',m_size_v,m_size_u,-1.d0,qds(i)%SU,&
             m_size_v,qds(i)%u,1,1.d0,qds(i)%fv,1)
        CALL DGETRS('N',m_size_v,1,qds(i)%MV,m_size_v,&
             qds(i)%IPIVV,qds(i)%fv,m_size_v,INFO)
        ! The time derivative is now in the right hand side array.
        ! The u-equation.
        call DGEMV ('N',m_size_u,m_size_v,-1.d0,qds(i)%SV,&
             m_size_u,qds(i)%v,1,1.d0,qds(i)%fu,1)
        CALL DGETRS('N',m_size_u,1,qds(i)%MU,m_size_u,&
             qds(i)%IPIVU,qds(i)%fu,m_size_u,INFO)
        ! The time derivative is now in the right hand side array.
     end do


     ! Store stage 1 and prepare for stage 2.
     !$OMP PARALLEL DO PRIVATE(I,IVAR,K,L)
     do i = 1,num_quads
        do ivar = 1,nvar
           do l = 0,q_u
              do k = 0,q_u
                 ku(k,l,ivar,1,i) = dt*qds(i)%fu(k,l,ivar)
                 qds(i)%u(k,l,ivar) = um(k,l,ivar,i) + 0.5d0*ku(k,l,ivar,1,i)
              end do
           end do
           do l = 0,q_v
              do k = 0,q_v
                 kv(k,l,ivar,1,i) = dt*qds(i)%fv(k,l,ivar)
                 qds(i)%v(k,l,ivar) = vm(k,l,ivar,i) + 0.5d0*kv(k,l,ivar,1,i)
              end do
           end do
        end do
     end do
     
     ! Increment time
     t = t + 0.5d0*dt
     ! Stage 2. Ditto above.

     !$OMP PARALLEL DO PRIVATE(I)
     do i = 1,num_quads
        call compute_my_face_states(qds(i),nint,chebmat,dchebmat)
     end do
     call compute_outside_face_states(qds,num_quads,t)

     !$OMP PARALLEL DO PRIVATE(I)
     do i = 1,num_quads
        call compute_surface_integrals(qds(i),nint,chebmat,dchebmat,weights)
     end do

     !$OMP PARALLEL DO PRIVATE(I)
     do i = 1,num_quads 
        ! Twilight
        qds(i)%fv = qds(i)%fv + cos(omega_0*t)*fvmms(:,:,:,i)  
        call DGEMV ('N',m_size_v,m_size_u,-1.d0,qds(i)%SU,&
             m_size_v,qds(i)%u,1,1.d0,qds(i)%fv,1)
        CALL DGETRS('N',m_size_v,1,qds(i)%MV,m_size_v,&
             qds(i)%IPIVV,qds(i)%fv,m_size_v,INFO)
        call DGEMV ('N',m_size_u,m_size_v,-1.d0,qds(i)%SV,&
             m_size_u,qds(i)%v,1,1.d0,qds(i)%fu,1)
        CALL DGETRS('N',m_size_u,1,qds(i)%MU,m_size_u,&
             qds(i)%IPIVU,qds(i)%fu,m_size_u,INFO)
     end do

     !$OMP PARALLEL DO PRIVATE(I,IVAR,K,L)
     do i = 1,num_quads
        do ivar = 1,nvar
           do l = 0,q_u
              do k = 0,q_u
                 ku(k,l,ivar,2,i) = dt*qds(i)%fu(k,l,ivar)
                 qds(i)%u(k,l,ivar) = um(k,l,ivar,i) + 0.5d0*ku(k,l,ivar,2,i)
              end do
           end do
           do l = 0,q_v
              do k = 0,q_v
                 kv(k,l,ivar,2,i) = dt*qds(i)%fv(k,l,ivar) 
                 qds(i)%v(k,l,ivar) = vm(k,l,ivar,i) + 0.5d0*kv(k,l,ivar,2,i)
              end do
           end do
        end do
     end do

     ! Increment time (With 0...)
     t = t + 0.d0*dt
     ! Stage 3. Ditto above.

     !$OMP PARALLEL DO PRIVATE(I)
     do i = 1,num_quads
        call compute_my_face_states(qds(i),nint,chebmat,dchebmat)
     end do
     call compute_outside_face_states(qds,num_quads,t)

     !$OMP PARALLEL DO PRIVATE(I)
     do i = 1,num_quads
        call compute_surface_integrals(qds(i),nint,chebmat,dchebmat,weights)
     end do

     !$OMP PARALLEL DO PRIVATE(I)
     do i = 1,num_quads
        ! Twilight
        qds(i)%fv = qds(i)%fv + cos(omega_0*t)*fvmms(:,:,:,i)  
        call DGEMV ('N',m_size_v,m_size_u,-1.d0,qds(i)%SU,&
             m_size_v,qds(i)%u,1,1.d0,qds(i)%fv,1)
        CALL DGETRS('N',m_size_v,1,qds(i)%MV,m_size_v,&
             qds(i)%IPIVV,qds(i)%fv,m_size_v,INFO)
        call DGEMV ('N',m_size_u,m_size_v,-1.d0,qds(i)%SV,&
             m_size_u,qds(i)%v,1,1.d0,qds(i)%fu,1)
        CALL DGETRS('N',m_size_u,1,qds(i)%MU,m_size_u,&
             qds(i)%IPIVU,qds(i)%fu,m_size_u,INFO)
     end do

     !$OMP PARALLEL DO PRIVATE(I,IVAR,K,L)
     do i = 1,num_quads
        do ivar = 1,nvar
           do l = 0,q_u
              do k = 0,q_u
                 ku(k,l,ivar,3,i) = dt*qds(i)%fu(k,l,ivar)
                 qds(i)%u(k,l,ivar) = um(k,l,ivar,i) + ku(k,l,ivar,3,i)
              end do
           end do
           do l = 0,q_v
              do k = 0,q_v
                 kv(k,l,ivar,3,i) = dt*qds(i)%fv(k,l,ivar)
                 qds(i)%v(k,l,ivar) = vm(k,l,ivar,i) + kv(k,l,ivar,3,i)
              end do
           end do
        end do
     end do

     ! Increment time
     t = t + 0.5d0*dt
     ! Stage 4. Ditto above.

     !$OMP PARALLEL DO PRIVATE(I)
     do i = 1,num_quads
        call compute_my_face_states(qds(i),nint,chebmat,dchebmat)
     end do
     call compute_outside_face_states(qds,num_quads,t)

     !$OMP PARALLEL DO PRIVATE(I)
     do i = 1,num_quads
        call compute_surface_integrals(qds(i),nint,chebmat,dchebmat,weights)
     end do

     !$OMP PARALLEL DO PRIVATE(I)
     do i = 1,num_quads
        ! Twilight
        qds(i)%fv = qds(i)%fv + cos(omega_0*t)*fvmms(:,:,:,i)  
        call DGEMV ('N',m_size_v,m_size_u,-1.d0,qds(i)%SU,m_size_v,&
             qds(i)%u,1,1.d0,qds(i)%fv,1)
        CALL DGETRS('N',m_size_v,1,qds(i)%MV,m_size_v,qds(i)%IPIVV,&
             qds(i)%fv,m_size_v,INFO)
        call DGEMV ('N',m_size_u,m_size_v,-1.d0,qds(i)%SV,m_size_u,&
             qds(i)%v,1,1.d0,qds(i)%fu,1)
        CALL DGETRS('N',m_size_u,1,qds(i)%MU,m_size_u,qds(i)%IPIVU,&
             qds(i)%fu,m_size_u,INFO)
     end do

     !$OMP PARALLEL DO PRIVATE(I,IVAR,K,L)
     do i = 1,num_quads
        do ivar = 1,nvar
           do l = 0,q_u
              do k = 0,q_u
                 ku(k,l,ivar,4,i) = dt*qds(i)%fu(k,l,ivar)
              end do
           end do
           do l = 0,q_v
              do k = 0,q_v
                 kv(k,l,ivar,4,i) = dt*qds(i)%fv(k,l,ivar)
              end do
           end do
        end do
     end do
     ! Sum up the stages.
     !$OMP PARALLEL DO PRIVATE(I,IVAR,K,L)
     do i = 1,num_quads 
        do ivar = 1,nvar
           do l = 0,q_u
              do k = 0,q_u
                 qds(i)%u(k,l,ivar) = um(k,l,ivar,i) + (1.d0/6.d0)*&
                      (ku(k,l,ivar,1,i)+2.d0*ku(k,l,ivar,2,i)&
                      +2.d0*ku(k,l,ivar,3,i)+ku(k,l,ivar,4,i))
              end do
           end do
           do l = 0,q_v
              do k = 0,q_v
                 qds(i)%v(k,l,ivar) = vm(k,l,ivar,i) + (1.d0/6.d0)*&
                      (kv(k,l,ivar,1,i)+2.d0*kv(k,l,ivar,2,i)&
                      +2.d0*kv(k,l,ivar,3,i)+kv(k,l,ivar,4,i))
              end do
           end do
        end do
     end do
     if(plot .and. ((mod(it,plot_freq).eq.0).or.(it.eq.nsteps))) then
        !call print_solution(qds,uplot,vplot,xplot,yplot,&
        !     chebmat_plot,q_u,q_v,num_quads,nplot,it,nvar)
        call get_refined_solution(qds,uplot,vplot,xplot,yplot,&
             chebmat_plot,q_u,q_v,num_quads,nplot,it,nvar)
        
        uplot(:,:,:,1) = uplot(:,:,:,1) &
             - amp_u1_0*cos(omega_0*t)*sin(kx_0*xplot+x_off_0)*sin(ky_0*yplot+y_off_0)
        uplot(:,:,:,2) = uplot(:,:,:,2) &
             - amp_u2_0*cos(omega_0*t)*sin(kx_0*xplot+x_off_0)*sin(ky_0*yplot+y_off_0)
        vplot(:,:,:,1) = vplot(:,:,:,1) &
             + omega_0*amp_u1_0*sin(omega_0*t)*sin(kx_0*xplot+x_off_0)*sin(ky_0*yplot+y_off_0)
        vplot(:,:,:,2) = vplot(:,:,:,2) &
             + omega_0*amp_u2_0*sin(omega_0*t)*sin(kx_0*xplot+x_off_0)*sin(ky_0*yplot+y_off_0)
        
        l2_err = 0.d0
        do ivar = 1,nvar
           do i = 1,num_quads
              integrand = 0.d0
              do l = 0,q_u
                 do k = 0,q_u
                    mc = qds(i)%u(k,l,ivar)-uexact(k,l,ivar,i)*cos(omega_0*t)
                    do iy = 1,nint
                       do ix = 1,nint
                          integrand(ix,iy) = integrand(ix,iy) &
                               + mc*chebmat(ix,k)*chebmat(iy,l)
                       end do
                    end do
                 end do
              end do
              ! Integrate in r for each s
              do iy = 1,nint
                 fint(iy) = sum(weights*qds(i)%jac(1:nint,iy)*integrand(1:nint,iy)**2)
              end do
              ! Then integrate in s
              l2_err = l2_err + abs(sum(weights*fint))
           end do
        end do
        write(29,'(5E12.4,2(X,I5,X),2I7.5)') &
             it*dt,h_eff,maxval(abs(vplot)),maxval(abs(uplot)),&
             sqrt(l2_err)/sqrt(l2_norm),q_u,q_v,it,nsteps
        write(*,'(5E12.4,2(X,I5,X),2I7.5)') &
             it*dt,h_eff,maxval(abs(vplot)),maxval(abs(uplot)),&
             sqrt(l2_err)/sqrt(l2_norm),q_u,q_v,it,nsteps
     end if
  end do
  close(29)
  deallocate(chebmat,dchebmat,vplot,uplot,xplot,yplot,um,vm,ku,kv,chebmat_plot)
  
end program dg_dath

subroutine compute_my_face_states(qd,nint,chebmat,dchebmat)
  use doublePrecision
  use quad_element
  use problemsetup, only: q_u,q_v
  implicit none
  integer :: nint
  type(quad) :: qd
  real(kind=dp) :: chebmat(nint,0:q_u),dchebmat(nint,0:q_u)
  real(kind=dp) :: mode_c
  real(kind=dp) :: ux_in(1:nint,4,qd%nvar),uy_in(1:nint,4,qd%nvar)
  integer :: k,l,m_u,m_v,nvar,ivar
  !
  ! This routine computes and assigns the interior face states
  ! for v, w_x and w_y.
  !
  ! We assume the modes are ordered in column major order:
  ! u_00, u_10, u_20,..., u_01,..., u_(q+1)(q+1).
  ! v_00, v_10, v_20,..., v_01,..., v_(q)(q).
  !
  
  ! Number of modes for u and v.
  m_u  = qd%q_u
  m_v  = qd%q_v
  nvar = qd%nvar
  
  qd%v_in = 0.d0
  qd%wx_in = 0.d0
  qd%wy_in = 0.d0
  
  do ivar = 1,nvar
     ! Assign the v states
     do l = 0,m_v
        do k = 0,m_v
           mode_c = qd%v(k,l,ivar)
           ! Face 1. s = 1, r \in [-1,1].
           qd%v_in(:,1,ivar) = qd%v_in(:,1,ivar) &
                + mode_c*chebmat(1:nint,k)*chebmat(nint,l)
           ! Face 2. r = -1, s \in [-1,1].
           qd%v_in(:,2,ivar) = qd%v_in(:,2,ivar) &
                + mode_c*chebmat(1,k)*chebmat(1:nint,l)
           ! Face 3. s = -1, r \in [-1,1].
           qd%v_in(:,3,ivar) = qd%v_in(:,3,ivar) &
                + mode_c*chebmat(1:nint,k)*chebmat(1,l)
           ! Face 4. r = 1, s \in [-1,1].
           qd%v_in(:,4,ivar) = qd%v_in(:,4,ivar) &
                + mode_c*chebmat(nint,k)*chebmat(1:nint,l)
        end do
     end do
  end do
  
  ! Compute u_{1,x}, u_{1,y} and u_{2,x}, u_{2,y}.
  do ivar = 1,nvar
     ux_in(:,:,ivar) = 0.d0
     uy_in(:,:,ivar) = 0.d0
     do l = 0,m_u
        do k = 0,m_u
           mode_c = qd%u(k,l,ivar)
           ! Recall w_x = r_x w_r + s_x w_s
           ! Face 1. s = 1, r \in [-1,1].
           ux_in(1:nint,1,ivar) = ux_in(1:nint,1,ivar) + mode_c*&
                (qd%rx(1:nint,nint)*dchebmat(1:nint,k)*chebmat(nint,l)&
                ! r_x w_r at s = 1.
                +qd%sx(1:nint,nint)*chebmat(1:nint,k)*dchebmat(nint,l))
           ! s_x w_s at s = 1.
           ! Face 2. r = -1, s \in [-1,1].
           ux_in(1:nint,2,ivar) = ux_in(1:nint,2,ivar) + mode_c*&
                (qd%rx(1,1:nint)*dchebmat(1,k)*chebmat(1:nint,l)&
                ! r_x w_r at r = -1.
                +qd%sx(1,1:nint)*chebmat(1,k)*dchebmat(1:nint,l))
           ! s_x w_s at r = -1.
           ! Face 3. s = -1, r \in [-1,1].
           ux_in(1:nint,3,ivar) = ux_in(1:nint,3,ivar) + mode_c*&
                (qd%rx(1:nint,1)*dchebmat(1:nint,k)*chebmat(1,l)&
                ! r_x w_r at s = -1.
                +qd%sx(1:nint,1)*chebmat(1:nint,k)*dchebmat(1,l))
           ! s_x w_s at s = -1.
           ! Face 4. r = 1, s \in [-1,1].
           ux_in(1:nint,4,ivar) = ux_in(1:nint,4,ivar) + mode_c*&
                (qd%rx(nint,1:nint)*dchebmat(nint,k)*chebmat(1:nint,l)&
                ! r_x w_r at r = 1.
                +qd%sx(nint,1:nint)*chebmat(nint,k)*dchebmat(1:nint,l))
           ! s_x w_s at r = 1.
           ! Ditto, w_y = r_y w_r +s_y w_s
           ! Face 1. s = 1, r \in [-1,1].
           uy_in(1:nint,1,ivar) = uy_in(1:nint,1,ivar) + mode_c*&
                (qd%ry(1:nint,nint)*dchebmat(1:nint,k)*chebmat(nint,l)&
                ! r_y w_r at s = 1.
                +qd%sy(1:nint,nint)*chebmat(1:nint,k)*dchebmat(nint,l))
           ! s_y w_s at s = 1.
           ! Face 2. r = -1, s \in [-1,1].
           uy_in(1:nint,2,ivar) = uy_in(1:nint,2,ivar) + mode_c*&
                (qd%ry(1,1:nint)*dchebmat(1,k)*chebmat(1:nint,l)&
                ! r_y w_r at r = -1.
                +qd%sy(1,1:nint)*chebmat(1,k)*dchebmat(1:nint,l))
           ! s_y w_s at r = -1.
           ! Face 3. s = -1, r \in [-1,1].
           uy_in(1:nint,3,ivar) = uy_in(1:nint,3,ivar) + mode_c*&
                (qd%ry(1:nint,1)*dchebmat(1:nint,k)*chebmat(1,l)&
                ! r_y w_r at s = -1.
                +qd%sy(1:nint,1)*chebmat(1:nint,k)*dchebmat(1,l))
           ! s_y w_s at s = -1.
           ! Face 4. r = 1, s \in [-1,1].
           uy_in(1:nint,4,ivar) = uy_in(1:nint,4,ivar) + mode_c*&
                (qd%ry(nint,1:nint)*dchebmat(nint,k)*chebmat(1:nint,l)&
                ! r_y w_r at r = 1.
                +qd%sy(nint,1:nint)*chebmat(nint,k)*dchebmat(1:nint,l))
           ! s_y w_s at r = 1.
        end do
     end do
  end do
  ! Side 1
  qd%wx_in(:,1,1) = (2.d0*qd%mu_arr(1:nint,nint)+qd%lam_arr(1:nint,nint))*ux_in(:,1,1) &
       + qd%lam_arr(1:nint,nint)*uy_in(:,1,2)
  qd%wy_in(:,1,1) = qd%mu_arr(1:nint,nint)*(uy_in(:,1,1) + ux_in(:,1,2))
  qd%wx_in(:,1,2) = qd%mu_arr(1:nint,nint)*(uy_in(:,1,1) + ux_in(:,1,2))
  qd%wy_in(:,1,2) = qd%lam_arr(1:nint,nint)*ux_in(:,1,1) &
       + (2.d0*qd%mu_arr(1:nint,nint)+qd%lam_arr(1:nint,nint))*uy_in(:,1,2)
  ! Side 2
  qd%wx_in(:,2,1) = (2.d0*qd%mu_arr(1,1:nint)+qd%lam_arr(1,1:nint))*ux_in(:,2,1) &
       + qd%lam_arr(1,1:nint)*uy_in(:,2,2)
  qd%wy_in(:,2,1) = qd%mu_arr(1,1:nint)*(uy_in(:,2,1) + ux_in(:,2,2))
  qd%wx_in(:,2,2) = qd%mu_arr(1,1:nint)*(uy_in(:,2,1) + ux_in(:,2,2))
  qd%wy_in(:,2,2) = qd%lam_arr(1,1:nint)*ux_in(:,2,1) &
       + (2.d0*qd%mu_arr(1,1:nint)+qd%lam_arr(1,1:nint))*uy_in(:,2,2)
  ! Side 3
  qd%wx_in(:,3,1) = (2.d0*qd%mu_arr(1:nint,1)+qd%lam_arr(1:nint,1))*ux_in(:,3,1) &
       + qd%lam_arr(1:nint,1)*uy_in(:,3,2)
  qd%wy_in(:,3,1) = qd%mu_arr(1:nint,1)*(uy_in(:,3,1) + ux_in(:,3,2))
  qd%wx_in(:,3,2) = qd%mu_arr(1:nint,1)*(uy_in(:,3,1) + ux_in(:,3,2))
  qd%wy_in(:,3,2) = qd%lam_arr(1:nint,1)*ux_in(:,3,1) &
       + (2.d0*qd%mu_arr(1:nint,1)+qd%lam_arr(1:nint,1))*uy_in(:,3,2)
  ! Side 4
  qd%wx_in(:,4,1) = (2.d0*qd%mu_arr(nint,1:nint)+qd%lam_arr(nint,1:nint))*ux_in(:,4,1) &
       + qd%lam_arr(nint,1:nint)*uy_in(:,4,2)
  qd%wy_in(:,4,1) = qd%mu_arr(nint,1:nint)*(uy_in(:,4,1) + ux_in(:,4,2))
  qd%wx_in(:,4,2) = qd%mu_arr(nint,1:nint)*(uy_in(:,4,1) + ux_in(:,4,2))
  qd%wy_in(:,4,2) = qd%lam_arr(nint,1:nint)*ux_in(:,4,1) &
       + (2.d0*qd%mu_arr(nint,1:nint)+qd%lam_arr(nint,1:nint))*uy_in(:,4,2)
  
end subroutine compute_my_face_states

subroutine compute_outside_face_states(qds,num_quads,t)
  use doublePrecision
  use problemsetup, only : nint, bc_al_be,compute_bc_forcing
  use quad_element
  implicit none
  integer :: num_quads
  type(quad) :: qds(num_quads)
  integer:: i,j,k,nbr,nbr_face,ivar,nvar
  real(kind = dp) :: t
  real(kind = dp) :: alpha_bc,beta_bc,v_in_loc,wn_in_loc,v_out_loc,wn_out_loc
  real(kind = dp) :: v_bc_force(nint),traction_bc_force(nint),x(nint),y(nint)
  nvar = qds(1)%nvar
  ! Now set the outside states.
  do i = 1,num_quads
     do ivar = 1,nvar
        ! Loop over the faces.
        do j = 1,4
           ! Interior boundary, supply outside states from
           ! the adjacent element.
           if(qds(i)%bc_type(j) .eq. 100) then
              nbr = qds(i)%nbr(j,1)
              nbr_face = qds(i)%nbr(j,2)
              if(qds(i)%nbr_orientation(j) .gt. 0)  then
                 do k = 1,nint
                    qds(i)%v_out(k,j,ivar) = qds(nbr)%v_in(k,nbr_face,ivar)
                    qds(i)%wx_out(k,j,ivar) = qds(nbr)%wx_in(k,nbr_face,ivar)
                    qds(i)%wy_out(k,j,ivar) = qds(nbr)%wy_in(k,nbr_face,ivar)
                 end do
              else
                 do k = 1,nint
                    qds(i)%v_out(k,j,ivar) = qds(nbr)%v_in(nint-(k-1),nbr_face,ivar)
                    qds(i)%wx_out(k,j,ivar) = qds(nbr)%wx_in(nint-(k-1),nbr_face,ivar)
                    qds(i)%wy_out(k,j,ivar) = qds(nbr)%wy_in(nint-(k-1),nbr_face,ivar)
                 end do
              end if
           else
              !
              ! Physical boundary conditions based on the
              ! types set in the problemsetup module.
              ! We need to compute v_out and wn_out
              ! to obtain wx_out and wy_out.
              !
              alpha_bc = bc_al_be(qds(i)%bc_type(j),1,ivar)
              beta_bc  = bc_al_be(qds(i)%bc_type(j),2,ivar)

              if (j.eq.1) then
                 x = qds(i)%x(1:nint,nint)
                 y = qds(i)%y(1:nint,nint)
              elseif(j.eq.2) then
                 x = qds(i)%x(1,1:nint)
                 y = qds(i)%y(1,1:nint)
              elseif(j.eq.3) then
                 x = qds(i)%x(1:nint,1)
                 y = qds(i)%y(1:nint,1)
              elseif(j.eq.4) then
                 x = qds(i)%x(nint,1:nint)
                 y = qds(i)%y(nint,1:nint)
              end if
              call compute_bc_forcing(v_bc_force,traction_bc_force,x,y,t,&
                   ivar,qds(i)%bc_type(j),nint)
              do k = 1,nint

                 v_in_loc  = qds(i)%v_in(k,j,ivar)
                 wn_in_loc = qds(i)%wx_in(k,j,ivar)*qds(i)%nx_in(k,j)&
                      +qds(i)%wy_in(k,j,ivar)*qds(i)%ny_in(k,j)
                 
                 v_out_loc  = 2.d0*v_bc_force(k) + v_in_loc &
                      - 2.d0*alpha_bc*(alpha_bc*v_in_loc+beta_bc*wn_in_loc)
                 ! The definition in the paper is based on the
                 ! normal from outside element
                 ! which is what we use here.
                 wn_out_loc = -2.d0*traction_bc_force(k) - wn_in_loc&
                      + 2.d0*beta_bc*(alpha_bc*v_in_loc+beta_bc*wn_in_loc)
                 
                 qds(i)%v_out(k,j,ivar)  = v_out_loc
                 ! We reverse the sign to account for the
                 ! normal from the adjacent element.
                 qds(i)%wx_out(k,j,ivar) = &
                      -cos(atan2(qds(i)%ny_in(k,j),qds(i)%nx_in(k,j)))*wn_out_loc
                 qds(i)%wy_out(k,j,ivar) = &
                      -sin(atan2(qds(i)%ny_in(k,j),qds(i)%nx_in(k,j)))*wn_out_loc
              end do
           end if
        end do
     end do
  end do

end subroutine compute_outside_face_states

subroutine compute_surface_integrals(qd,nint,chebmat,dchebmat,weights)
  use doublePrecision
  use quad_element
  use problemsetup, only : upwind,q_u,q_v,lam_0,mu_0
  
  implicit none
  integer :: nint
  type(quad) :: qd
  real(kind=dp) :: chebmat(nint,0:q_u),dchebmat(nint,0:q_u),weights(nint)
  real(kind = dp) :: vstar(nint),wxstar(nint),&
       wystar(nint),wstar_n(nint,4,2),vstar_minus_v(nint,4,2)
  real(kind = dp) :: x(nint),scale_jump
  integer :: j,k,l,ivar,nvar

  nvar = qd%nvar
  qd%fv = 0.d0
  qd%fu = 0.d0
  
  do ivar = 1,nvar
     ! Compute numerical fluxes!
     do j = 1,4
        ! Central part
        vstar = 0.5d0*(qd%v_in(:,j,ivar)+qd%v_out(:,j,ivar))
        wxstar = 0.5d0*(qd%wx_in(:,j,ivar)+qd%wx_out(:,j,ivar))
        wystar = 0.5d0*(qd%wy_in(:,j,ivar)+qd%wy_out(:,j,ivar))
        ! upwind part
        if (upwind) then
           ! Scale the jump term in the
           ! velocity equation to avoid numerical stiffness.
           scale_jump = 1.d0/((2.d0*mu_0+lam_0))
           vstar = vstar +  scale_jump*0.5d0*(&
                (qd%wx_out(:,j,ivar)-qd%wx_in(:,j,ivar))*qd%nx_in(:,j)&
                +(qd%wy_out(:,j,ivar)-qd%wy_in(:,j,ivar))*qd%ny_in(:,j))
           wxstar = wxstar + 0.5d0*qd%nx_in(:,j)*(qd%v_out(:,j,ivar)&
                -qd%v_in(:,j,ivar))
           wystar = wystar + 0.5d0*qd%ny_in(:,j)*(qd%v_out(:,j,ivar)&
                -qd%v_in(:,j,ivar))
        end if
        ! These are the actuall integrands.
        vstar_minus_v(:,j,ivar) = (vstar - qd%v_in(:,j,ivar))
        wstar_n(:,j,ivar) = (qd%nx_in(:,j)*wxstar + qd%ny_in(:,j)*wystar)
     end do
  end do
  
  ! We first compute the contribution from the
  do ivar = 1,nvar
     do l = 0,q_v
        do k = 0,q_v
           ! Face 1. s = 1, r \in [-1,1].
           qd%fv(k,l,ivar) = qd%fv(k,l,ivar) &
                +sum(weights*wstar_n(:,1,ivar)*qd%dl_face(:,1)&
                *chebmat(1:nint,k)*chebmat(nint,l))
           ! Face 2. r = -1, s \in [-1,1].
           qd%fv(k,l,ivar) = qd%fv(k,l,ivar) &
                +sum(weights*wstar_n(:,2,ivar)*qd%dl_face(:,2)&
                *chebmat(1,k)*chebmat(1:nint,l))
           ! Face 3. s = -1, r \in [-1,1].
           qd%fv(k,l,ivar) = qd%fv(k,l,ivar) &
                +sum(weights*wstar_n(:,3,ivar)*qd%dl_face(:,3)&
                *chebmat(1:nint,k)*chebmat(1,l))
           ! Face 4. r = 1, s \in [-1,1].
           qd%fv(k,l,ivar) = qd%fv(k,l,ivar) &
                +sum(weights*wstar_n(:,4,ivar)*qd%dl_face(:,4)&
                *chebmat(nint,k)*chebmat(1:nint,l))
        end do
     end do
  end do
  ! First equation
  ivar = 1
  do l = 0,q_u
     do k = 0,q_u
        ! Recall w_x = r_x w_r + s_x w_s
        ! Face 1. s = 1, r \in [-1,1].
        !
        qd%fu(k,l,ivar) = qd%fu(k,l,ivar) + &
             sum(weights*qd%dl_face(:,1)*&
             (vstar_minus_v(:,1,1)* &
             ((2.d0*qd%mu_arr(1:nint,nint)+qd%lam_arr(1:nint,nint))*&
             qd%nx_in(:,1)*&
             (qd%rx(1:nint,nint)*dchebmat(1:nint,k)*chebmat(nint,l) &
             +qd%sx(1:nint,nint)*chebmat(1:nint,k)*dchebmat(nint,l)) &
             +qd%mu_arr(1:nint,nint)*qd%ny_in(:,1)*&
             (qd%ry(1:nint,nint)*dchebmat(1:nint,k)*chebmat(nint,l)&
             +qd%sy(1:nint,nint)*chebmat(1:nint,k)*dchebmat(nint,l)))&
             + vstar_minus_v(:,1,2) * &
             (qd%lam_arr(1:nint,nint)&
             *qd%ny_in(:,1)*&
             (qd%rx(1:nint,nint)*dchebmat(1:nint,k)*chebmat(nint,l) &
             +qd%sx(1:nint,nint)*chebmat(1:nint,k)*dchebmat(nint,l)) &
             +qd%mu_arr(1:nint,nint)&
             *qd%nx_in(:,1)*&
             (qd%ry(1:nint,nint)*dchebmat(1:nint,k)*chebmat(nint,l)&
             +qd%sy(1:nint,nint)*chebmat(1:nint,k)*dchebmat(nint,l)))))
        ! Face 2. r = -1, s \in [-1,1].
        qd%fu(k,l,ivar) = qd%fu(k,l,ivar) + &
             sum(weights*qd%dl_face(:,2)*&
             (vstar_minus_v(:,2,1) * &
             ((2.d0*qd%mu_arr(1,1:nint)+qd%lam_arr(1,1:nint))*qd%nx_in(:,2)*&
             (qd%rx(1,1:nint)*dchebmat(1,k)*chebmat(1:nint,l) &
             +qd%sx(1,1:nint)*chebmat(1,k)*dchebmat(1:nint,l))&
             +qd%mu_arr(1,1:nint)*qd%ny_in(:,2)*&
             (qd%ry(1,1:nint)*dchebmat(1,k)*chebmat(1:nint,l)&
             +qd%sy(1,1:nint)*chebmat(1,k)*dchebmat(1:nint,l)))  &
             + vstar_minus_v(:,2,2) * &
             (qd%lam_arr(1,1:nint)*qd%ny_in(:,2)*&
             (qd%rx(1,1:nint)*dchebmat(1,k)*chebmat(1:nint,l) &
             +qd%sx(1,1:nint)*chebmat(1,k)*dchebmat(1:nint,l))&
             +qd%mu_arr(1,1:nint)*qd%nx_in(:,2)*&
             (qd%ry(1,1:nint)*dchebmat(1,k)*chebmat(1:nint,l)&
             +qd%sy(1,1:nint)*chebmat(1,k)*dchebmat(1:nint,l)))))
        ! Face 3. s = -1, r \in [-1,1].
        qd%fu(k,l,ivar) = qd%fu(k,l,ivar) + &
             sum(weights*qd%dl_face(:,3)*&
             (vstar_minus_v(:,3,1) * &
             ((2.d0*qd%mu_arr(1:nint,1)+qd%lam_arr(1:nint,1))*qd%nx_in(:,3)*&
             (qd%rx(1:nint,1)*dchebmat(1:nint,k)*chebmat(1,l)&
             +qd%sx(1:nint,1)*chebmat(1:nint,k)*dchebmat(1,l))&
             +qd%mu_arr(1:nint,1)*qd%ny_in(:,3)*&
             (qd%ry(1:nint,1)*dchebmat(1:nint,k)*chebmat(1,l)&
             +qd%sy(1:nint,1)*chebmat(1:nint,k)*dchebmat(1,l))) &
             + vstar_minus_v(:,3,2) * &
             (qd%lam_arr(1:nint,1)*qd%ny_in(:,3)*&
             (qd%rx(1:nint,1)*dchebmat(1:nint,k)*chebmat(1,l)&
             +qd%sx(1:nint,1)*chebmat(1:nint,k)*dchebmat(1,l))&
             +qd%mu_arr(1:nint,1)*qd%nx_in(:,3)*&
             (qd%ry(1:nint,1)*dchebmat(1:nint,k)*chebmat(1,l)&
             +qd%sy(1:nint,1)*chebmat(1:nint,k)*dchebmat(1,l)))))
        ! Face 4. r = 1, s \in [-1,1].
        qd%fu(k,l,ivar) = qd%fu(k,l,ivar) + &
             sum(weights*qd%dl_face(:,4)*&
             (vstar_minus_v(:,4,1) * &
             ((2.d0*qd%mu_arr(nint,1:nint)+qd%lam_arr(nint,1:nint))*qd%nx_in(:,4)*&
             (qd%rx(nint,1:nint)*dchebmat(nint,k)*chebmat(1:nint,l)&
             +qd%sx(nint,1:nint)*chebmat(nint,k)*dchebmat(1:nint,l))&
             +qd%mu_arr(nint,1:nint)*qd%ny_in(:,4)*&
             (qd%ry(nint,1:nint)*dchebmat(nint,k)*chebmat(1:nint,l)&
             +qd%sy(nint,1:nint)*chebmat(nint,k)*dchebmat(1:nint,l))) &
             + vstar_minus_v(:,4,2) * &
             (qd%lam_arr(nint,1:nint)*qd%ny_in(:,4)*&
             (qd%rx(nint,1:nint)*dchebmat(nint,k)*chebmat(1:nint,l)&
             +qd%sx(nint,1:nint)*chebmat(nint,k)*dchebmat(1:nint,l))&
             +qd%mu_arr(nint,1:nint)*qd%nx_in(:,4)*&
             (qd%ry(nint,1:nint)*dchebmat(nint,k)*chebmat(1:nint,l)&
             +qd%sy(nint,1:nint)*chebmat(nint,k)*dchebmat(1:nint,l)))))
     end do
  end do
  ivar = 2
  do l = 0,q_u
     do k = 0,q_u
        ! Recall w_x = r_x w_r + s_x w_s
        ! Face 1. s = 1, r \in [-1,1].
        !
        qd%fu(k,l,ivar) = qd%fu(k,l,ivar) + &
             sum(weights*qd%dl_face(:,1)*&
             (vstar_minus_v(:,1,2)* &
             (qd%mu_arr(1:nint,nint)*qd%nx_in(:,1)*&
             (qd%rx(1:nint,nint)*dchebmat(1:nint,k)*chebmat(nint,l) &
             +qd%sx(1:nint,nint)*chebmat(1:nint,k)*dchebmat(nint,l)) &
             +(2.d0*qd%mu_arr(1:nint,nint)+qd%lam_arr(1:nint,nint))*qd%ny_in(:,1)*&
             (qd%ry(1:nint,nint)*dchebmat(1:nint,k)*chebmat(nint,l)&
             +qd%sy(1:nint,nint)*chebmat(1:nint,k)*dchebmat(nint,l)))&
             + vstar_minus_v(:,1,1) * &
             (qd%mu_arr(1:nint,nint)*qd%ny_in(:,1)*&
             (qd%rx(1:nint,nint)*dchebmat(1:nint,k)*chebmat(nint,l) &
             +qd%sx(1:nint,nint)*chebmat(1:nint,k)*dchebmat(nint,l)) &
             +qd%lam_arr(1:nint,nint)*qd%nx_in(:,1)*&
             (qd%ry(1:nint,nint)*dchebmat(1:nint,k)*chebmat(nint,l)&
             +qd%sy(1:nint,nint)*chebmat(1:nint,k)*dchebmat(nint,l)))))
        ! Face 2. r = -1, s \in [-1,1].
        qd%fu(k,l,ivar) = qd%fu(k,l,ivar) + &
             sum(weights*qd%dl_face(:,2)*&
             (vstar_minus_v(:,2,2) * &
             (qd%mu_arr(1,1:nint)*qd%nx_in(:,2)*&
             (qd%rx(1,1:nint)*dchebmat(1,k)*chebmat(1:nint,l) &
             +qd%sx(1,1:nint)*chebmat(1,k)*dchebmat(1:nint,l))&
             +(2.d0*qd%mu_arr(1,1:nint)+qd%lam_arr(1,1:nint))*qd%ny_in(:,2)*&
             (qd%ry(1,1:nint)*dchebmat(1,k)*chebmat(1:nint,l)&
             +qd%sy(1,1:nint)*chebmat(1,k)*dchebmat(1:nint,l)))  &
             + vstar_minus_v(:,2,1) * &
             (qd%mu_arr(1,1:nint)*qd%ny_in(:,2)*&
             (qd%rx(1,1:nint)*dchebmat(1,k)*chebmat(1:nint,l) &
             +qd%sx(1,1:nint)*chebmat(1,k)*dchebmat(1:nint,l))&
             +qd%lam_arr(1,1:nint)*qd%nx_in(:,2)*&
             (qd%ry(1,1:nint)*dchebmat(1,k)*chebmat(1:nint,l)&
             +qd%sy(1,1:nint)*chebmat(1,k)*dchebmat(1:nint,l)))))
        ! Face 3. s = -1, r \in [-1,1].
        qd%fu(k,l,ivar) = qd%fu(k,l,ivar) + &
             sum(weights*qd%dl_face(:,3)*&
             (vstar_minus_v(:,3,2) * &
             (qd%mu_arr(1:nint,1)*qd%nx_in(:,3)*&
             (qd%rx(1:nint,1)*dchebmat(1:nint,k)*chebmat(1,l)&
             +qd%sx(1:nint,1)*chebmat(1:nint,k)*dchebmat(1,l))&
             +(2.d0*qd%mu_arr(1:nint,1)+qd%lam_arr(1:nint,1))*qd%ny_in(:,3)*&
             (qd%ry(1:nint,1)*dchebmat(1:nint,k)*chebmat(1,l)&
             +qd%sy(1:nint,1)*chebmat(1:nint,k)*dchebmat(1,l))) &
             + vstar_minus_v(:,3,1) * &
             (qd%mu_arr(1:nint,1)*qd%ny_in(:,3)*&
             (qd%rx(1:nint,1)*dchebmat(1:nint,k)*chebmat(1,l)&
             +qd%sx(1:nint,1)*chebmat(1:nint,k)*dchebmat(1,l))&
             +qd%lam_arr(1:nint,1)*qd%nx_in(:,3)*&
             (qd%ry(1:nint,1)*dchebmat(1:nint,k)*chebmat(1,l)&
             +qd%sy(1:nint,1)*chebmat(1:nint,k)*dchebmat(1,l)))))
        ! Face 4. r = 1, s \in [-1,1].
        qd%fu(k,l,ivar) = qd%fu(k,l,ivar) + &
             sum(weights*qd%dl_face(:,4)*&
             (vstar_minus_v(:,4,2) * &
             (qd%mu_arr(nint,1:nint)*qd%nx_in(:,4)*&
             (qd%rx(nint,1:nint)*dchebmat(nint,k)*chebmat(1:nint,l)&
             +qd%sx(nint,1:nint)*chebmat(nint,k)*dchebmat(1:nint,l))&
             +(2.d0*qd%mu_arr(nint,1:nint)+qd%lam_arr(nint,1:nint))*qd%ny_in(:,4)*&
             (qd%ry(nint,1:nint)*dchebmat(nint,k)*chebmat(1:nint,l)&
             +qd%sy(nint,1:nint)*chebmat(nint,k)*dchebmat(1:nint,l))) &
             + vstar_minus_v(:,4,1) * &
             (qd%mu_arr(nint,1:nint)*qd%ny_in(:,4)*&
             (qd%rx(nint,1:nint)*dchebmat(nint,k)*chebmat(1:nint,l)&
             +qd%sx(nint,1:nint)*chebmat(nint,k)*dchebmat(1:nint,l))&
             +qd%lam_arr(nint,1:nint)*qd%nx_in(:,4)*&
             (qd%ry(nint,1:nint)*dchebmat(nint,k)*chebmat(1:nint,l)&
             +qd%sy(nint,1:nint)*chebmat(nint,k)*dchebmat(1:nint,l)))))
     end do
  end do

  ! Fixup extra eq.
  qd%fu(qd%extra_eq_1(1),&
       qd%extra_eq_1(2),&
       qd%extra_eq_1(3)) = 0.d0
  qd%fu(qd%extra_eq_2(1),&
       qd%extra_eq_2(2),&
       qd%extra_eq_2(3)) = 0.d0
  qd%fu(qd%extra_eq_3(1),&
       qd%extra_eq_3(2),&
       qd%extra_eq_3(3)) = 0.d0

end subroutine compute_surface_integrals

subroutine set_initial_data(qd,nint)
  use doublePrecision
  use problemsetup, only: init_u,init_v,pis,q_u,q_v
  use quad_element
  use chebfuns
  implicit none
  type(quad) :: qd
  integer :: nint
  integer :: ix,iy,i,j,k,l,ivar
  real(kind=dp) :: xnodes(nint),weights(nint),fint(nint)
  real(kind=dp) :: x(nint,nint) ,y(nint,nint),chebmat(nint,0:q_u)
  real(kind=dp) :: flocu(nint,nint) ,flocv(nint,nint)
  real(kind=dp) :: pi1(2),pi2(2),pi3(2),pi4(2),pi2_m(2),pi2_p(2),pi4_m(2),pi4_p(2)
  real(kind=dp) :: xy_loc(2),xy_s(2),xy_e(2),eta,xi
  real(kind = dp), parameter :: pi = acos(-1.d0)

  ! Get initial conditions by projecting
  ! onto a Cheb expansion.
  do j = 1,nint
     xnodes(j) = -cos(pi*real(j-1,dp)/real(nint-1,dp))
     do k = 0,q_u
        chebmat(j,k)   = cheb(xnodes(j),k)
     end do
  end do
  weights(1:nint) = pi/real(nint-1,dp)
  weights(1) = 0.5d0*weights(1)
  weights(nint) = 0.5d0*weights(nint)

  ! Compute x-y coordinates
  ! Compute metric for elements on the boundary
  ! We use a Gordon-Hall mapping
  ! The soubroutine pis must contain the approproate information
  ! for the parametrization of the curvilinear elements
  !
  xy_s = qd%xy(3,1:2)
  xy_e = qd%xy(2,1:2)
  eta = 1.d0
  call pis(pi2_p,eta,xy_s,xy_e,qd%bc_type(2))
  eta = -1.d0
  call pis(pi2_m,eta,xy_s,xy_e,qd%bc_type(2))
  !
  xy_s = qd%xy(4,1:2)
  xy_e = qd%xy(1,1:2)
  eta = 1.d0
  call pis(pi4_p,eta,xy_s,xy_e,qd%bc_type(4))
  eta = -1.d0
  call pis(pi4_m,eta,xy_s,xy_e,qd%bc_type(4))
  !
  do iy = 1,nint
     eta = xnodes(iy)
     !
     xy_s = qd%xy(3,1:2)
     xy_e = qd%xy(2,1:2)
     call pis(pi2,eta,xy_s,xy_e,qd%bc_type(2))
     !
     xy_s = qd%xy(4,1:2)
     xy_e = qd%xy(1,1:2)
     call pis(pi4,eta,xy_s,xy_e,qd%bc_type(4))
     do ix = 1,nint
        xi  = xnodes(ix)
        !
        xy_s = qd%xy(2,1:2)
        xy_e = qd%xy(1,1:2)
        call pis(pi1,xi,xy_s,xy_e,qd%bc_type(1))
        !
        xy_s = qd%xy(3,1:2)
        xy_e = qd%xy(4,1:2)
        call pis(pi3,xi,xy_s,xy_e,qd%bc_type(3))
        xy_loc = (1.d0-eta)/2.d0*pi3+(1.d0+eta)/2.d0*pi1&
             +(1.d0-xi)/2.d0*(pi2-(1.d0+eta)/2.d0*pi2_p-(1.d0-eta)/2.d0*pi2_m)&
             +(1.d0+xi)/2.d0*(pi4-(1.d0+eta)/2.d0*pi4_p-(1.d0-eta)/2.d0*pi4_m)
        x(ix,iy) = xy_loc(1)
        y(ix,iy) = xy_loc(2)
     end do
  end do
  ! Now project
  do ivar = 1,qd%nvar
     do i = 1,nint
        do j = 1,nint
           flocu(i,j)  = init_u(x(i,j),y(i,j),ivar)
           flocv(i,j)  = init_v(x(i,j),y(i,j),ivar)
        end do
     end do
     do l = 0,q_u
        do k = 0,q_u
           do j = 1,nint
              fint(j) = sum(weights*flocu(1:nint,j)&
                   *chebmat(1:nint,k)*chebmat(j,l))
           end do
           qd%u(k,l,ivar) = sum(weights*fint)
           ! Normalize
           do j = 1,nint
              fint(j) = sum(weights*chebmat(1:nint,k)**2*chebmat(j,l)**2)
           end do
           qd%u(k,l,ivar) = qd%u(k,l,ivar)/(sum(weights*fint))
        end do
     end do
     do l = 0,q_v
        do k = 0,q_v
           do j = 1,nint
              fint(j) = sum(weights*flocv(1:nint,j)&
                   *chebmat(1:nint,k)*chebmat(j,l))
           end do
           qd%v(k,l,ivar) = sum(weights*fint)
           ! Normalize
           do j = 1,nint
              fint(j) = sum(weights*chebmat(1:nint,k)**2*chebmat(j,l)**2)
           end do
           qd%v(k,l,ivar) = qd%v(k,l,ivar)/(sum(weights*fint))
        end do
     end do
  end do
end subroutine set_initial_data

subroutine set_metric(qd,xnodes,diffmat,nint)
  use doublePrecision
  use quad_element
  use problemsetup, only: pis
  implicit none
  type(quad) :: qd
  integer :: nint
  integer :: ix,iy
  real(kind=dp) :: xnodes(nint)
  real(kind=dp) :: x_coord_elem(nint,nint) ,y_coord_elem(nint,nint),diffmat(nint,nint)
  real(kind=dp) :: pi1(2),pi2(2),pi3(2),pi4(2),pi2_m(2),pi2_p(2),pi4_m(2),pi4_p(2)
  real(kind=dp) :: xy_loc(2),xy_s(2),xy_e(2),eta,xi

  ! Compute metric
  ! We use a Gordon-Hall mapping
  ! The soubroutine pis must contain the approproate information
  ! for the parametrization of the curvilinear elements
  !
  xy_s = qd%xy(3,1:2)
  xy_e = qd%xy(2,1:2)
  eta = 1.d0
  call pis(pi2_p,eta,xy_s,xy_e,qd%bc_type(2))
  eta = -1.d0
  call pis(pi2_m,eta,xy_s,xy_e,qd%bc_type(2))
  !
  xy_s = qd%xy(4,1:2)
  xy_e = qd%xy(1,1:2)
  eta = 1.d0
  call pis(pi4_p,eta,xy_s,xy_e,qd%bc_type(4))
  eta = -1.d0
  call pis(pi4_m,eta,xy_s,xy_e,qd%bc_type(4))
  !
  do iy = 1,nint
     eta = xnodes(iy)
     !
     xy_s = qd%xy(3,1:2)
     xy_e = qd%xy(2,1:2)
     call pis(pi2,eta,xy_s,xy_e,qd%bc_type(2))
     !
     xy_s = qd%xy(4,1:2)
     xy_e = qd%xy(1,1:2)
     call pis(pi4,eta,xy_s,xy_e,qd%bc_type(4))
     do ix = 1,nint
        xi  = xnodes(ix)
        !
        xy_s = qd%xy(2,1:2)
        xy_e = qd%xy(1,1:2)
        call pis(pi1,xi,xy_s,xy_e,qd%bc_type(1))
        !
        xy_s = qd%xy(3,1:2)
        xy_e = qd%xy(4,1:2)
        call pis(pi3,xi,xy_s,xy_e,qd%bc_type(3))
        xy_loc = (1.d0-eta)/2.d0*pi3+(1.d0+eta)/2.d0*pi1&
             +(1.d0-xi)/2.d0*(pi2-(1.d0+eta)/2.d0*pi2_p-(1.d0-eta)/2.d0*pi2_m)&
             +(1.d0+xi)/2.d0*(pi4-(1.d0+eta)/2.d0*pi4_p-(1.d0-eta)/2.d0*pi4_m)
        x_coord_elem(ix,iy) = xy_loc(1)
        y_coord_elem(ix,iy) = xy_loc(2)
     end do
  end do

  qd%x = x_coord_elem
  qd%y = y_coord_elem
  
  call compute_curve_metric(qd%rx,qd%sx,qd%ry,qd%sy,qd%jac,&
       x_coord_elem,y_coord_elem,Diffmat,nint)
  ! Compute normals and line elements on all sides

  ! Face 1. corresponds to s = 1 and r \in [-1,1].
  ! Thus the normal is (s_x,s_y) / \sqrt(s_x^2+s_y^2).
  ! The line integral element is dl = \sqrt(x_r^2+y_r)^2 = J * \sqrt(s_x^2+s_y^2).
  ! Compute the norm of the metric.
  qd%dl_face(1:nint,1) = sqrt(qd%sx(1:nint,nint)**2+qd%sy(1:nint,nint)**2)
  ! Compute outward pointing unit normal.
  qd%nx_in(1:nint,1)   = qd%sx(1:nint,nint)/qd%dl_face(1:nint,1)
  qd%ny_in(1:nint,1)   = qd%sy(1:nint,nint)/qd%dl_face(1:nint,1)
  ! Scale by Jacobian to get the metric.
  qd%dl_face(1:nint,1) = qd%dl_face(1:nint,1)*qd%jac(1:nint,nint)

  ! Face 2. corresponds to r = -1 and s \in [-1,1].
  ! Thus the normal is (-r_x,-r_y) / \sqrt(r_x^2+r_y^2).
  ! The line integral element is dl = \sqrt(x_s^2+y_s)^2 = J * \sqrt(r_x^2+r_y^2).
  ! Compute the norm of the metric.
  qd%dl_face(1:nint,2) = sqrt(qd%rx(1,1:nint)**2+qd%ry(1,1:nint)**2)
  ! Compute outward pointing unit normal.
  qd%nx_in(1:nint,2)   = -qd%rx(1,1:nint)/qd%dl_face(1:nint,2)
  qd%ny_in(1:nint,2)   = -qd%ry(1,1:nint)/qd%dl_face(1:nint,2)
  ! Scale by Jacobian to get the metric.
  qd%dl_face(1:nint,2) = qd%dl_face(1:nint,2)*qd%jac(1,1:nint)

  ! Face 3. corresponds to s = -1 and r \in [-1,1].
  ! Thus the normal is (-s_x,-s_y) / \sqrt(s_x^2+s_y^2).
  ! The line integral element is dl = \sqrt(x_r^2+y_r)^2 = J * \sqrt(s_x^2+s_y^2).
  ! Compute the norm of the metric.
  qd%dl_face(1:nint,3) = sqrt(qd%sx(1:nint,1)**2+qd%sy(1:nint,1)**2)
  ! Compute outward pointing unit normal.
  qd%nx_in(1:nint,3)   = -qd%sx(1:nint,1)/qd%dl_face(1:nint,3)
  qd%ny_in(1:nint,3)   = -qd%sy(1:nint,1)/qd%dl_face(1:nint,3)
  ! Scale by Jacobian to get the metric.
  qd%dl_face(1:nint,3) = qd%dl_face(1:nint,3)*qd%jac(1:nint,1)

  ! Face 4. corresponds to r = 1 and s \in [-1,1].
  ! Thus the normal is (r_x,r_y) / \sqrt(r_x^2+r_y^2).
  ! The line integral element is dl = \sqrt(x_s^2+y_s)^2 = J * \sqrt(r_x^2+r_y^2).
  ! Compute the norm of the metric.
  qd%dl_face(1:nint,4) = sqrt(qd%rx(nint,1:nint)**2+qd%ry(nint,1:nint)**2)
  ! Compute outward pointing unit normal.
  qd%nx_in(1:nint,4)   = qd%rx(nint,1:nint)/qd%dl_face(1:nint,4)
  qd%ny_in(1:nint,4)   = qd%ry(nint,1:nint)/qd%dl_face(1:nint,4)
  ! Scale by Jacobian to get the metric.
  qd%dl_face(1:nint,4) = qd%dl_face(1:nint,4)*qd%jac(nint,1:nint)

end subroutine set_metric

subroutine Set_up_elements(qds,num_quads)
  use doublePrecision
  use quad_element
  use problemsetup
  implicit none
  type(quad), dimension(:), allocatable :: qds
  character(len=100) :: str
  integer :: i,j,k,l,myiostat
  integer :: num_nodes, node_nr,num_elem,num_bc_face,num_quads
  real(kind=dp), dimension(:,:), allocatable :: node_list
  integer, dimension(:,:), allocatable :: elem_list_all,quad_list, bc_face_list
  real(kind=dp), dimension(3) :: xyz_coord


  ! Process the grid
  open(23,file = trim(node_name))
  read(23,*) num_nodes
  write(*,*) "Grid has ", num_nodes, " nodes."
  allocate(node_list(num_nodes,2))
  do i=1,num_nodes
     read(23,*) node_nr, xyz_coord
     node_list(node_nr,1:2) = xyz_coord(1:2)
  end do
  close(23)
  ! Read elements
  open(23,file = trim(element_name))
  read(23,*) num_elem
  allocate(elem_list_all(num_elem,9))
  do i=1,num_elem
     READ(23, "(A)", IOSTAT=myIOStat) str
     READ(str, *, IOSTAT=myIOStat) elem_list_all(i,:)
  end do
  close(23)
  ! Count edges and quads
  num_bc_face = 0
  num_quads   = 0
  do i=1,num_elem
     if(elem_list_all(i,2).eq.1) num_bc_face = num_bc_face + 1
     if(elem_list_all(i,2).eq.3) num_quads   = num_quads + 1
  end do
  write(*,*) 'Grid has ', num_bc_face, ' boundary faces'
  write(*,*) 'Grid has ', num_quads, ' quads'
  allocate(quad_list(num_quads,10),bc_face_list(num_bc_face,4))
  ! quad: elem #, nodes 1-4 counter clock,
  ! the 4 elements that share nodes 1-2, 2-3, 3-4, 4-1.
  quad_list = -1
  k = 0
  l = 0
  do i=1,num_elem
     ! Faces
     if(elem_list_all(i,2).eq.1) then
        k = k+1
        bc_face_list(k,1) = k
        ! Type
        bc_face_list(k,2) = elem_list_all(i,4)
        ! node numbers
        bc_face_list(k,3:4) = elem_list_all(i,6:7)
     end if
     ! Quads
     if(elem_list_all(i,2).eq.3) then
        l = l + 1
        quad_list(l,1) = l
        quad_list(l,2:5) = elem_list_all(i,6:9)
     end if
  end do

  ! Now allocate and populate our list of elements
  allocate(qds(num_quads))

  ! Now set up connectivity info for each element
  ! Brute force ...
  ! We loop over all elements
  do i = 1,num_quads
     ! First allocate
     call allocate_quad(qds(i),q_u,q_v,nint,nvar)
     qds(i)%my_ind = i
     qds(i)%has_physical_bc = .false.
     qds(i)%bc_type(1:4) = 100 ! Interior

     ! Set the nodes of the element
     do k = 2,5
        qds(i)%xy(k-1,1:2) = node_list(quad_list(i,k),1:2)
     end do

     ! Each face is either interior or on the boundary
     ! if it is on the boundary it may be curved or straight
     do j = 1,num_quads
        if(i .ne. j) then
           ! Face one
           do k = 1,4
              if(quad_list(i,2).eq.quad_list(j,k+1)) then
                 do l = 1,4
                    if(k.ne.l) then
                       if(quad_list(i,3).eq.quad_list(j,l+1)) then
                          qds(i)%nbr(1,1) = quad_list(j,1)
                          if( (k.eq.2).and.(l.eq.1) )then
                             qds(i)%nbr(1,2) = 1
                             qds(i)%nbr_orientation(1) = -1
                          elseif( (k.eq.3).and.(l.eq.2) )then
                             qds(i)%nbr(1,2) = 2
                             qds(i)%nbr_orientation(1) = -1
                          elseif( (k.eq.4).and.(l.eq.3) )then
                             qds(i)%nbr(1,2) = 3
                             qds(i)%nbr_orientation(1) = 1
                          elseif( (k.eq.1).and.(l.eq.4) )then
                             qds(i)%nbr(1,2) = 4
                             qds(i)%nbr_orientation(1) = 1
                          else
                             write(*,*) 'Bad mesh?'
                             stop
                          end if
                       end if
                    end if
                 end do
              end if
           end do
           ! Face two
           do k = 1,4
              if(quad_list(i,3).eq.quad_list(j,k+1)) then
                 ! Found one node
                 do l = 1,4
                    if(k.ne.l) then
                       if(quad_list(i,4).eq.quad_list(j,l+1)) then
                          qds(i)%nbr(2,1) = quad_list(j,1)
                          if( (k.eq.2).and.(l.eq.1) )then
                             qds(i)%nbr(2,2) = 1
                             qds(i)%nbr_orientation(2) = -1
                          elseif( (k.eq.3).and.(l.eq.2) )then
                             qds(i)%nbr(2,2) = 2
                             qds(i)%nbr_orientation(2) = -1
                          elseif( (k.eq.4).and.(l.eq.3) )then
                             qds(i)%nbr(2,2) = 3
                             qds(i)%nbr_orientation(2) = 1
                          elseif( (k.eq.1).and.(l.eq.4) )then
                             qds(i)%nbr(2,2) = 4
                             qds(i)%nbr_orientation(2) = 1
                          else
                             write(*,*) 'Bad mesh?'
                             stop
                          end if
                       end if
                    end if
                 end do
              end if
           end do
           ! Face one
           do k = 1,4
              if(quad_list(i,4).eq.quad_list(j,k+1)) then
                 ! Found one node
                 do l = 1,4
                    if(k.ne.l) then
                       if(quad_list(i,5).eq.quad_list(j,l+1)) then
                          qds(i)%nbr(3,1) = quad_list(j,1)
                          !                          write(*,*) k,l
                          if( (k.eq.2).and.(l.eq.1) )then
                             qds(i)%nbr(3,2) = 1
                             qds(i)%nbr_orientation(3) = 1
                          elseif( (k.eq.3).and.(l.eq.2) )then
                             qds(i)%nbr(3,2) = 2
                             qds(i)%nbr_orientation(3) = 1
                          elseif( (k.eq.4).and.(l.eq.3) )then
                             qds(i)%nbr(3,2) = 3
                             qds(i)%nbr_orientation(3) = -1
                          elseif( (k.eq.1).and.(l.eq.4) )then
                             qds(i)%nbr(3,2) = 4
                             qds(i)%nbr_orientation(3) = -1
                          else
                             write(*,*) 'Bad mesh?'
                             stop
                          end if
                       end if
                    end if
                 end do
              end if
           end do
           ! Face one
           do k = 1,4
              if(quad_list(i,5).eq.quad_list(j,k+1)) then
                 ! Found one node
                 do l = 1,4
                    if(k.ne.l) then
                       if(quad_list(i,2).eq.quad_list(j,l+1)) then
                          qds(i)%nbr(4,1) = quad_list(j,1)
                          if( (k.eq.2).and.(l.eq.1) )then
                             qds(i)%nbr(4,2) = 1
                             qds(i)%nbr_orientation(4) = 1
                          elseif( (k.eq.3).and.(l.eq.2) )then
                             qds(i)%nbr(4,2) = 2
                             qds(i)%nbr_orientation(4) = 1
                          elseif( (k.eq.4).and.(l.eq.3) )then
                             qds(i)%nbr(4,2) = 3
                             qds(i)%nbr_orientation(4) = -1
                          elseif( (k.eq.1).and.(l.eq.4) )then
                             qds(i)%nbr(4,2) = 4
                             qds(i)%nbr_orientation(4) = -1
                          else
                             write(*,*) 'Bad mesh?'
                             stop
                          end if
                       end if
                    end if
                 end do
              end if
           end do
           ! Second pair
           do k = 2,5
              if(quad_list(i,3).eq.quad_list(j,k)) then
                 ! Found one node
                 do l = 2,5
                    if(k.ne.l) then
                       if(quad_list(i,4).eq.quad_list(j,l)) then
                          qds(i)%nbr(2,1) = quad_list(j,1)
                          if((k-1)*(l-1) .eq. 2)  qds(i)%nbr(2,2) = 1
                          if((k-1)*(l-1) .eq. 6)  qds(i)%nbr(2,2) = 2
                          if((k-1)*(l-1) .eq. 12) qds(i)%nbr(2,2) = 3
                          if((k-1)*(l-1) .eq. 4)  qds(i)%nbr(2,2) = 4
                       end if
                    end if
                 end do
              end if
           end do
           ! Third pair
           do k = 2,5
              if(quad_list(i,4).eq.quad_list(j,k)) then
                 ! Found one node
                 do l = 2,5
                    if(k.ne.l) then
                       if(quad_list(i,5).eq.quad_list(j,l)) then
                          qds(i)%nbr(3,1) = quad_list(j,1)
                          if((k-1)*(l-1) .eq. 2)  qds(i)%nbr(3,2) = 1
                          if((k-1)*(l-1) .eq. 6)  qds(i)%nbr(3,2) = 2
                          if((k-1)*(l-1) .eq. 12) qds(i)%nbr(3,2) = 3
                          if((k-1)*(l-1) .eq. 4)  qds(i)%nbr(3,2) = 4
                       end if
                    end if
                 end do
              end if
           end do
           ! Fourth pair
           do k = 2,5
              if(quad_list(i,5).eq.quad_list(j,k)) then
                 ! Found one node
                 do l = 2,5
                    if(k.ne.l) then
                       if(quad_list(i,2).eq.quad_list(j,l)) then
                          qds(i)%nbr(4,1) = quad_list(j,1)
                          if((k-1)*(l-1) .eq. 2)  qds(i)%nbr(4,2) = 1
                          if((k-1)*(l-1) .eq. 6)  qds(i)%nbr(4,2) = 2
                          if((k-1)*(l-1) .eq. 12) qds(i)%nbr(4,2) = 3
                          if((k-1)*(l-1) .eq. 4)  qds(i)%nbr(4,2) = 4
                       end if
                    end if
                 end do
              end if
           end do
        end if
     end do
     ! Now check against faces
     do j = 1,num_bc_face
        ! First pair
        do k = 3,4
           if(quad_list(i,2).eq.bc_face_list(j,k)) then
              ! Found one node
              do l = 3,4
                 if(k.ne.l) then
                    if(quad_list(i,3).eq.bc_face_list(j,l)) then
                       qds(i)%nbr(1,1) = -1
                       qds(i)%bc_type(1) = bc_face_list(j,2)
                       qds(i)%has_physical_bc = .true.
                    end if
                 end if
              end do
           end if
        end do
        ! Second pair
        do k = 3,4
           if(quad_list(i,3).eq.bc_face_list(j,k)) then
              ! Found one node
              do l = 3,4
                 if(k.ne.l) then
                    if(quad_list(i,4).eq.bc_face_list(j,l)) then
                       qds(i)%nbr(2,1) = -1
                       qds(i)%bc_type(2) = bc_face_list(j,2)
                       qds(i)%has_physical_bc = .true.
                    end if
                 end if
              end do
           end if
        end do
        ! Third pair
        do k = 3,4
           if(quad_list(i,4).eq.bc_face_list(j,k)) then
              ! Found one node
              do l = 3,4
                 if(k.ne.l) then
                    if(quad_list(i,5).eq.bc_face_list(j,l)) then
                       qds(i)%nbr(3,1) = -1
                       qds(i)%bc_type(3) = bc_face_list(j,2)
                       qds(i)%has_physical_bc = .true.
                    end if
                 end if
              end do
           end if
        end do
        ! Fourth pair
        do k = 3,4
           if(quad_list(i,5).eq.bc_face_list(j,k)) then
              ! Found one node
              do l = 3,4
                 if(k.ne.l) then
                    if(quad_list(i,2).eq.bc_face_list(j,l)) then
                       qds(i)%nbr(4,1) = -1
                       qds(i)%bc_type(4) = bc_face_list(j,2)
                       qds(i)%has_physical_bc = .true.
                    end if
                 end if
              end do
           end if
        end do
     end do
  end do
  deallocate(node_list)
  deallocate(quad_list,bc_face_list)
  deallocate(elem_list_all)

end subroutine Set_up_elements

subroutine compute_curve_metric(r_x,s_x,r_y,s_y,jac,X,Y,D,n)
  use doublePrecision
  implicit none
  integer :: n
  real(kind=dp), dimension(n,n) :: r_x,s_x,r_y,s_y,jac,X,Y,D
  real(kind=dp), dimension(n,n) :: x_r, x_s, y_r, y_s

  integer :: i
  !% Compute the derivatives w.r.t r & s
  do i = 1,n
     x_r(:,i) = matmul(D,X(:,i))
     y_r(:,i) = matmul(D,Y(:,i))
     x_s(i,:) = matmul(D,X(i,:))
     y_s(i,:) = matmul(D,Y(i,:))
  end do
  jac = x_r*y_s-y_r*x_s
  r_x =  y_s/jac
  r_y = -x_s/jac
  s_x = -y_r/jac
  s_y =  x_r/jac

end subroutine compute_curve_metric

subroutine xy_plot_grid(qd,x_plot,y_plot,nplot)
  use doublePrecision
  use quad_element
  use problemsetup, only: pis
  implicit none
  type(quad) :: qd
  integer :: nplot
  integer :: ix,iy
  real(kind=dp) :: xnodes(nplot)
  real(kind=dp) :: x_plot(nplot,nplot) ,y_plot(nplot,nplot)
  real(kind=dp) :: pi1(2),pi2(2),pi3(2),pi4(2),pi2_m(2),pi2_p(2),pi4_m(2),pi4_p(2)
  real(kind=dp) :: xy_loc(2),xy_s(2),xy_e(2),eta,xi

  do ix = 0,nplot-1
     xnodes(ix+1) = -1.d0 + 2.d0*dble(ix)/dble(nplot-1)
  end do

  ! Compute metric
  ! We use a Gordon-Hall mapping
  ! The soubroutine pis must contain the approproate information
  ! for the parametrization of the curvilinear elements
  !
  xy_s = qd%xy(3,1:2)
  xy_e = qd%xy(2,1:2)
  eta = 1.d0
  call pis(pi2_p,eta,xy_s,xy_e,qd%bc_type(2))
  eta = -1.d0
  call pis(pi2_m,eta,xy_s,xy_e,qd%bc_type(2))
  !
  xy_s = qd%xy(4,1:2)
  xy_e = qd%xy(1,1:2)
  eta = 1.d0
  call pis(pi4_p,eta,xy_s,xy_e,qd%bc_type(4))
  eta = -1.d0
  call pis(pi4_m,eta,xy_s,xy_e,qd%bc_type(4))
  !
  do iy = 1,nplot
     eta = xnodes(iy)
     !
     xy_s = qd%xy(3,1:2)
     xy_e = qd%xy(2,1:2)
     call pis(pi2,eta,xy_s,xy_e,qd%bc_type(2))
     !
     xy_s = qd%xy(4,1:2)
     xy_e = qd%xy(1,1:2)
     call pis(pi4,eta,xy_s,xy_e,qd%bc_type(4))
     do ix = 1,nplot
        xi  = xnodes(ix)
        !
        xy_s = qd%xy(2,1:2)
        xy_e = qd%xy(1,1:2)
        call pis(pi1,xi,xy_s,xy_e,qd%bc_type(1))
        !
        xy_s = qd%xy(3,1:2)
        xy_e = qd%xy(4,1:2)
        call pis(pi3,xi,xy_s,xy_e,qd%bc_type(3))
        xy_loc = (1.d0-eta)/2.d0*pi3+(1.d0+eta)/2.d0*pi1&
             +(1.d0-xi)/2.d0*(pi2-(1.d0+eta)/2.d0*pi2_p-(1.d0-eta)/2.d0*pi2_m)&
             +(1.d0+xi)/2.d0*(pi4-(1.d0+eta)/2.d0*pi4_p-(1.d0-eta)/2.d0*pi4_m)
        x_plot(ix,iy) = xy_loc(1)
        y_plot(ix,iy) = xy_loc(2)
     end do
  end do
end subroutine xy_plot_grid

subroutine get_refined_solution(qds,uplot,vplot,xplot,yplot,chebmat_plot&
     ,q_u,q_v,num_quads,nplot,it,nvar)
  use doublePrecision
  use quad_element
  implicit none
  type(quad) :: qds(num_quads)
  integer :: num_quads, nplot, q_u, q_v, nvar
  real(kind=dp) :: vplot(0:nplot-1,0:nplot-1,num_quads,nvar),&
       uplot(0:nplot-1,0:nplot-1,num_quads,nvar),&
       xplot(0:nplot-1,0:nplot-1,num_quads),&
       yplot(0:nplot-1,0:nplot-1,num_quads)
  real(kind=dp) :: chebmat_plot(0:nplot-1,0:q_u)
  real(kind=dp) :: mc
  integer :: i,l,k,iy,ix,it,ivar
  
  vplot = 0.d0
  uplot = 0.d0
  do ivar = 1,nvar
     do i = 1,num_quads
        do l = 0,q_u
           do k = 0,q_u
              mc = qds(i)%u(k,l,ivar)
              do iy = 0,nplot-1
                 do ix = 0,nplot-1
                    uplot(ix,iy,i,ivar) = uplot(ix,iy,i,ivar) &
                         + mc*chebmat_plot(ix,k)*chebmat_plot(iy,l)
                 end do
              end do
           end do
        end do
     end do
     do i = 1,num_quads
        do l = 0,q_v
           do k = 0,q_v
              mc = qds(i)%v(k,l,ivar)
              do iy = 0,nplot-1
                 do ix = 0,nplot-1
                    vplot(ix,iy,i,ivar) = vplot(ix,iy,i,ivar) &
                         + mc*chebmat_plot(ix,k)*chebmat_plot(iy,l)
                 end do
              end do
           end do
        end do
     end do
  end do
end subroutine get_refined_solution
