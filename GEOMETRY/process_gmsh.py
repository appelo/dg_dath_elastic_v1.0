import sys
import string

# find nodes and save them 
msh_f = open(sys.argv[1], 'r')
nodes_f = open('nodes.txt', 'w')
write_node = 0
for line in msh_f:
    if '$EndNodes' in line: 
        write_node = 0
        continue
    if write_node == 1:
         nodes_f.write(line)
    if '$Nodes' in line: 
        write_node = 1
        continue
msh_f.close()
nodes_f.close()
# find elements and save them
msh_f = open(sys.argv[1], 'r')
elements_f = open('elements.txt', 'w')
write_node = 0
for line in msh_f:
    if '$EndElements' in line: 
        write_node = 0
        continue
    if write_node == 1:
         elements_f.write(line)
    if '$Elements' in line: 
        write_node = 1
        continue
msh_f.close()
elements_f.close()

