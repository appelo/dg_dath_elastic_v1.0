if(2==2)
    m1 = 1;
    l1 = 1;
    r1 = 1;
    
    m2 = 2.0;
    l2 = 3.0;
    r2 = 1.98;
else
    m1 = 3;
    l1 = 3;
    r1 = 10;
    
    m2 = 1.;
    l2 = 1.;
    r2 = 1.;
end    
c = 0.99*sqrt(m1/r1);

alpha = -0.03;
for i = 1:100
    cp1 = sqrt((2*m1+l1)/r1);
    cs1 = sqrt(m1/r1);
    
    cp2 = sqrt((2*m2+l2)/r2);
    cs2 = sqrt(m2/r2);
    
    bp1 = sqrt(1-c^2/cp1^2);
    bs1 = sqrt(1-c^2/cs1^2);
    bp2 = sqrt(1-c^2/cp2^2);
    bs2 = sqrt(1-c^2/cs2^2);
    
    p4 = (r1/r2-1)^2-(r1/r2*bp2+bp1)*(r1/r2*bs2+bs1);
    p2 = 4*((r1/r2)*(cs1/cs2)^2-1)*(r1/r2*bp2*bs2-bp1*bs1-r1/r2+1);
    p0 = 4*((r1/r2)*(cs1/cs2)^2-1)^2*(bp1*bs1-1)*(bp2*bs2-1);
    cr = cs2*roots([p4 0 p2 0 p0]);
    [m,ind] = min(abs(c-real(cr)));
    c = (1-alpha)*c+alpha*abs(cr(ind));
    disp([m1,c cr(ind) m])
end

A = [bp1 bp2 -1i 1i
     1i -1i bs1 bs2
     -1i*2*m1*bp1 -1i*2*m2*bp2 -m1*(1+bs1^2) m2*(1+bs2^2)]; 
%     (-l1*c^2/cp1^2+2*m1*bp1^2) (l2*c^2/cp2^2+2*m2*bp2^2) -1i*2*m1*bs1 -1i*2*m2*bs2];

B = [-0.2952173626624*1i
     0.5220044931212*1i
     -0.6798795208473
     -0.9339639688697];
A*B

b = null(A);
b = b/b(4);
b = b/norm(b);
KVB = [b(1) b(3) b(2) b(4)]
