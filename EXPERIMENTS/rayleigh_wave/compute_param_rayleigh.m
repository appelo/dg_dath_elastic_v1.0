

lam = 1;
mu  = 0.0001;

dispersion = @(x,mu,lam) sqrt(1-x^2)*sqrt(1-mu*x^2/(lam+2*mu))-(1-x^2/2)^2;


dispersion(0.8,mu,lam)
options = optimset('TolX',1e-15);
xi = fzero(@(x) dispersion(x,mu,lam), 0.8,options)

dispersion(xi,mu,lam)