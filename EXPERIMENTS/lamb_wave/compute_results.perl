#!/usr/apps/bin/perl
#
# perl program to run convergence test for Lamb wave
use POSIX;

# Here is the generic file
$cmdFile="./problemsetup_lamb.f90.T";

for( $q=1; $q <= 13; $q = $q+1){
  
  $qu=$q;
  $qv=$q;
  
  open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
  open(OUTFILE,">./problemsetup_lamb.f90") || die "cannot open file $file!" ;
  # Setup the outfile
  while( <FILE> )
    {
      $_ =~ s/\bQQQU\b/$qu/;
      $_ =~ s/\bQQQV\b/$qv/;
      $_ =~ s/UPUPUP/.false./;
      print OUTFILE $_;
    }
  close( OUTFILE );
  close( FILE );
  # Compose the output file
  system("make clean -s ; make -s; rm -rf multivar.* c0* ; ./dg_dath.x");
  system("mv err.txt Lamberr-CENT-qu-$qu-qv-$qv-.txt");
}

for( $q=1; $q <= 13; $q = $q+1){
  
  $qu=$q;
  $qv=$q;
  
  open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
  open(OUTFILE,">./problemsetup_lamb.f90") || die "cannot open file $file!" ;
  # Setup the outfile
  while( <FILE> )
    {
      $_ =~ s/\bQQQU\b/$qu/;
      $_ =~ s/\bQQQV\b/$qv/;
      $_ =~ s/UPUPUP/.true./;
      print OUTFILE $_;
    }
  close( OUTFILE );
  close( FILE );
  # Compose the output file
  system("make clean -s ; make -s; rm -rf multivar.* c0* ; ./dg_dath.x");
  system("mv err.txt Lamberr-UP-qu-$qu-qv-$qv-.txt");
}

exit;
