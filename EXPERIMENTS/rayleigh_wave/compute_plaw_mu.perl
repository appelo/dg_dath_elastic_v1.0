#!/usr/apps/bin/perl
#
# perl program to run convergence test for the harmonic vibration in an annulus
use POSIX;

# Here is the generic file
$cmdFile="./problemsetup_rayleigh.f90.T2";

@mulist = ("1.d0","0.1d0","0.01d0","0.001d0","0.0001d0");

@xilist = ("9.194016867619661e-01","9.495540838881597e-01","9.547015634939026e-01","9.552510360428478e-01","9.553063507938881e-01");
@qlist = ("2","3","5");

for( $j=0; $j<5; $j++ ){
  $xi = $xilist[$j];
  $mu = $mulist[$j];
  for( $i=0; $i < 3; $i++){
    $q = $qlist[$i];
    for( $g=1; $g <= 8; $g = 2*$g){
      
      $qu=$q;
      $qv=$q;
      
      open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
      open(OUTFILE,">./problemsetup_rayleigh.f90") || die "cannot open file $file!" ;
      # Setup the outfile
      while( <FILE> )
	{
	  $_ =~ s/\bMUMUMU\b/$mu/;
	  $_ =~ s/\bXIXIXI\b/$xi/;
	  $_ =~ s/\bQQQU\b/$qu/;
	  $_ =~ s/\bQQQV\b/$qv/;
	  $_ =~ s/GGG/$g/;
	  print OUTFILE $_;
	}
      close( OUTFILE );
      close( FILE );
      # Compose the output file
      system("make clean -s ; make -s; rm -rf multivar.* c0* ; ./dg_dath.x");
      system("mv err.txt CPlawerr-mu-$mu-grid-$g-q-$qu-$qv-.txt");
    }
  }
}
exit;

