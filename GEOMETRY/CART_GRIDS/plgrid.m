
for i = 2:50
    clf
    elem = dlmread(['cart-' num2str(i) '-e.txt']);
    node = dlmread(['cart-' num2str(i) '-n.txt']);
    node(1,:) =[];
    for i = 1:length(elem(:,1));
        if(elem(i,4) == 100)
            x = [node(elem(i,6:9),2); node(elem(i,6),2)];
            y = [node(elem(i,6:9),3); node(elem(i,6),3)];
            plot(x,y,'k','linewidth',2)
            hold on
        end
    end
    axis equal
    axis([-0.1 1.1 -0.1 1.1])
    set(gca,'Fontsize',18)
    drawnow
    pause
end
