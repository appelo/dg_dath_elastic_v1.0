a = dir('CPlawerr-mu-1.d0-grid-*-q-2-2-.txt');
U = [];
for i = 1:length(a)
    u = load(a(i).name);
    U = [U ; [u(end,2) u(end,4)]];
end
M1 = sort(U,1);
P1 = 3./M1(:,1);

a = dir('CPlawerr-mu-0.1d0-grid-*-q-2-2-.txt');
U = [];
for i = 1:length(a)
    u = load(a(i).name);
    U = [U ; [u(end,2) u(end,4)]];
end
M01 = sort(U,1);
P01 = 3./M01(:,1);

a = dir('CPlawerr-mu-0.01d0-grid-*-q-2-2-.txt');
U = [];
for i = 1:length(a)
    u = load(a(i).name);
    U = [U ; [u(end,2) u(end,4)]];
end
M001 = sort(U,1);
P001 = 3./M001(:,1);

a = dir('CPlawerr-mu-0.001d0-grid-*-q-2-2-.txt');
U = [];
for i = 1:length(a)
    u = load(a(i).name);
    U = [U ; [u(end,2) u(end,4)]];
end
M0001 = sort(U,1);
P0001 = 3./M0001(:,1);

a = dir('CPlawerr-mu-0.0001d0-grid-*-q-2-2-.txt');
U = [];
for i = 1:length(a)
    u = load(a(i).name);
    U = [U ; [u(end,2) u(end,4)]];
end
M00001 = sort(U,1);
P00001 = 3./M00001(:,1);

figure(1)
loglog(P1,M1(:,2),'+-',P01,M01(:,2),'+-',P001,M001(:,2),'+-',P0001,M0001(:,2),'+-',P00001,M00001(:,2),'+-','linewidth',2)

set(gca,'Fontsize',18)
axis([2 110 1e-4 1])
legend('\mu = 1','\mu = 0.1','\mu = 0.01','\mu = 0.001','\mu = 0.0001')
xlabel('XXXXX')
ylabel('YYYYY')
title('ZZZZZ')

a = dir('CPlawerr-mu-1.d0-grid-*-q-3-3-.txt');
U = [];
for i = 1:length(a)
    u = load(a(i).name);
    U = [U ; [u(end,2) u(end,4)]];
end
M1 = sort(U,1);
P1 = 4./M1(:,1);

a = dir('CPlawerr-mu-0.1d0-grid-*-q-3-3-.txt');
U = [];
for i = 1:length(a)
    u = load(a(i).name);
    U = [U ; [u(end,2) u(end,4)]];
end
M01 = sort(U,1);
P01 = 4./M01(:,1);

a = dir('CPlawerr-mu-0.01d0-grid-*-q-3-3-.txt');
U = [];
for i = 1:length(a)
    u = load(a(i).name);
    U = [U ; [u(end,2) u(end,4)]];
end
M001 = sort(U,1);
P001 = 4./M001(:,1);

a = dir('CPlawerr-mu-0.001d0-grid-*-q-3-3-.txt');
U = [];
for i = 1:length(a)
    u = load(a(i).name);
    U = [U ; [u(end,2) u(end,4)]];
end
M0001 = sort(U,1);
P0001 = 4./M0001(:,1);

a = dir('CPlawerr-mu-0.0001d0-grid-*-q-3-3-.txt');
U = [];
for i = 1:length(a)
    u = load(a(i).name);
    U = [U ; [u(end,2) u(end,4)]];
end
M00001 = sort(U,1);
P00001 = 4./M00001(:,1);

figure(2)
loglog(P1,M1(:,2),'+-',P01,M01(:,2),'+-',P001,M001(:,2),'+-',P0001,M0001(:,2),'+-',P00001,M00001(:,2),'+-','linewidth',2)

axis([3 200 1e-6 1])
set(gca,'Fontsize',18)
legend('\mu = 1','\mu = 0.1','\mu = 0.01','\mu = 0.001','\mu = 0.0001')
xlabel('XXXXX')
ylabel('YYYYY')
title('ZZZZZ')

a = dir('CPlawerr-mu-1.d0-grid-*-q-5-5-.txt');
U = [];
for i = 1:length(a)
    u = load(a(i).name);
    U = [U ; [u(end,2) u(end,4)]];
end
M1 = sort(U,1);
P1 = 6./M1(:,1);

a = dir('CPlawerr-mu-0.1d0-grid-*-q-5-5-.txt');
U = [];
for i = 1:length(a)
    u = load(a(i).name);
    U = [U ; [u(end,2) u(end,4)]];
end
M01 = sort(U,1);
P01 = 6./M01(:,1);

a = dir('CPlawerr-mu-0.01d0-grid-*-q-5-5-.txt');
U = [];
for i = 1:length(a)
    u = load(a(i).name);
    U = [U ; [u(end,2) u(end,4)]];
end
M001 = sort(U,1);
P001 = 6./M001(:,1);

a = dir('CPlawerr-mu-0.001d0-grid-*-q-5-5-.txt');
U = [];
for i = 1:length(a)
    u = load(a(i).name);
    U = [U ; [u(end,2) u(end,4)]];
end
M0001 = sort(U,1);
P0001 = 6./M0001(:,1);

a = dir('CPlawerr-mu-0.0001d0-grid-*-q-5-5-.txt');
U = [];
for i = 1:length(a)
    u = load(a(i).name);
    U = [U ; [u(end,2) u(end,4)]];
end
M00001 = sort(U,1);
P00001 = 6./M00001(:,1);

figure(3)
loglog(P1,M1(:,2),'+-',P01,M01(:,2),'+-',P001,M001(:,2),'+-',P0001,M0001(:,2),'+-',P00001,M00001(:,2),'+-','linewidth',2)
axis([5 100 0.5e-6 0.1])
set(gca,'Fontsize',18)
legend('\mu = 1','\mu = 0.1','\mu = 0.01','\mu = 0.001','\mu = 0.0001')
xlabel('XXXXX')
ylabel('YYYYY')
title('ZZZZZ')

figure(1)
print -depsc2 rayleigh_errors_vary_mu_q_u_2.eps
figure(2)
print -depsc2 rayleigh_errors_vary_mu_q_u_3.eps
figure(3)
print -depsc2 rayleigh_errors_vary_mu_q_u_5.eps
