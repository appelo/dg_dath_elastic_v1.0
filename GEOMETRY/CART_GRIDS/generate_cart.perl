#!/usr/apps/bin/perl
#
# perl program to run convergence test for the harmonic vibration in an annulus
use POSIX;

# Here is the generic file
$cmdFile="./cart.geo.T";
      
for( $g=2; $g <= 50; $g = $g+1){
  open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
  open(OUTFILE,">./cart.geo") || die "cannot open file $file!" ;
    while( <FILE> )
      {
	$_ =~ s/\bGGG\b/$g/;
	print OUTFILE $_;
      }
  close( OUTFILE );
  close( FILE );
  system("/Applications/Gmsh.app/Contents/MacOS/gmsh cart.geo -2");
  system("python ../process_gmsh.py cart.msh");
  system("mv elements.txt cart-$g-e.txt"); 
  system("mv nodes.txt cart-$g-n.txt"); 
}
exit;
