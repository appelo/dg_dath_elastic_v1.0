#!/usr/apps/bin/perl
#
# perl program to run test for CFL dependence
# 
use POSIX;

# Here is the generic file
$cmdFile="./problemsetup_cavity_DKM_example.f90.T";

$upflux=".false.";
$type = "C";
for( $g=2; $g <= 4; $g++){
  for( $q=2; $q <= 9; $q = $q+1){
    for( $lam = 1; $lam <= 1024; $lam = 2*$lam){
      $qu=$q;
      $qv=$q;
      
      open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
      open(OUTFILE,">./problemsetup_cavity_DKM_example.f90") || die "cannot open file $file!" ;
      # Setup the outfile
      while( <FILE> )
	{
	  $_ =~ s/\bQQQU\b/$qu/;
	  $_ =~ s/\bQQQV\b/$qv/;
	  $_ =~ s/\bLAMLAM\b/$lam/;
	  $_ =~ s/\bUPFLUX\b/$upflux/;
	  $_ =~ s/GGGGGG/$g/;
	  print OUTFILE $_;
	}
      close( OUTFILE );
      close( FILE );
      # Compose the output file
      system("make clean -s ; make -s; ./dg_dath.x ; mv eigs.txt eigs-STRESS$type-lam-$lam-g-$g-q-$q.txt");
    }
  }
}

exit;
$upflux=".true.";
$type = "U";
for( $g=2; $g <= 4; $g++){
  for( $q=2; $q <= 9; $q = $q+1){
    for( $lam = 1; $lam <= 1024; $lam = 2*$lam){
      $qu=$q;
      $qv=$q;
      
      open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
      open(OUTFILE,">./problemsetup_cavity_DKM_example.f90") || die "cannot open file $file!" ;
      # Setup the outfile
      while( <FILE> )
	{
	  $_ =~ s/\bQQQU\b/$qu/;
	  $_ =~ s/\bQQQV\b/$qv/;
	  $_ =~ s/\bLAMLAM\b/$lam/;
	  $_ =~ s/\bUPFLUX\b/$upflux/;
	  $_ =~ s/GGGGGG/$g/;
	  print OUTFILE $_;
	}
      close( OUTFILE );
      close( FILE );
      # Compose the output file
      system("make clean -s ; make -s; ./dg_dath.x ; mv eigs.txt eigs-$type-lam-$lam-g-$g-q-$q.txt");
    }
  }
}
