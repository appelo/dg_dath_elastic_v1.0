%clear
%clf

elem = dlmread('mms_mesh_e.txt');
node = dlmread('mms_mesh_n.txt');
node(1,:) =[];

for i = 1:length(elem(:,1));
    if(elem(i,4) == 100)
        x = [node(elem(i,6:9),2); node(elem(i,6),2)];
        y = [node(elem(i,6:9),3); node(elem(i,6),3)];
        plot(x,y,'r','linewidth',2)
        hold on
    end
end
axis equal
axis([-2.1 2.1 -2.1 2.1])
set(gca,'Fontsize',18)