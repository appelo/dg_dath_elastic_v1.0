function void = daexact

y = 0;
x = 1.2;
t = 1;

aip = pi/3;
omega = 2*pi;

lam_a = 3; 
mu_a  = 2;
rho_a = 1;
lam_b = 1; 
mu_b  = 1;
rho_b = 1;

Vpa = sqrt((2*mu_a+lam_a)/rho_a);
Vpb = sqrt((2*mu_b+lam_b)/rho_b);
Vsa = sqrt(mu_a/rho_a);
Vsb = sqrt(mu_b/rho_b);
kpa = omega / Vpa;
kpb = omega / Vpb;
ksa = omega / Vsa;
ksb = omega / Vsb;


sip = sin(aip);
cip = sqrt(1-sip^2);

srp = sin(aip)/Vpa*Vpa;
crp = sqrt(1-srp^2);
srs = sin(aip)/Vpa*Vsa;
crs = sqrt(1-srs^2);
stp = sin(aip)/Vpa*Vpb;
ctp = sqrt(1-stp^2);
sts = sin(aip)/Vpa*Vsb;
cts = sqrt(1-sts^2);

uip = cos(kpa*(x*sip-y*cip)-omega*t)*[sip;-cip];
urp = cos(kpa*(x*srp+y*crp)-omega*t)*[srp; crp];
urs = cos(ksa*(x*srs+y*crs)-omega*t)*[crs;-srs];
utp = cos(kpb*(x*stp-y*ctp)-omega*t)*[stp;-ctp];
uts = cos(ksb*(x*sts-y*cts)-omega*t)*[cts; sts];

uipx = -kpa*sip*sin(kpa*(x*sip-y*cip)-omega*t)*[sip;-cip];
urpx = -kpa*srp*sin(kpa*(x*srp+y*crp)-omega*t)*[srp; crp];
ursx = -ksa*srs*sin(ksa*(x*srs+y*crs)-omega*t)*[crs;-srs];
utpx = -kpb*stp*sin(kpb*(x*stp-y*ctp)-omega*t)*[stp;-ctp];
utsx = -ksb*sts*sin(ksb*(x*sts-y*cts)-omega*t)*[cts; sts];

uipy =  kpa*cip*sin(kpa*(x*sip-y*cip)-omega*t)*[sip;-cip];
urpy = -kpa*crp*sin(kpa*(x*srp+y*crp)-omega*t)*[srp; crp];
ursy = -ksa*crs*sin(ksa*(x*srs+y*crs)-omega*t)*[crs;-srs];
utpy =  kpb*ctp*sin(kpb*(x*stp-y*ctp)-omega*t)*[stp;-ctp];
utsy =  ksb*cts*sin(ksb*(x*sts-y*cts)-omega*t)*[cts; sts];

b = [uip;
    (2*mu_a+lam_a)*uipy(2)+lam_a*uipx(1)
    mu_a*(uipy(1)+uipx(2))];

arp = [urp;
       (2*mu_a+lam_a)*urpy(2)+lam_a*urpx(1)
       mu_a*(urpy(1)+urpx(2))];
ars = [urs;
       (2*mu_a+lam_a)*ursy(2)+lam_a*ursx(1)
       mu_a*(ursy(1)+ursx(2))];
atp = [utp;
       (2*mu_b+lam_b)*utpy(2)+lam_b*utpx(1)
       mu_b*(utpy(1)+utpx(2))];
ats = [uts;
       (2*mu_b+lam_b)*utsy(2)+lam_b*utsx(1)
       mu_b*(utsy(1)+utsx(2))];

A = [-arp -ars atp ats]\b


