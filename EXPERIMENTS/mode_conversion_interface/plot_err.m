ColOrd = [0 0 0
          0 0 1.0000
          1.0000 0 0
          0 1.0000 0
          .7 .5 0 
          1.0000 0.1034 0.7241
          1.0000 0.8276 0
          0 0.3448 0
         ];
[m,n] = size(ColOrd);


figure(1)
clf
ii = 1;
E=[];
for i = [16 32 64]
    u = load(sprintf(['err-grid-%i-qu-3-qv-3-.txt'],i));
    k = ii;
    ColRow = rem(k,m);
    if ColRow == 0
        ColRow = m;
    end
    % Get the color
    Col = ColOrd(ColRow,:);
    semilogy(u(:,1),u(:,3),'color',Col,'linewidth',2)
    hold on
    ii = ii + 1;
    E = [E;u(end,3)];
end
set(gca,'fontsize',18)
xlabel('XXXXX')
ylabel('YYYYY')
E3 = E;
p3=log2(E(1:end-1)./E(2:end))

%figure(2)
%clf
ii = 1;
E=[];
for i = [8 16 32]
    u = load(sprintf(['err-grid-%i-qu-5-qv-5-.txt'],i));
    k = ii;
    ColRow = rem(k,m);
    if ColRow == 0
        ColRow = m;
    end
    % Get the color
    Col = ColOrd(ColRow,:);
    semilogy(u(:,1),u(:,3),'--','color',Col,'linewidth',2)
    hold on
    ii = ii + 1;
    E = [E;u(end,3)];
end
set(gca,'fontsize',18)
xlabel('XXXXX')
ylabel('YYYYY')
E5 = E;
p5=log2(E(1:end-1)./E(2:end))

figure(3)
clf
ii = 1;
E=[];
for i = [4 8 16]
    u = load(sprintf(['err-grid-%i-qu-7-qv-7-.txt'],i));
    k = ii;
    ColRow = rem(k,m);
    if ColRow == 0
        ColRow = m;
    end
    % Get the color
    Col = ColOrd(ColRow,:);
    semilogy(u(:,1),u(:,3),'color',Col,'linewidth',2)
    hold on
    ii = ii + 1;
    E = [E;u(end,3)];
end
E7 = E;
set(gca,'fontsize',18)
xlabel('XXXXX')
ylabel('YYYYY')
p7=log2(E(1:end-1)./E(2:end))

%figure(4)
%clf
ii = 1;
E=[];
for i = [4 8 16]
    u = load(sprintf(['err-grid-%i-qu-9-qv-9-.txt'],i));
    k = ii;
    ColRow = rem(k,m);
    if ColRow == 0
        ColRow = m;
    end
    % Get the color
    Col = ColOrd(ColRow,:);
    semilogy(u(:,1),u(:,3),'--','color',Col,'linewidth',2)
    hold on
    ii = ii + 1;
    E = [E;u(end,3)];
end
set(gca,'fontsize',18)
xlabel('XXXXX')
ylabel('YYYYY')
E9 = E;
p9=log2(E(1:end-1)./E(2:end))

D1 = [[16 32 64]' E3 [0;p3] [8 16 32]' E5 [0;p5]]
D2 = [[4 8 16]' E7 [0;p7] [4 8 16]' E9 [0;p9]]

disp(['\begin{table}[]'])
disp(['\caption{Errors and convergence rates for the mode conversion ' ...
      'example.  \label{Tab:mode_conversion}}'])
disp(['\begin{center}'])
disp(['\begin{tabular}{|c|c|c|c|c|c|}'])
disp(['\hline'])
disp(sprintf(['$q_u =3, N$ & $e_{2,u}(t=10)$ & rate & $q_u=5, N$ & $e_{2,u}(t=10)$ & rate   \\\\ '],1:7))
disp(['\hline'])
disp(sprintf(['%i & %3.2e & %3.2f & %i & %3.2e & %3.2f \\\\ '],D1(1,:)))
disp(sprintf(['%i & %3.2e & %3.2f & %i & %3.2e & %3.2f \\\\ '],D1(2,:)))
disp(sprintf(['%i & %3.2e & %3.2f & %i & %3.2e & %3.2f \\\\ '],D1(3,:)))
disp(['\hline'])
disp(['\hline'])
disp(sprintf(['$q_u =7, N$ & $e_{2,u}(t=10)$ & rate & $q_u=9, N$ & $e_{2,u}(t=10)$ & rate   \\\\ '],1:7))
disp(['\hline'])
disp(sprintf(['%i & %3.2e & %3.2f & %i & %3.2e & %3.2f \\\\ '],D2(1,:)))
disp(sprintf(['%i & %3.2e & %3.2f & %i & %3.2e & %3.2f \\\\ '],D2(2,:)))
disp(sprintf(['%i & %3.2e & %3.2f & %i & %3.2e & %3.2f \\\\ '],D2(3,:)))
disp(['\hline'])
disp(['\end{tabular}'])
disp(['\end{center}'])
disp(['\end{table} '])

figure(1)
axis([0 10 1e-12 1e-1])
legend('AA1','AA2','AA3','AA4','AA5','AA6','Orientation','Horizontal')

print -depsc2 mode_qu_3_5.eps

figure(3)
axis([0 10 1e-12 1e-1])
legend('AA1','AA2','AA3','AA4','AA5','AA6','Orientation','Horizontal')
print -depsc2 mode_qu_7_9.eps

