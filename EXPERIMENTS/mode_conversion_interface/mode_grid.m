clear
clf
% Number of cells on each of the four sides.
for nx = [1 2 4 8 16 32 64];
    ny = 2*nx;

    % Add the cartesian inner grid
    XC = zeros(nx+1,ny+1);
    YC = zeros(nx+1,ny+1);
    for i = 0:nx
        for j = 0:ny
            XC(1+i,1+j) =  -1 + 2*i/nx;
            YC(1+i,1+j) =  -2 + 4*j/ny;
        end
    end
    plot(XC,YC,'k',XC',YC','k')
    hold on
    % Add elements
    EX = [];
    EY = [];
    for i = 1:nx
        for j = 1:ny
            EX =[EX ;[XC(i,j) XC(i+1,j) XC(i+1,j+1) XC(i,j+1)]];
            EY =[EY ;[YC(i,j) YC(i+1,j) YC(i+1,j+1) YC(i,j+1)]];
        end
    end
    % plot(EX,EY,'r*')

    BOX = [];
    BOY = [];

    % Right bdry
    for j = 1:ny
        i = nx+1;
        BOX =[BOX ;[XC(i,j) XC(i,j+1)]];
        BOY =[BOY ;[YC(i,j) YC(i,j+1)]];
    end
    % Left bdry
    for j = 1:ny
        i = 1;
        BOX =[BOX ;[XC(i,j) XC(i,j+1)]];
        BOY =[BOY ;[YC(i,j) YC(i,j+1)]];
    end
    % Top bdry
    for i = 1:nx
        j = ny+1;
        BOX =[BOX ;[XC(i,j) XC(i+1,j)]];
        BOY =[BOY ;[YC(i,j) YC(i+1,j)]];
    end
    % Bottom bdry
    for i = 1:nx
        j = 1;
        BOX =[BOX ;[XC(i,j) XC(i+1,j)]];
        BOY =[BOY ;[YC(i,j) YC(i+1,j)]];
    end
    plot(BOX,BOY,'co')

    nodes = (nx+1)*(ny+1);
    xx = XC(:);
    yy = YC(:);
    for i = 1:nodes
        plot(xx(i),yy(i),'r*')
    end
    NODES = [(1:length(xx))' xx yy 0*xx];
    NELEMENTS = length(BOX(:,1)) + length(EX(:,1));
    Ntol = 1e-10;
    ii = 1;
    fid = fopen(sprintf('modegrid_%i_e.txt',nx),'wt');
    fprintf(fid,'%i \n',NELEMENTS);
    for kk = 10:11
        for i = 1:ny
            N = [];
            for j = 1:2
                IX = find(abs(BOX(ii,j)-NODES(:,2))< Ntol);
                IY = find(abs(BOY(ii,j)-NODES(:,3))< Ntol);
                for k = 1:length(IX)
                    I = find(IX(k) == IY);
                    if(isempty(I))
                    else
                        N = [N IY(I)];
                    end
                end
            end
            fprintf(fid,'%i %i %i %i %i %i %i \n',[ii 1 2 kk 2 N]);
            ii = ii+1;
        end
    end

    for kk = 12:13
        for i = 1:nx
            N = [];
            for j = 1:2
                IX = find(abs(BOX(ii,j)-NODES(:,2))< Ntol);
                IY = find(abs(BOY(ii,j)-NODES(:,3))< Ntol);
                for k = 1:length(IX)
                    I = find(IX(k) == IY);
                    if(isempty(I))
                    else
                        N = [N IY(I)];
                    end
                end
            end
            fprintf(fid,'%i %i %i %i %i %i %i \n',[ii 1 2 kk 2 N]);
            ii = ii+1;
        end
    end

    for i = 1:length(EX(:,1))
        N = [];
        for j = 1:4
            IX = find(abs(EX(i,j)-NODES(:,2))< Ntol);
            IY = find(abs(EY(i,j)-NODES(:,3))< Ntol);
            for k = 1:length(IX)
                I = find(IX(k) == IY);
                if(isempty(I))
                else
                    N = [N IY(I)];
                end
            end
        end
        disp([ii 3 2 100 6 N])
        fprintf(fid,'%i %i %i %i %i %i %i %i %i \n',[ii 3 2 100 6 N]);
        ii = ii+1;
    end
    fclose(fid);

    fid = fopen(sprintf('modegrid_%i_n.txt',nx),'wt');
    fprintf(fid,'%i \n',nodes);
    for i = 1:nodes
        fprintf(fid,'%i %26.16e %26.16e %26.16e\n',NODES(i,:));
    end
    fclose(fid)

end