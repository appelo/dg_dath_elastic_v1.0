clf
ri = 1/2; 
ro = 1;

% Number of cells on each of the four sides.
for nt = 20:4:80;
    clf
    nr = nt/4;
    dtheta = 2*pi/nt;
    
    X=[];
    Y=[];
    x = zeros(nr,1);
    y = zeros(nr,1);
    theta = pi + dtheta*(0:nt-1)';
    for i = 1:nt
        x0 = ri*cos(theta(i));
        y0 = ri*sin(theta(i));
        x1 = ro*cos(theta(i));
        y1 = ro*sin(theta(i));
        dx = (x1-x0)/(nr-1);
        dy = (y1-y0)/(nr-1);
        for j = 1:nr
            x(j) = x0 + (j-1)*dx;
            y(j) = y0 + (j-1)*dy;
        end
        X=[X x];
        Y=[Y y];
    end
    
    XX=[X X(:,1)];
    YY=[Y Y(:,1)];
    plot(XX,YY,'k',XX',YY','k')
    axis equal
    hold on

    EX = [];
    EY = [];
    for i = 1:length(X(1,:))
        for j = 1:nr-1
            EX =[EX ;[XX(j,i) XX(j+1,i) XX(j+1,i+1) XX(j,i+1)]];
            EY =[EY ;[YY(j,i) YY(j+1,i) YY(j+1,i+1) YY(j,i+1)]];
        end
    end
    
    plot(EX,EY,'r*')
    
    BIX = [];
    BIY = [];
    BOX = [];
    BOY = [];
    for i = 1:length(X(1,:))
        j = 1;
        BIX =[BIX ;[XX(j,i) XX(j,i+1)]];
        BIY =[BIY ;[YY(j,i) YY(j,i+1)]];
        j = nr;
        BOX =[BOX ;[XX(j,i) XX(j,i+1)]];
        BOY =[BOY ;[YY(j,i) YY(j,i+1)]];
    end
    plot(BIX,BIY,'bo',BOX,BOY,'co')

    nodes = length(X(:,1))*length(X(1,:));
    xx = X(:);
    yy = Y(:);
    for i = 1:nodes
        plot(xx(i),yy(i),'r*')
    end
    
    NODES = [(1:length(xx))' xx yy 0*xx];
    
    NELEMENTS = length(BIX(:,1)) + length(BOX(:,1)) + length(EX(:,1));
    
    Ntol = 1e-10;
    ii = 1;
    fid = fopen(sprintf('ann_%i_e.txt',nt),'wt');
    fprintf(fid,'%i \n',NELEMENTS);
    for i = 1:length(BIX(:,1))
        N = [];
        for j = 1:2
            IX = find(abs(BIX(i,j)-NODES(:,2))< Ntol);
            IY = find(abs(BIY(i,j)-NODES(:,3))< Ntol);
            for k = 1:length(IX)
                I = find(IX(k) == IY);
                if(isempty(I))
                else
                    N = [N IY(I)];
                end
            end
        end
        % disp([ii 1 2 11 1 N])
        fprintf(fid,'%i %i %i %i %i %i %i \n',[ii 1 2 11 1 N]);
        ii = ii+1;
    end
    for i = 1:length(BOX(:,1))
        N = [];
        for j = 1:2
            IX = find(abs(BOX(i,j)-NODES(:,2))< Ntol);
            IY = find(abs(BOY(i,j)-NODES(:,3))< Ntol);
            for k = 1:length(IX)
                I = find(IX(k) == IY);
                if(isempty(I))
                else
                    N = [N IY(I)];
                end
            end
        end
        %disp([ii 1 2 10 2 N])
        fprintf(fid,'%i %i %i %i %i %i %i \n',[ii 1 2 10 2 N]);
        ii = ii+1;
    end
    for i = 1:length(EX(:,1))
        N = [];
        for j = 1:4
            IX = find(abs(EX(i,j)-NODES(:,2))< Ntol);
            IY = find(abs(EY(i,j)-NODES(:,3))< Ntol);
            for k = 1:length(IX)
                I = find(IX(k) == IY);
                if(isempty(I))
                else
                    N = [N IY(I)];
                end
            end
        end
        disp([ii 3 2 100 6 N])
        fprintf(fid,'%i %i %i %i %i %i %i %i %i \n',[ii 3 2 100 6 N]);
        ii = ii+1;
    end
    fclose(fid);
    
    fid = fopen(sprintf('ann_%i_n.txt',nt),'wt');
    fprintf(fid,'%i \n',nodes);
    for i = 1:nodes
        fprintf(fid,'%i %26.16e %26.16e %26.16e\n',NODES(i,:));
    end
    fclose(fid)
    drawnow
    pause(0.1)
end
