clear

figure(1)
clf
a = dir('Rayerr-grid-*-q-3-3-.txt');
ColOrd = [0 0 0
          0 0 1.0000
          1.0000 0 0
          0 1.0000 0
          .7 .5 0 
          1.0000 0.1034 0.7241
          1.0000 0.8276 0
          0 0.3448 0
         ];
[m,n] = size(ColOrd);

for i = 1:length(a)
    u = load(a(i).name);
    k = i;
    ColRow = rem(k,m);
    if ColRow == 0
        ColRow = m;
    end
    % Get the color
    Col = ColOrd(ColRow,:);
    semilogy(u(2:end,1),u(2:end,4),'color',Col,'linewidth',2)
    hold on
end
set(gca,'fontsize',18)
xlabel('XXXXX')
ylabel('YYYYY')
legend('AAAAA','BBBBB','CCCCC')

figure(2)
clf
a = dir('Rayerr-grid-*-q-9-9-.txt');
ColOrd = [0 0 0
          0 0 1.0000
          1.0000 0 0
          0 1.0000 0
          .7 .5 0 
          1.0000 0.1034 0.7241
          1.0000 0.8276 0
          0 0.3448 0
         ];
[m,n] = size(ColOrd);

for i = 1:length(a)
    u = load(a(i).name);
    k = i;
    ColRow = rem(k,m);
    if ColRow == 0
        ColRow = m;
    end
    % Get the color
    Col = ColOrd(ColRow,:);
    semilogy(u(2:end,1),u(2:end,4),'color',Col,'linewidth',2)
    hold on
end
set(gca,'fontsize',18)
xlabel('XXXXX')
ylabel('YYYYY')
legend('AAAAA','BBBBB','CCCCC')

figure(1)
axis([0 20 1e-6 1])
print -depsc2 rayleigh_errors_q_u_3.eps

figure(2)
axis([0 20 1e-13 1e-6])
print -depsc2 rayleigh_errors_q_u_9.eps
