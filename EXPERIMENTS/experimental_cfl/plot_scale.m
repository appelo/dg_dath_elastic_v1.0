
q = 4;
LL = [];
ML = [];
figure(1),clf
for lam = [1 2 4 8 16 32 64 128 256 512 1024] ;
    LL =[LL ; lam];
    a = dir(sprintf('eigs-S1C-lam-%i-g-*-q-%i.txt',lam,q));
    M = [];
    for i = 1:length(a)
        u = load(a(i).name);
        plot(u(1,2)*u(:,3),u(1,2)*u(:,4),'*')
        hold on
        M = [M ; max(abs(u(1,2)*u(:,4)))];
    end
    axis equal
    ML = [ML ; max(M)];
end
set(gca,'fontsize',18)
xlabel('XXXXXX')
ylabel('YYYYYY')
print -depsc2 C_spectrum.eps

cp2 = 2+LL;
cs2 = 1;
figure(2),clf
semilogx(LL,ML./(sqrt(cp2)),'*-','linewidth',2),set(gca,'fontsize',18)
figure(3), clf
q = 4;
LL = [];
ML = [];
for lam = [1 2 4 8 16 32 64 128 256 512 1024] ;
    LL =[LL ; lam];
    a = dir(sprintf('eigs-SU-lam-%i-g-*-q-%i.txt',lam,q));
    M = [];
    for i = length(a)
        u = load(a(i).name);
        plot(u(1,2)*u(:,3),u(1,2)*u(:,4),'*')
        hold on
        M = [M ; max(abs(u(1,2)*sqrt(u(:,3).^2+u(:,4).^2)))];
    end
    ML = [ML ; max(M)];
end
axis equal
set(gca,'fontsize',18)
xlabel('XXXXXX')
ylabel('YYYYYY')
print -depsc2 US1_spectrum.eps


cp2 = 2+LL;
cs2 = 1;

figure(2)
hold on
semilogx(LL,ML./(sqrt(cp2)),'*-','linewidth',2),set(gca,'fontsize',18)
hold on
figure(4), clf
q = 4;
LL = [];
ML = [];
for lam = [1 2 4 8 16 32 64 128 256 512 1024] ;
    LL =[LL ; lam];
    a = dir(sprintf('eigs-S1U-lam-%i-g-*-q-%i.txt',lam,q));
    M = [];
    for i = length(a)
        u = load(a(i).name);
        plot(u(1,2)*u(:,3),u(1,2)*u(:,4),'*')
        hold on
        M = [M ; max(abs(u(1,2)*sqrt(u(:,3).^2+u(:,4).^2)))];
    end
    ML = [ML ; max(M)];
end
axis equal
set(gca,'fontsize',18)
xlabel('XXXXXX')
ylabel('YYYYYY')
print -depsc2 US2_spectrum.eps

cp2 = 2+LL;
cs2 = 1;
figure(2)
semilogx(LL,ML./(sqrt(cp2)),'*-','linewidth',2),set(gca,'fontsize',18)
hold on

figure(5), clf
q = 4;
LL = [];
ML = [];
for lam = [1 2 4 8 16 32 64 128 256 512 1024] ;
    LL =[LL ; lam];
    a = dir(sprintf('eigs-S2U-lam-%i-g-*-q-%i.txt',lam,q));
    M = [];
    for i = length(a)
        u = load(a(i).name);
        plot(u(1,2)*u(:,3),u(1,2)*u(:,4),'*')
        hold on
        M = [M ; max(abs(u(1,2)*sqrt(u(:,3).^2+u(:,4).^2)))];
    end
    ML = [ML ; max(M)];
end
axis equal
set(gca,'fontsize',18)
xlabel('XXXXXX')
ylabel('YYYYYY')
print -depsc2 US3_spectrum.eps

cp2 = 2+LL;
cs2 = 1;

figure(2)
semilogx(LL,ML./(sqrt(cp2)),'*-','linewidth',2),set(gca,'fontsize',18)
hold on
axis([0 1025 20 40])
xlabel('XXXXXX')
ylabel('YYYYYY')
legend AAAAAA1 AAAAAA2 AAAAAA3 AAAAAA4

print -depsc2 UPFlux_SCALING.eps