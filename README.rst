.. -*- rst -*- -*- restructuredtext -*-

.. This file should be written using the restructure text
.. conventions.  It will be displayed on the bitbucket source page and
.. serves as the documentation of the directory.


##################################################################################################################
dg\_dath\_elastic - Discontinuous Galerkin Solver by Daniel Appelo & Thomas Hagstrom for the Elastic Wave equation
##################################################################################################################

This repository contains the implementation used in the examples in
the paper *An energy-based discontinuous Galerkin discretization of the elastic wave equation in second order form*.

Disclamer
---------
This solver is a research code so expect the
documentation to be very brief. To use this code it is recommended
that you have some experience with Fortran and know how to use a
debugger. 

Requirements and general structure
----------------------------------

The code is requires you to install `Silo`__ in order to save the
solution and grid for later visualization in `Visit`__. As we
currently perform a direct factorization of the mass matrix you will
also need LAPACK. In later versions we plan to include a sparse
factorization. For most of the experiments you can use openMP to
enable shared memory parallelization. 

All the examples in the ``EXPERIMENTS`` directory are built up the same
way, there is a driver called ``dg_dath.f90`` and a file specifying
the problems setup called something like ``problemsetup_lamb.f90``.  

General purpose routines are stored in the ``SRC``
directory. The files residing there are:

 - ``assemble.f90``: Assembles the system approximating the elastic
   wave equation.  
 - ``chebfuns.f90``: Functions for the evaluating Chebyshev polynomials.
 - ``doubleprecision.f90``: Defining what we mean by double precision
   (exactly what you would expect).
 - ``silooutput.f90``: Routines for saving the solution.
 - ``lglnodes.f90``: A translation of `Greg von Winckel's matlab`__
   routine to compute nodes and weights for Legendre-Gauss-Lobatto
   quadrature.  
 - ``quad_element.f90``: Defines the type that describes quad
   elements. 
 - ``weights.f`` : `Bengt Fornberg's`__ ingenious and fast algorithm
   to compute the weights of finite difference formulas.  
  
__ https://wci.llnl.gov/simulation/computer-codes/silo/downloads/
__ https://wci.llnl.gov/simulation/computer-codes/visit/
__ http://www.mathworks.com/matlabcentral/profile/authors/869721-greg-von-winckel
__ https://amath.colorado.edu/faculty/fornberg/

Running the different experiments
---------------------------------

Most of the results in the paper can be reproduced by

 1. Generating the grids by a Matlab script or by GMSH
 2. Copying the service routines by doing ``make copy``.
 3. Producing the results by running a perl script that compiles and
    solves the problem at hand for some sequence of parameters.
 4. Postprocess the results into figures and tables by attached Matlab scripts. 

Time-step Constraints and Scaling of the Upwind Flux
----------------------------------------------------

In this experiment (located in the directory ``experimental_cfl`` and
described in Section 5.1 in the paper) the
results from the paper can be reproduced by running the two perl
scripts ``compute_experimental_cfl.perl`` and ``compute_scale.perl``
and followed by running the Matlab script ``plot_eigs.m`` and ``plot_scale.m``.

Examples illustrating the accuracy of the method for manufactured solutions
---------------------------------------------------------------------------
These experiments are described in Sections 5.2.1-5.2.4 in the paper.

Annulus experiment
------------------

To generate the grids run the script ``cyl_uni_grid.m`` in Matlab. The
grids will be stored in two files containing the nodes,
e.g. ``ann_20_n.txt``, and the elements, e.g. ``ann_20_e.txt``. The
first line in the file containing the nodes consists of an integer
indicating the number of nodes, followed by the coordinates of the
nodes. The file containing the elements starts with a line which
indicates how many elements there are followed by lists of the
elements. At some point we will add a more comprehensive description
of the format we use (which is essentially the same as in GMSH) but
for now we suggest that you take a look in the subroutine
``Set_up_elements`` residing in ``dg_dath.f90``.

In order to reproduce the results from the paper you can do
``make copy`` followed by ``perl perl compute_results_harmonic_annulus.perl``
and ``perl perl compute_results_harmonic_annulus_lam100.perl`` . This 
will generate errors that can be turned into figures and tables using
``get_errors.m`` and ``get_errors100.m``. 


Method of manufactured solutions
--------------------------------

The directories ``mms_cavity_harmonic``, ``mms_degree_flux`` contains
the experiments with manufactured solutions. In both cases the results
can be reproduced by invoking ``compute_degree_flux_results.perl`` followed by
``plot_err_deg_flux.m``.

   
Examples illustrating the accuracy of the method for analytical solutions
---------------------------------------------------------------------------

There are four experiments considering analytic solutions obtained in
simple geometries with piecewise constant material properties. The
four different problems are described by the names of the directories
where they reside:

  - ``rayleigh_wave`` Section 5.3.1
  - ``lamb_wave`` Section 5.3.2
  - ``mode_conversion_interface`` Section 5.3.3
  - ``stoneley_wave`` Section 5.3.4

For all of these the results can be recovered by first
ececuting the respective perl scripts and then producing the
post-processed output by running Matlab scripts.

Examples of applications 
-------------------------
