#!/usr/apps/bin/perl
#
# perl program to run convergence test for mode conversion
use POSIX;

# Here is the generic file
$cmdFile="./problemsetup_mode_conversion_example.f90.T";

if(1==2){
  $q=3;
  
  for( $g=16; $g <= 64; $g = 2*$g){
    
    $qu=$q;
    $qv=$q;
    
    open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
    open(OUTFILE,">./problemsetup_mode_conversion_example.f90") || die "cannot open file $file!" ;
    # Setup the outfile
    while( <FILE> )
      {
	$_ =~ s/\bQQQU\b/$qu/;
	$_ =~ s/\bQQQV\b/$qv/;
	$_ =~ s/GGG/$g/;
	print OUTFILE $_;
      }
    close( OUTFILE );
    close( FILE );
    # Compose the output file
    system("make clean -s ; make -s; rm -rf multivar.* c0* ; ./dg_dath.x");
    system("mv err.txt err-grid-$g-qu-$qu-qv-$qv-.txt");
  }
  $q=5;
  for( $g=8; $g <= 32; $g = 2*$g){
    
    $qu=$q;
    $qv=$q;
    
    open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
    open(OUTFILE,">./problemsetup_mode_conversion_example.f90") || die "cannot open file $file!" ;
    # Setup the outfile
    while( <FILE> )
      {
	$_ =~ s/\bQQQU\b/$qu/;
	$_ =~ s/\bQQQV\b/$qv/;
	$_ =~ s/GGG/$g/;
	print OUTFILE $_;
      }
    close( OUTFILE );
    close( FILE );
    # Compose the output file
    system("make clean -s ; make -s; rm -rf multivar.* c0* ; ./dg_dath.x");
    system("mv err.txt err-grid-$g-qu-$qu-qv-$qv-.txt");
  }
}
$q=7;
for( $g=4; $g <= 16; $g = 2*$g){
  
    $qu=$q;
    $qv=$q;
    
    open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
    open(OUTFILE,">./problemsetup_mode_conversion_example.f90") || die "cannot open file $file!" ;
    # Setup the outfile
    while( <FILE> )
      {
	$_ =~ s/\bQQQU\b/$qu/;
	$_ =~ s/\bQQQV\b/$qv/;
	$_ =~ s/GGG/$g/;
	print OUTFILE $_;
      }
    close( OUTFILE );
    close( FILE );
    # Compose the output file
    system("make clean -s ; make -s; rm -rf multivar.* c0* ; ./dg_dath.x");
    system("mv err.txt err-grid-$g-qu-$qu-qv-$qv-.txt");
  }

$q=9;
for( $g=4; $g <= 16; $g = 2*$g){
  
  $qu=$q;
  $qv=$q;
  
  open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
  open(OUTFILE,">./problemsetup_mode_conversion_example.f90") || die "cannot open file $file!" ;
  # Setup the outfile
  while( <FILE> )
    {
      $_ =~ s/\bQQQU\b/$qu/;
      $_ =~ s/\bQQQV\b/$qv/;
      $_ =~ s/GGG/$g/;
      print OUTFILE $_;
    }
  close( OUTFILE );
  close( FILE );
  # Compose the output file
  system("make clean -s ; make -s; rm -rf multivar.* c0* ; ./dg_dath.x");
  system("mv err.txt err-grid-$g-qu-$qu-qv-$qv-.txt");
}

exit;

