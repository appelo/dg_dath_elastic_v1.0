clear

slopes =zeros(2,7);
ColOrd = [0 0 0
          0 0 1.0000
          1.0000 0 0
          0 1.0000 0
          .7 .5 0
          1.0000 0.1034 0.7241
          1.0000 0.8276 0
          0 0.3448 0
         ];
[m,n] = size(ColOrd);
figure(1)
clf
for i = 2:3
  a = dir(sprintf(['err-up-.true.-lam-1.d0-qu-%i-qv-%i-grid-*.txt'],[i i-1]));
  k = i;
  ColRow = rem(k,m);
  if ColRow == 0
    ColRow = m;
  end
  % Get the color
  Col = ColOrd(ColRow,:);
  U = [];
  for j = 1:length(a)
    u = load(a(j).name);
    if(isempty(u))
    else
      U = [U; [u(end,8) u(end,4)]];
    end
  end
  U2 = sort(U,1);
  N = flipud((20:4:60)');
  h = 2*pi./N;
  loglog(h,U2(:,2),'d--','color',Col,'linewidth',2)
  hold on
  c = [ones(size(h)) log(h)]\log(U2(:,2));
  slopes(2,i) = c(2);
end

for i = 4:7
  a = dir(sprintf(['err-up-.true.-lam-1.d0-qu-%i-qv-%i-grid-*.txt'],[i i-1]));
  k = i;
  ColRow = rem(k,m);
  if ColRow == 0
    ColRow = m;
  end
  % Get the color
  Col = ColOrd(ColRow,:);
  U = [];
  for j = 1:length(a)
    u = load(a(j).name);
    if(isempty(u))
    else
      U = [U; [u(end,8) u(end,4)]];
    end
  end
  U2 = sort(U,1);
  N = flipud((20:4:40)');
  h = 2*pi./N;
  loglog(h,U2(:,2),'d--','color',Col,'linewidth',2)
  hold on
  c = [ones(size(h)) log(h)]\log(U2(:,2));
  slopes(2,i) = c(2);
end

for i = 2:3
  a = dir(sprintf(['err-up-.false.-lam-1.d0-qu-%i-qv-%i-grid-*.txt'],[i i-1]));
  k = i;
  ColRow = rem(k,m);
  if ColRow == 0
    ColRow = m;
  end
  % Get the color
  Col = ColOrd(ColRow,:);
  U = [];
  for j = 1:length(a)
    u = load(a(j).name);
    if(isempty(u))
    else
      U = [U; [u(end,8) u(end,4)]];
    end
  end
  U2 = sort(U,1);
  N = flipud((20:4:60)');
  h = 2*pi./N;
  loglog(h,U2(:,2),'d-','color',Col,'linewidth',2)
  hold on
  c = [ones(size(h)) log(h)]\log(U2(:,2));
  slopes(1,i) = c(2);
end

for i = 4:7
  a = dir(sprintf(['err-up-.false.-lam-1.d0-qu-%i-qv-%i-grid-*.txt'],[i i-1]));
  k = i;
  ColRow = rem(k,m);
  if ColRow == 0
    ColRow = m;
  end
  % Get the color
  Col = ColOrd(ColRow,:);
  U = [];
  for j = 1:length(a)
    u = load(a(j).name);
    if(isempty(u))
    else
      U = [U; [u(end,8) u(end,4)]];
    end
  end
  U2 = sort(U,1);
  N = flipud((20:4:40)');
  h = 2*pi./N;
  loglog(h,U2(:,2),'d-','color',Col,'linewidth',2)
  hold on
  c = [ones(size(h)) log(h)]\log(U2(:,2));
  slopes(1,i) = c(2);
end

set(gca,'fontsize',18)
legend('U21','U32','U43','U54','U65','U76',...
       'C21','C32','C43','C54','C65','C76','Location','NorthEast')
xlabel('XXXXX')
ylabel('YYYYY')
axis([0.1 0.5 1e-9 1])

disp(['\begin{table}[]'])
disp(['\caption{Harmonic oscillations of an annulus with $\lambda = 1, \, \mu = 1, \, \rho = 1$. Estimated rates of convergence for Central and ' ...
      'upwind flux using $q_u = q_v+1$.  \label{Tab:MMS_rates_diff_order}}'])
disp(['\begin{center}'])
disp(['\begin{tabular}{|l|c|c|c|c|c|c|c|}'])
disp(['\hline'])
disp(sprintf(['$q_u$ &  %i    & %i    & %i    & %i    & %i    & %i \\\\ '],2:7))
disp(sprintf(['C     & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f \\\\ '],slopes(1,2:7)))
disp(sprintf(['U     & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f \\\\ '],slopes(2,2:7)))
disp(['\hline'])
disp(['\end{tabular}'])
disp(['\end{center}'])
disp(['\end{table} '])

slopes =zeros(2,7);
ColOrd = [0 0 0
          0 0 1.0000
          1.0000 0 0
          0 1.0000 0
          .7 .5 0
          1.0000 0.1034 0.7241
          1.0000 0.8276 0
          0 0.3448 0
         ];
[m,n] = size(ColOrd);

figure(2)
clf

for i = 2:3
  a = dir(sprintf(['err-up-.true.-lam-1.d0-qu-%i-qv-%i-grid-*.txt'],[i i]));
  k = i;
  ColRow = rem(k,m);
  if ColRow == 0
    ColRow = m;
  end
  % Get the color
  Col = ColOrd(ColRow,:);
  U = [];
  for j = 1:length(a)
    u = load(a(j).name);
    if(isempty(u))
    else
      U = [U; [u(end,8) u(end,4)]];
    end
  end
  U2 = sort(U,1);
  N = flipud((20:4:60)');
  h = 2*pi./N;
  loglog(h,U2(:,2),'d--','color',Col,'linewidth',2)
  hold on
  c = [ones(size(h)) log(h)]\log(U2(:,2));
  slopes(2,i) = c(2);
end

for i = 4:7
  a = dir(sprintf(['err-up-.true.-lam-1.d0-qu-%i-qv-%i-grid-*.txt'],[i i]));
  k = i;
  ColRow = rem(k,m);
  if ColRow == 0
    ColRow = m;
  end
  % Get the color
  Col = ColOrd(ColRow,:);
  U = [];
  for j = 1:length(a)
    u = load(a(j).name);
    if(isempty(u))
    else
      U = [U; [u(end,8) u(end,4)]];
    end
  end
  U2 = sort(U,1);
  N = flipud((20:4:40)');
  h = 2*pi./N;
  loglog(h,U2(:,2),'d--','color',Col,'linewidth',2)
  hold on
  c = [ones(size(h)) log(h)]\log(U2(:,2));
  slopes(2,i) = c(2);
end

for i = 2:3
  a = dir(sprintf(['err-up-.false.-lam-1.d0-qu-%i-qv-%i-grid-*.txt'],[i i]));
  k = i;
  ColRow = rem(k,m);
  if ColRow == 0
    ColRow = m;
  end
  % Get the color
  Col = ColOrd(ColRow,:);
  U = [];
  for j = 1:length(a)
    u = load(a(j).name);
    if(isempty(u))
    else
      U = [U; [u(end,8) u(end,4)]];
    end
  end
  U2 = sort(U,1);
  N = flipud((20:4:60)');
  h = 2*pi./N;
  loglog(h,U2(:,2),'d-','color',Col,'linewidth',2)
  hold on
  c = [ones(size(h)) log(h)]\log(U2(:,2));
  slopes(1,i) = c(2);
end
for i = 4:7
  a = dir(sprintf(['err-up-.false.-lam-1.d0-qu-%i-qv-%i-grid-*.txt'],[i i]));
  k = i;
  ColRow = rem(k,m);
  if ColRow == 0
    ColRow = m;
  end
  % Get the color
  Col = ColOrd(ColRow,:);
  U = [];
  for j = 1:length(a)
    u = load(a(j).name);
    if(isempty(u))
    else
      U = [U; [u(end,8) u(end,4)]];
    end
  end
  U2 = sort(U,1);
  N = flipud((20:4:40)');
  h = 2*pi./N;
  loglog(h,U2(:,2),'d-','color',Col,'linewidth',2)
  hold on
  c = [ones(size(h)) log(h)]\log(U2(:,2));
  slopes(1,i) = c(2);
end

set(gca,'fontsize',18)
legend('U22','U33','U44','U55','U66','U77',...
       'C22','C33','C44','C55','C66','C77','Location','NorthEast')
xlabel('XXXXX')
ylabel('YYYYY')
axis([0.1 0.5 1e-9 1])

disp(['\begin{table}[]'])
disp(['\caption{Harmonic oscillations of an annulus with $\lambda = 1, \, \mu = 1, \, \rho = 1$. Estimated rates of convergence for Central and ' ...
      'upwind flux using $q_u = q_v$.  \label{Tab:MMS_rates_same_order}}'])
disp(['\begin{center}'])
disp(['\begin{tabular}{|l|c|c|c|c|c|c|c|}'])
disp(['\hline'])
disp(sprintf(['$q_u$ & %i    & %i    & %i    & %i    & %i    & %i \\\\ '],2:7))
disp(sprintf(['C     & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f \\\\ '],slopes(1,2:7)))
disp(sprintf(['U     & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f \\\\ '],slopes(2,2:7)))
disp(['\hline'])
disp(['\end{tabular}'])
disp(['\end{center}'])
disp(['\end{table} '])


figure(1)
axis([0.1 0.5 1e-9 1])
print -depsc2 ann_harmonic_deg_flux_different.eps

figure(2)
axis([0.1 0.5 1e-9 1])
print -depsc2 ann_harmonic_deg_flux_same.eps
