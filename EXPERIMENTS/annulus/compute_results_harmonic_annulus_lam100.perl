#!/usr/apps/bin/perl
#
# perl program to run convergence test for the harmonic vibration in an annulus
use POSIX;

# Here is the generic file
$cmdFile="./problemsetup_harmonic_annulus.f90.T";

$lam = "100.d0";
$mu  = "1.d0";
$alpha = "3.578028273880645d0";
$b0 = "2.524281875355463d0";
if(2==2){
$cfl = "0.1d0";
$upflux = ".true.";
for( $q=4; $q <= 7; $q = $q+1){
    for( $g=20; $g <= 40; $g = $g+4){
	open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
	open(OUTFILE,">./problemsetup_harmonic_annulus.f90") || die "cannot open file $file!" ;
	# Setup the outfile
	$qu = $q;
	$qv = $q-1;
	while( <FILE> )
	{
	    $_ =~ s/\bQQQU\b/$qu/;
	    $_ =~ s/\bQQQV\b/$qv/;
	    $_ =~ s/\bALPHA\b/$alpha/;
	    $_ =~ s/\bBBBB\b/$b0/;
	    $_ =~ s/\bLAMLAM\b/$lam/;
	    $_ =~ s/\bMUMU\b/$mu/;
                $_ =~ s/GGGG/$g/;
	    $_ =~ s/UPFLUX/$upflux/;
	    $_ =~ s/CCCFFFLLL/$cfl/;
	    print OUTFILE $_;
	}
	close( OUTFILE );
	close( FILE );
	# Compose the output file
	system("make clean -s ; make -s; ./dg_dath.x ; mv err.txt err-up-$upflux-lam-$lam-qu-$qu-qv-$qv-grid-$g.txt");
    }
}


$cfl = "1.5d0";

for( $q=2; $q <= 3; $q = $q+1){
    for( $g=20; $g <= 60; $g = $g+4){
	open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
	open(OUTFILE,">./problemsetup_harmonic_annulus.f90") || die "cannot open file $file!" ;
	# Setup the outfile
	$qu = $q;
	$qv = $q-1;
	while( <FILE> )
	{
	    $_ =~ s/\bQQQU\b/$qu/;
	    $_ =~ s/\bQQQV\b/$qv/;
	    $_ =~ s/\bALPHA\b/$alpha/;
	    $_ =~ s/\bBBBB\b/$b0/;
	    $_ =~ s/\bLAMLAM\b/$lam/;
	    $_ =~ s/\bMUMU\b/$mu/;
	    $_ =~ s/GGGG/$g/;
	    $_ =~ s/UPFLUX/$upflux/;
	    $_ =~ s/CCCFFFLLL/$cfl/;
	    print OUTFILE $_;
	}
	close( OUTFILE );
            close( FILE );
	# Compose the output file
	system("make clean -s ; make -s; ./dg_dath.x ; mv err.txt err-up-$upflux-lam-$lam-qu-$qu-qv-$qv-grid-$g.txt");
    }
}


$cfl = "0.1d0";
$upflux = ".false.";
for( $q=4; $q <= 7; $q = $q+1){
    for( $g=20; $g <= 40; $g = $g+4){
        open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
        open(OUTFILE,">./problemsetup_harmonic_annulus.f90") || die "cannot open file $file!" ;
        # Setup the outfile
        $qu = $q;
        $qv = $q-1;
        while( <FILE> )
        {
            $_ =~ s/\bQQQU\b/$qu/;
            $_ =~ s/\bQQQV\b/$qv/;
            $_ =~ s/\bALPHA\b/$alpha/;
            $_ =~ s/\bBBBB\b/$b0/;
            $_ =~ s/\bLAMLAM\b/$lam/;
            $_ =~ s/\bMUMU\b/$mu/;
            $_ =~ s/GGGG/$g/;
            $_ =~ s/UPFLUX/$upflux/;
            $_ =~ s/CCCFFFLLL/$cfl/;
            print OUTFILE $_;
        }
        close( OUTFILE );
        close( FILE );
        # Compose the output file
        system("make clean -s ; make -s; ./dg_dath.x ; mv err.txt err-up-$upflux-lam-$lam-qu-$qu-qv-$qv-grid-$g.txt");
    }
}

$cfl = "1.5d0";
for( $q=2; $q <= 3; $q = $q+1){
    for( $g=20; $g <= 60; $g = $g+4){
        open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
        open(OUTFILE,">./problemsetup_harmonic_annulus.f90") || die "cannot open file $file!" ;
        # Setup the outfile
        $qu = $q;
        $qv = $q-1;
        while( <FILE> )
        {
            $_ =~ s/\bQQQU\b/$qu/;
            $_ =~ s/\bQQQV\b/$qv/;
            $_ =~ s/\bALPHA\b/$alpha/;
            $_ =~ s/\bBBBB\b/$b0/;
            $_ =~ s/\bLAMLAM\b/$lam/;
            $_ =~ s/\bMUMU\b/$mu/;
            $_ =~ s/GGGG/$g/;
            $_ =~ s/UPFLUX/$upflux/;
            $_ =~ s/CCCFFFLLL/$cfl/;
            print OUTFILE $_;
        }
        close( OUTFILE );
        close( FILE );
        # Compose the output file
        system("make clean -s ; make -s; ./dg_dath.x ; mv err.txt err-up-$upflux-lam-$lam-qu-$qu-qv-$qv-grid-$g.txt");
    }
}

}

$cfl = "0.1d0";
$upflux = ".true.";
for( $q=4; $q <= 7; $q = $q+1){
    for( $g=20; $g <= 40; $g = $g+4){
        open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
        open(OUTFILE,">./problemsetup_harmonic_annulus.f90") || die "cannot open file $file!" ;
        # Setup the outfile
        $qu = $q;
        $qv = $q;
        while( <FILE> )
        {
            $_ =~ s/\bQQQU\b/$qu/;
            $_ =~ s/\bQQQV\b/$qv/;
            $_ =~ s/\bALPHA\b/$alpha/;
            $_ =~ s/\bBBBB\b/$b0/;
            $_ =~ s/\bLAMLAM\b/$lam/;
            $_ =~ s/\bMUMU\b/$mu/;
            $_ =~ s/GGGG/$g/;
            $_ =~ s/UPFLUX/$upflux/;
            $_ =~ s/CCCFFFLLL/$cfl/;
            print OUTFILE $_;
        }
        close( OUTFILE );
        close( FILE );
        # Compose the output file
        system("make clean -s ; make -s; ./dg_dath.x ; mv err.txt err-up-$upflux-lam-$lam-qu-$qu-qv-$qv-grid-$g.txt");
    }
}

$cfl = "1.5d0";

for( $q=2; $q <= 3; $q = $q+1){
    for( $g=20; $g <= 60; $g = $g+4){
        open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
        open(OUTFILE,">./problemsetup_harmonic_annulus.f90") || die "cannot open file $file!" ;
        # Setup the outfile
        $qu = $q;
        $qv = $q;
        while( <FILE> )
        {
            $_ =~ s/\bQQQU\b/$qu/;
            $_ =~ s/\bQQQV\b/$qv/;
            $_ =~ s/\bALPHA\b/$alpha/;
            $_ =~ s/\bBBBB\b/$b0/;
            $_ =~ s/\bLAMLAM\b/$lam/;
            $_ =~ s/\bMUMU\b/$mu/;
            $_ =~ s/GGGG/$g/;
            $_ =~ s/UPFLUX/$upflux/;
            $_ =~ s/CCCFFFLLL/$cfl/;
            print OUTFILE $_;
        }
        close( OUTFILE );
        close( FILE );
        # Compose the output file
        system("make clean -s ; make -s; ./dg_dath.x ; mv err.txt err-up-$upflux-lam-$lam-qu-$qu-qv-$qv-grid-$g.txt");
    }
}

$cfl = "0.1d0";
$upflux = ".false.";
for( $q=4; $q <= 7; $q = $q+1){
    for( $g=20; $g <= 40; $g = $g+4){
        open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
        open(OUTFILE,">./problemsetup_harmonic_annulus.f90") || die "cannot open file $file!" ;
        # Setup the outfile
        $qu = $q;
        $qv = $q;
        while( <FILE> )
        {
            $_ =~ s/\bQQQU\b/$qu/;
            $_ =~ s/\bQQQV\b/$qv/;
            $_ =~ s/\bALPHA\b/$alpha/;
            $_ =~ s/\bBBBB\b/$b0/;
            $_ =~ s/\bLAMLAM\b/$lam/;
            $_ =~ s/\bMUMU\b/$mu/;
            $_ =~ s/GGGG/$g/;
            $_ =~ s/UPFLUX/$upflux/;
            $_ =~ s/CCCFFFLLL/$cfl/;
            print OUTFILE $_;
        }
        close( OUTFILE );
        close( FILE );
        # Compose the output file
        system("make clean -s ; make -s; ./dg_dath.x ; mv err.txt err-up-$upflux-lam-$lam-qu-$qu-qv-$qv-grid-$g.txt");
    }
}

$cfl = "1.5d0";
for( $q=2; $q <= 3; $q = $q+1){
    for( $g=20; $g <= 60; $g = $g+4){
        open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
        open(OUTFILE,">./problemsetup_harmonic_annulus.f90") || die "cannot open file $file!" ;
        # Setup the outfile
        $qu = $q;
        $qv = $q;
        while( <FILE> )
        {
            $_ =~ s/\bQQQU\b/$qu/;
            $_ =~ s/\bQQQV\b/$qv/;
            $_ =~ s/\bALPHA\b/$alpha/;
            $_ =~ s/\bBBBB\b/$b0/;
            $_ =~ s/\bLAMLAM\b/$lam/;
            $_ =~ s/\bMUMU\b/$mu/;
            $_ =~ s/GGGG/$g/;
            $_ =~ s/UPFLUX/$upflux/;
            $_ =~ s/CCCFFFLLL/$cfl/;
            print OUTFILE $_;
        }
        close( OUTFILE );
        close( FILE );
        # Compose the output file
        system("make clean -s ; make -s; ./dg_dath.x ; mv err.txt err-up-$upflux-lam-$lam-qu-$qu-qv-$qv-grid-$g.txt");
    }
}
