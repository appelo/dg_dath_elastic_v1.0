ColOrd = [0 0 0
          0 0 1.0000
          1.0000 0 0
          0 1.0000 0
          .7 .5 0 
          1.0000 0.1034 0.7241
          1.0000 0.8276 0
          0 0.3448 0
         ];
[m,n] = size(ColOrd);

figure(1)
clf
ii = 1;
E=[];
for q = 1:10
    u = load(sprintf(['Lamberr-UP-qu-%i-qv-%i-.txt'],[q,q]));
    k = ii;
    ColRow = rem(k,m);
    if ColRow == 0
        ColRow = m;
    end
    % Get the color
    Col = ColOrd(ColRow,:);
    semilogy(u(:,1),u(:,5),'color',Col,'linewidth',2)
    hold on
    ii = ii + 1;
    E = [E;u(end,5)];
end
set(gca,'fontsize',18)
xlabel('XXXXX')
ylabel('YYYYY')

ii = 1;
EC=[];
for q = 1:10
    u = load(sprintf(['Lamberr-CENT-qu-%i-qv-%i-.txt'],[q,q]));
    k = ii;
    ColRow = rem(k,m);
    if ColRow == 0
        ColRow = m;
    end
    % Get the color
    Col = ColOrd(ColRow,:);
    semilogy(u(:,1),u(:,5),'--','color',Col,'linewidth',2)
    hold on
    ii = ii + 1;
    EC = [EC;u(end,5)];
end
set(gca,'fontsize',18)
xlabel('XXXXX')
ylabel('YYYYY')

figure(2)
semilogy(E,'k*','markersize',10)
hold on
semilogy(EC,'ks','markersize',10)
set(gca,'fontsize',18)

xlabel('XXXXX')
ylabel('YYYYY')

print -depsc2 lamb_error.eps
