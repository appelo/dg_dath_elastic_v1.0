function void = lamb  

dispersion(2*pi)




function f = dispersion(k)

omega = 13.137063197233;

d = 1/2;
lam = 2;
mu = 1;
rho = 1;

cs2 = mu/rho;
cp2 = (2*mu+lam)/rho;

p = sqrt(omega^2/cp2-k^2);
q = sqrt(omega^2/cs2-k^2);

f = tan(q*d)/tan(p*d)+4*k^2*p*q/(q^2-k^2)^2;

Q = 53.88807700007 * 2*mu*k*q*cos(q*d)/((lam*k^2+(2*mu+lam)*p^2)*cos(p*d))
QQ = 126.1992721468 