module problemSetup
  ! This module solves the elastic wave equation in a square cavity
  ! with manufactured solution according to example 5.2.1 in Duru, Kreiss & Mattsson
  !
  use doublePrecision
  implicit none
  integer, parameter :: nvar = 2
  integer, parameter :: q_u = 9
  integer, parameter :: q_v = 9
  
  real(kind = dp), parameter :: lam_0 =  1024
  real(kind = dp), parameter :: mu_0  =  1.d0
  ! Value v1 and v2
  
  integer, parameter :: nint = q_u+6
  real(kind = dp) :: bc_al_be(10:99,2,nvar)
  logical, parameter :: upwind = .false.
  
  ! A single element or four
  character(100), parameter :: element_name = '../../GEOMETRY/CART_GRIDS/cart-4-e.txt'
  character(100), parameter :: node_name    = '../../GEOMETRY/CART_GRIDS/cart-4-n.txt'
  
contains
  
  subroutine set_bc
    implicit none
    !
    ! This routine is used to set boundary conditions
    ! on boundary curve 10,11,...
    ! The boundary conditions are on the form
    ! \alpha v + \beta \Nabla u\cdot {\bf n} = 0,
    ! with \alpha^2+\beta^2 = 1.0
    !

    bc_al_be(:,1,:) = 1.d0  ! All Dirichlet
    bc_al_be(:,2,:) = sqrt(1.d0-bc_al_be(:,1,:)**2)

  end subroutine set_bc

  subroutine compute_bc_forcing(v_bc_force,traction_bc_force,x,y,t,&
       ivar,bc_number,nint)
    use doubleprecision
    implicit none
    integer  :: nint,ivar,bc_number
    real(kind = dp) :: v_bc_force(nint),traction_bc_force(nint),x(nint),y(nint),t

    traction_bc_force = 0.d0
    v_bc_force = 0.d0
  end subroutine compute_bc_forcing

  real(kind = dp) function init_u(x,y,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y
    if (ivar .eq. 1) then
       init_u = 0.d0
    else
       init_u = 0.d0
    end if
    return
  end function init_u

  real(kind = dp) function init_v(x,y,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y
    ! This is actually used for the mms forcing for v!
    if (ivar .eq. 1) then
       init_v = 0.d0
    else
       init_v = 0.d0
    end if
    return
  end function init_v

  subroutine pis(xy,s,xy_start,xy_end,curve_type)

    use doublePrecision
    implicit none
    real(kind=dp) :: xy(2),xy_start(2),xy_end(2),s
    real(kind=dp) :: theta_s, theta_e
    integer :: curve_type
    ! This problem only has straight lines...

    xy = xy_start + 0.5d0*(1.d0 + s)*(xy_end - xy_start)

  end subroutine pis

end module problemSetup
