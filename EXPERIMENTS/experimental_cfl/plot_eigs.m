if(1==1)
    figure(1), clf
    figure(2), clf
    for q = 2:9;
        LL = [];
        ML = [];
        for lam = [1 2 4 8 16 32 64 128 256 512 1024] ;
            LL =[LL ; lam];

            a = dir(sprintf('eigs-C-lam-%i-g-*-q-%i.txt',lam,q));
            M = [];
            for i = 1:length(a)
                u = load(a(i).name);
                M = [M ; max(abs(u(1,2)*u(:,4)))];
            end
            ML = [ML ; max(M)];
        end
        cp2 = 2+LL;
        cs2 = 1;
        figure(1)
        loglog(ML,(sqrt(cp2)*((q+1.5)*(q+1.5))),'*-')
        hold on
        figure(2)
        semilogx(LL,ML./(sqrt(cp2)*((q+1.5)*(q+1.5))),'*-','linewidth',2')
        hold on
    end
    
    figure(1)
    set(gca,'fontsize',18)
    xlabel('XXXXXX')
    ylabel('YYYYYY')
    print -depsc2 CENTRAL_CFL_COLLAPSE.eps

    figure(2)
    set(gca,'fontsize',18)
    xlabel('XXXXXX')
    ylabel('YYYYYY')
    legend 2 3 4 5 6 7 8 9        
    print -depsc2 CENTRAL_CFL_COLLAPSE2.eps
end
if(1==1)
    figure(3), clf
    figure(4), clf
    for q = 2:9;
        LL = [];
        ML = [];
        for lam = [1 2 4 8 16 32 64 128 256 512 1024] ;
            LL =[LL ; lam];

            a = dir(sprintf('eigs-U-lam-%i-g-*-q-%i.txt',lam,q));
            M = [];
            for i = 1:length(a)
                u = load(a(i).name);
                M = [M ; max(abs(u(1,2)*sqrt(u(:,3).^2+u(:,4).^2)))];
            end
            ML = [ML ; max(M)];
        end
        cp2 = 2+LL;
        cs2 = 1;
        figure(3)
        loglog(ML,(sqrt(cp2+cs2)*((q+1.5)*(q+1.5))),'*-')
        hold on
        figure(4)
        semilogx(LL,ML./(sqrt(cp2)*((q+1.5)*(q+1.5))),'*-','linewidth',2')
        hold on
    end
    figure(3)
    set(gca,'fontsize',18)
    xlabel('XXXXXX')
    ylabel('YYYYYY')
    print -depsc2 UPWIND_CFL_COLLAPSE.eps

    figure(4)
    set(gca,'fontsize',18)
    xlabel('XXXXXX')
    ylabel('YYYYYY')
    legend 2 3 4 5 6 7 8 9        
    print -depsc2 UPWIND_CFL_COLLAPSE2.eps

end
