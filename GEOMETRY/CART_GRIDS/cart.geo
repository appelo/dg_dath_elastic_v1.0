Point(1) = {0,0,0,0.1};
Point(2) = {0,1,0,0.1};
Point(3) = {1,1,0,0.1};
Point(4) = {1,0,0,0.1};

Line(1)={1,4};
Line(2)={4,3};
Line(3)={3,2};
Line(4)={2,1};


Line Loop(1) = {1,2,3,4} ;
Plane Surface(1) = {1};

// Set BC
// Straight lines 
MyLine = 10;
Physical Line(MyLine) = {1} ;
MyLine = 11;
Physical Line(MyLine) = {2} ;
MyLine = 12;
Physical Line(MyLine) = {3} ;
MyLine = 13;
Physical Line(MyLine) = {4} ;

MySurface = 100;
Physical Surface(MySurface) = {1} ;


// Tell Gmsh how many cells you want per edge
Transfinite Line{1,2,3,4} = 50 ;

// Tell Gmsh what the corner points are(going clockwise or counter-clockwise):
Transfinite Surface{1} = {1,4,3,2};

// Recombine the triangles into quads:
Recombine Surface{1};

// Havent tested this yet, but doesnt seem to hurt:
Mesh.Smoothing = 100;

