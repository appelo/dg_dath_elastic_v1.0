clear

slopes =zeros(2,7);
ColOrd = [0 0 0
          0 0 1.0000
          1.0000 0 0
          0 1.0000 0
          .7 .5 0 
          1.0000 0.1034 0.7241
          1.0000 0.8276 0
          0 0.3448 0
         ];
[m,n] = size(ColOrd);

figure(1)
clf
for l = 1:2
    for i = 1:7
        if(l == 1)
            a = dir(sprintf(['err-U-.false.-grid-*-q-%i-%i-.txt'],[i+1 i]));
        end
        if(l == 2)
            a = dir(sprintf(['err-U-.true.-grid-*-q-%i-%i-.txt'],[i+1 i]));
        end
        k = i;
        ColRow = rem(k,m);
        if ColRow == 0
            ColRow = m;
        end
        % Get the color
        Col = ColOrd(ColRow,:);
        U = [];
        for j = 1:length(a)
            u = load(a(j).name);
            U = [U; [u(end,9) u(end,5) u(end,2)]];
        end
        U2 = sort(U,1);
        h = U2(:,3);
        if(l == 1)
            loglog(h,U2(:,2),'d--','color',Col,'linewidth',2)
        else
            loglog(h,U2(:,2),'*-','color',Col,'linewidth',2)
        end
        hold on
        if(i == 5) 
            c = [ones(size(h(end-7:end-1))) log(h(end-7:end-1))]\log(U2(end-7:end-1,2));
        elseif(i == 6) 
            c = [ones(size(h(end-4:end-1))) log(h(end-4:end-1))]\log(U2(end-4:end-1,2));
        elseif(i == 7) 
            c = [ones(size(h(end-2:end))) log(h(end-2:end))]\log(U2(end-2:end,2));
        else
            c = [ones(size(h(1:end-1))) log(h(1:end-1))]\log(U2(1:end-1,2));
        end
        slopes(l,i) = c(2);
    end
end

set(gca,'fontsize',18)
legend('C21','C32','C43','C54','C65','C76','C87','U21','U32','U43','U54','U65','U76','U87','Location','NorthEast')
xlabel('XXXXX')
ylabel('YYYYY')
axis([0.01 4 1e-14 1])

disp(['\begin{table}[]'])
disp(['\caption{Estimated rates of convergence for Central and ' ...
      'upwind flux using $q_u = q_v+1$.  \label{Tab:MMS_rates_diff_order}}'])
disp(['\begin{center}'])
disp(['\begin{tabular}{|l|c|c|c|c|c|c|c|c|}'])
disp(['\hline'])
disp(sprintf(['$q_u$ & %i    & %i    & %i    & %i    & %i    & %i    & %i \\\\ '],2:8))
disp(sprintf(['C     & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f \\\\ '],slopes(1,:)))
disp(sprintf(['U     & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f \\\\ '],slopes(2,:)))
disp(['\hline'])
disp(['\end{tabular}'])
disp(['\end{center}'])
disp(['\end{table} '])

figure(2)
clf
slopes =zeros(2,7);
for l = 1:2
    for i = 1:7
        if(l == 1)
            a = dir(sprintf(['err-U-.false.-grid-*-q-%i-%i-.txt'],[i+1 i+1]));
        end
        if(l == 2)
            a = dir(sprintf(['err-U-.true.-grid-*-q-%i-%i-.txt'],[i+1 i+1]));
        end
        k = i;
        ColRow = rem(k,m);
        if ColRow == 0
            ColRow = m;
        end
        % Get the color
        Col = ColOrd(ColRow,:);
        U = [];
        for j = 1:length(a)
            u = load(a(j).name);
            U = [U; [u(end,9) u(end,5) u(end,2)]];
        end
        U2 = sort(U,1);
        h = U2(:,3);
        if(l == 1)
            loglog(h,U2(:,2),'d--','color',Col,'linewidth',2)
        else
            loglog(h,U2(:,2),'*-','color',Col,'linewidth',2)
        end
        hold on
        if(i == 5) 
            c = [ones(size(h(end-5:end-1))) log(h(end-5:end-1))]\log(U2(end-5:end-1,2));
        elseif(i == 6) 
            c = [ones(size(h(end-3:end-1))) log(h(end-3:end-1))]\log(U2(end-3:end-1,2));
        elseif(i == 7) 
            c = [ones(size(h(end-2:end-1))) log(h(end-2:end-1))]\log(U2(end-2:end-1,2));
        else
            c = [ones(size(h(1:end-1))) log(h(1:end-1))]\log(U2(1:end-1,2));
        end
        slopes(l,i) = c(2);
    end
end


set(gca,'fontsize',18)
legend('C22','C33','C44','C55','C66','C77','C88','U22','U33','U44','U55','U66','U77','U88','Location','NorthEast')
xlabel('XXXXX')
ylabel('YYYYY')
axis([0.01 4 1e-14 1])

disp(['\begin{table}[]'])
disp(['\caption{Estimated rates of convergence for Central and ' ...
      'upwind flux using $q_u = q_v$. \label{Tab:MMS_rates_same_order}}'])
disp(['\begin{center}'])
disp(['\begin{tabular}{|l|c|c|c|c|c|c|c|c|}'])
disp(['\hline'])
disp(sprintf(['$q_u$ & %i    & %i    & %i    & %i    & %i    & %i    & %i \\\\ '],2:8))
disp(sprintf(['C     & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f \\\\ '],slopes(1,:)))
disp(sprintf(['U     & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f & %3.2f \\\\ '],slopes(2,:)))
disp(['\hline'])
disp(['\end{tabular}'])
disp(['\end{center}'])
disp(['\end{table} '])

figure(1)
axis([0.01 4 1e-14 1])
print -depsc2 mms_harmonic_deg_flux_different.eps
figure(2)
axis([0.01 4 1e-14 1])
print -depsc2 mms_harmonic_deg_flux_same.eps





