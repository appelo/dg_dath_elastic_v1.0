subroutine assemble(qd,nint,chebmat,dchebmat,weights)
  use doublePrecision
  use quad_element
  use problemSetup, only: q_u,q_v
  implicit none
  integer :: nint
  type(quad) :: qd
  real(kind=dp) :: chebmat(nint,0:q_u),dchebmat(nint,0:q_u)
  real(kind=dp) :: fint(nint),weights(nint)
  real(kind=dp) :: metric12,metric21

  integer :: i,j,k,l,iy,m_u,m_v,row,col

  !
  ! This routine assembles the matrices MV,MU,SV,SU in the system
  !
  ! MU*[u1_t ; u2_t] + SV*[v1 ; v2] = "flux-stuff",
  ! MV*[v1_t ; v2_t] + SU*[u1 ; u2] = "flux-stuff".
  !
  ! We assume the modes are ordered in column major order with the "1" coordinate first:
  ! u_00,1, u_10,1, u_20,1,..., u_01,1,..., u_(q+1)(q+1),1, u_00,2, u_10,2, u_20,2,..., u_01,2,..., u_(q+1)(q+1),2.
  ! v_00,1, v_10,1, v_20,1,..., v_01,1,..., v_(q)(q),1, v_00,2, v_10,2, v_20,2,..., v_01,2,..., v_(q)(q),2.
  !
  ! Number of modes for u1,u2 and v1,v2.
  m_u = q_u
  m_v = q_v

  ! Assemble Mass and Stiffness matrices
  ! i,k is index in r. j,l is index in s.
  ! i,j for phi
  ! k,l for u,v

  ! MV is block diagonal.
  !
  ! MV = [MV1   0
  !         0 MV2]
  !
  ! MV1 = [ \int \phi^v_{i,j,1} \phi^v_{k,l,1}
  ! MV2 = [ \int \phi^v_{i,j,2} \phi^v_{k,l,2}
  qd%MV(:,:) = 0.d0
  ! First diagonal block
  ! Row index (eq. number)
  do j = 0,m_v
     do i = 0,m_v
        row = i+1 + j*(m_v+1)
        do l = 0,m_v
           do k = 0,m_v
              col = k + 1 + l*(m_v+1)
              ! Integrate in r for each s
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)&
                      *chebmat(1:nint,i)*chebmat(iy,j)& ! \phi^v-part, test
                      *chebmat(1:nint,k)*chebmat(iy,l)) !      v-part, trial
              end do
              ! Then integrate in s
              qd%MV(row,col) = sum(weights*fint)
           end do
        end do
     end do
  end do
  ! The next diagonal is identical so we simply copy it.
  do j = 0,m_v
     do i = 0,m_v
        row = i+1 + j*(m_v+1)
        do l = 0,m_v
           do k = 0,m_v
              col = k + 1 + l*(m_v+1)
              qd%MV((m_v+1)**2+row,(m_v+1)**2+col) = qd%MV(row,col)
           end do
        end do
     end do
  end do


  ! SU is  a 2x2 block matrix.
  !
  ! SU = [SU11 SU12
  !       SU21 SU22]
  !
  ! SU11 = \int  (2\mu+\lambda) D_1 \phi^v_{i,j,1} D_1 \phi^u_{k,l,1}
  !            + \mu D_2 \phi^v_{i,j,1} D_2 \phi^u_{k,l,1}
  ! SU12 = \int  \lambda D_1 \phi^v_{i,j,1} D_2 \phi^u_{k,l,2}
  !            + \mu D_2 \phi^v_{i,j,1} D_1 \phi^u_{k,l,2}
  ! SU21 = \int  \mu D_1 \phi^v_{i,j,2} D_2 \phi^u_{k,l,1}
  !            + \lambda D_2 \phi^v_{i,j,2} D_1 \phi^u_{k,l,1}
  ! SU22 = \int  \mu D_1 \phi^v_{i,j,2} D_1 \phi^u_{k,l,2}
  !            + (2\mu+\lambda) D_2 \phi^v_{i,j,2} D_2 \phi^u_{k,l,2}

  qd%SU(:,:) = 0.d0

  ! SU11
  do j = 0,m_v ! 2-dir phi
     do i = 0,m_v ! 1-dir
        row = i+1 + j*(m_v+1)
        do l = 0,m_u ! 2-dir u
           do k = 0,m_u ! 1-dir
              col = k + 1 + l*(m_u+1)
              ! Recall w_1 = r_1 w_r + s_1 w_s
              do iy = 1,nint
                 fint(iy) = sum(weights*&
                      (2.d0*qd%mu_arr(1:nint,iy)+qd%lam_arr(1:nint,iy))*&
                      qd%jac(1:nint,iy)*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%SU(row,col) = sum(weights*fint)
              ! Also, w_y = r_y w_r +s_y w_s
              ! This term is phi_y u_y
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      (qd%mu_arr(1:nint,iy))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%SU(row,col) = qd%SU(row,col) + sum(weights*fint)
           end do
        end do
     end do
  end do

  ! SU12 = \int  \lambda D_1 \phi^v_{i,j,1} D_2 \phi^u_{k,l,2}
  !            + \mu D_2 \phi^v_{i,j,1} D_1 \phi^u_{k,l,2}
  do j = 0,m_v ! 2-dir phi
     do i = 0,m_v ! 1-dir
        row = i+1 + j*(m_v+1)
        do l = 0,m_u ! 2-dir u
           do k = 0,m_u ! 1-dir
              col = (m_u+1)**2 + k + 1 + l*(m_u+1)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      (qd%lam_arr(1:nint,iy))*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%SU(row,col) = sum(weights*fint)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      (qd%mu_arr(1:nint,iy))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%SU(row,col) = qd%SU(row,col) + sum(weights*fint)
           end do
        end do
     end do
  end do


  ! SU21 = \int  \mu D_1 \phi^v_{i,j,2} D_2 \phi^u_{k,l,1}
  !            + \lambda D_2 \phi^v_{i,j,2} D_1 \phi^u_{k,l,1}
  do j = 0,m_v ! 2-dir phi
     do i = 0,m_v ! 1-dir
        row =(m_v+1)**2 + i+1 + j*(m_v+1)
        do l = 0,m_u ! 2-dir u
           do k = 0,m_u ! 1-dir
              col = k + 1 + l*(m_u+1)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      (qd%mu_arr(1:nint,iy))*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%SU(row,col) = sum(weights*fint)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      (qd%lam_arr(1:nint,iy))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%SU(row,col) = qd%SU(row,col) +sum(weights*fint)
           end do
        end do
     end do
  end do
  ! SU22
  do j = 0,m_v ! 2-dir phi
     do i = 0,m_v ! 1-dir
        row = (m_v+1)**2 + i+1 + j*(m_v+1)
        do l = 0,m_u ! 2-dir u
           do k = 0,m_u ! 1-dir
              col = (m_u+1)**2 + k + 1 + l*(m_u+1)
              ! Recall w_1 = r_1 w_r + s_1 w_s
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      (qd%mu_arr(1:nint,iy))*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%SU(row,col) = sum(weights*fint)
              ! Also, w_y = r_y w_r +s_y w_s
              ! This term is phi_y u_y
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      (2.d0*qd%mu_arr(1:nint,iy)+qd%lam_arr(1:nint,iy))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%SU(row,col) = qd%SU(row,col) + sum(weights*fint)
           end do
        end do
     end do
  end do

  ! MU is  a 2x2 block matrix.
  !
  ! MU = [MU11 MU12
  !       MU21 MU22]
  !
  ! MU11 = \int  (2\mu+\lambda) D_1 \phi^u_{i,j,1} D_1 \phi^u_{k,l,1}
  !            + \mu D_2 \phi^u_{i,j,1} D_2 \phi^u_{k,l,1}
  ! MU12 = \int  \lambda D_1 \phi^u_{i,j,1} D_2 \phi^u_{k,l,2}
  !            + \mu D_2 \phi^u_{i,j,1} D_1 \phi^u_{k,l,2}
  ! MU21 = \int  \mu D_1 \phi^u_{i,j,2} D_2 \phi^u_{k,l,1}
  !            + \lambda D_2 \phi^u_{i,j,2} D_1 \phi^u_{k,l,1}
  ! MU22 = \int  \mu D_1 \phi^u_{i,j,2} D_1 \phi^u_{k,l,2}
  !            + (2\mu+\lambda) D_2 \phi^u_{i,j,2} D_2 \phi^u_{k,l,2}

  qd%MU(:,:) = 0.d0

  ! MU11
  do j = 0,m_u ! 2-dir phi
     do i = 0,m_u ! 1-dir
        row = i+1 + j*(m_u+1)
        do l = 0,m_u ! 2-dir u
           do k = 0,m_u ! 1-dir
              col = k + 1 + l*(m_u+1)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      (2.d0*qd%mu_arr(1:nint,iy)+qd%lam_arr(1:nint,iy))*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%MU(row,col) = sum(weights*fint)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      qd%mu_arr(1:nint,iy)*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%MU(row,col) = qd%MU(row,col) + sum(weights*fint)
           end do
        end do
     end do
  end do

  ! MU12
  do j = 0,m_u ! 2-dir phi
     do i = 0,m_u ! 1-dir
        row = i+1 + j*(m_u+1)
        do l = 0,m_u ! 2-dir u
           do k = 0,m_u ! 1-dir
              col = (m_u+1)**2 + k + 1 + l*(m_u+1)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      qd%lam_arr(1:nint,iy)*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%MU(row,col) = sum(weights*fint)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      qd%mu_arr(1:nint,iy)*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%MU(row,col) = qd%MU(row,col) + sum(weights*fint)
           end do
        end do
     end do
  end do

  ! MU21
  do j = 0,m_u ! 2-dir phi
     do i = 0,m_u ! 1-dir
        row =(m_u+1)**2 + i+1 + j*(m_u+1)
        do l = 0,m_u ! 2-dir u
           do k = 0,m_u ! 1-dir
              col = k + 1 + l*(m_u+1)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      qd%mu_arr(1:nint,iy)*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%MU(row,col) = sum(weights*fint)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      qd%lam_arr(1:nint,iy)*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%MU(row,col) = qd%MU(row,col) +sum(weights*fint)
           end do
        end do
     end do
  end do
  ! MU22
  do j = 0,m_u ! 2-dir phi
     do i = 0,m_u ! 1-dir
        row = (m_u+1)**2 + i+1 + j*(m_u+1)
        do l = 0,m_u ! 2-dir u
           do k = 0,m_u ! 1-dir
              col = (m_u+1)**2 + k + 1 + l*(m_u+1)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      qd%mu_arr(1:nint,iy)*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%MU(row,col) = sum(weights*fint)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      (2.d0*qd%mu_arr(1:nint,iy)+qd%lam_arr(1:nint,iy))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%MU(row,col) = qd%MU(row,col) + sum(weights*fint)
           end do
        end do
     end do
  end do


  ! SV is  a 2x2 block matrix.
  !
  ! SV = [SV11 SV12
  !       SV21 SV22]
  !
  ! SV11 = \int  (2\mu+\lambda) D_1 \phi^u_{i,j,1} D_1 \phi^u_{k,l,1}
  !            + \mu D_2 \phi^u_{i,j,1} D_2 \phi^u_{k,l,1}
  ! SV12 = \int  \lambda D_1 \phi^u_{i,j,1} D_2 \phi^u_{k,l,2}
  !            + \mu D_2 \phi^u_{i,j,1} D_1 \phi^u_{k,l,2}
  ! SV21 = \int  \mu D_1 \phi^u_{i,j,2} D_2 \phi^u_{k,l,1}
  !            + \lambda D_2 \phi^u_{i,j,2} D_1 \phi^u_{k,l,1}
  ! SV22 = \int  \mu D_1 \phi^u_{i,j,2} D_1 \phi^u_{k,l,2}
  !            + (2\mu+\lambda) D_2 \phi^u_{i,j,2} D_2 \phi^u_{k,l,2}

  qd%SV(:,:) = 0.d0
  ! SV11
  do j = 0,m_u ! 2-dir phi
     do i = 0,m_u ! 1-dir
        row = i+1 + j*(m_u+1)
        do l = 0,m_v ! 2-dir u
           do k = 0,m_v ! 1-dir
              col = k + 1 + l*(m_v+1)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      (2.d0*qd%mu_arr(1:nint,iy)+qd%lam_arr(1:nint,iy))*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%SV(row,col) = -sum(weights*fint)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      qd%mu_arr(1:nint,iy)*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%SV(row,col) = qd%SV(row,col) - sum(weights*fint)
           end do
        end do
     end do
  end do

  ! SV12
  do j = 0,m_u ! 2-dir phi
     do i = 0,m_u ! 1-dir
        row = i+1 + j*(m_u+1)
        do l = 0,m_v ! 2-dir u
           do k = 0,m_v ! 1-dir
              col = (m_v+1)**2 + k + 1 + l*(m_v+1)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      qd%lam_arr(1:nint,iy)*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%SV(row,col) = -sum(weights*fint)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      qd%mu_arr(1:nint,iy)*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%SV(row,col) = qd%SV(row,col) - sum(weights*fint)
           end do
        end do
     end do
  end do

  ! SV21
  do j = 0,m_u ! 2-dir phi
     do i = 0,m_u ! 1-dir
        row =(m_u+1)**2 + i+1 + j*(m_u+1)
        do l = 0,m_v ! 2-dir u
           do k = 0,m_v ! 1-dir
              col = k + 1 + l*(m_v+1)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      qd%mu_arr(1:nint,iy)*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%SV(row,col) = -sum(weights*fint)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      qd%lam_arr(1:nint,iy)*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%SV(row,col) = qd%SV(row,col) -sum(weights*fint)
           end do
        end do
     end do
  end do
  ! SU22
  do j = 0,m_u ! 2-dir phi
     do i = 0,m_u ! 1-dir
        row = (m_u+1)**2 + i+1 + j*(m_u+1)
        do l = 0,m_v ! 2-dir u
           do k = 0,m_v ! 1-dir
              col = (m_v+1)**2 + k + 1 + l*(m_v+1)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      qd%mu_arr(1:nint,iy)*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%rx(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sx(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%SV(row,col) = -sum(weights*fint)
              do iy = 1,nint
                 fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                      (2.d0*qd%mu_arr(1:nint,iy)+qd%lam_arr(1:nint,iy))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,i)*chebmat(iy,j)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,i)*dchebmat(iy,j))*&
                      (qd%ry(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                      +qd%sy(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
              end do
              qd%SV(row,col) = qd%SV(row,col) - sum(weights*fint)
           end do
        end do
     end do
  end do

  ! Replace the \phi_u = (1,0)^T
  ! MU11
  j = 0
  i = 0
  row = i+1 + j*(m_u+1)
  qd%extra_eq_1(1) = i
  qd%extra_eq_1(2) = j
  qd%extra_eq_1(3) = 1
  qd%MU(row,:) = 0.d0
  do l = 0,m_u ! 2-dir u
     do k = 0,m_u ! 1-dir
        col = k + 1 + l*(m_u+1)
        do iy = 1,nint
           fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                chebmat(1:nint,k)*chebmat(iy,l))
        end do
        qd%MU(row,col) = sum(weights*fint)
     end do
  end do
  ! SV11
  qd%SV(row,:) = 0.d0
  do l = 0,m_v ! 2-dir u
     do k = 0,m_v ! 1-dir
        col = k + 1 + l*(m_v+1)
        do iy = 1,nint
           fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                chebmat(1:nint,k)*chebmat(iy,l))
        end do
        qd%SV(row,col) = -sum(weights*fint)
     end do
  end do
  ! Replace the \phi_u = (0,1)^T
  ! MU22
  j = 0
  i = 0
  row = (m_u+1)**2 + i+1 + j*(m_u+1)
  qd%extra_eq_2(1) = i
  qd%extra_eq_2(2) = j
  qd%extra_eq_2(3) = 2
  qd%MU(row,:) = 0.d0
  do l = 0,m_u ! 2-dir u
     do k = 0,m_u ! 1-dir
        col = (m_u+1)**2 + k + 1 + l*(m_u+1)
        do iy = 1,nint
           fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                chebmat(1:nint,k)*chebmat(iy,l))
        end do
        qd%MU(row,col) = sum(weights*fint)
     end do
  end do
  ! SV22
  qd%SV(row,:) = 0.d0
  do l = 0,m_v ! 2-dir u
     do k = 0,m_v ! 1-dir
        col = (m_v+1)**2 + k + 1 + l*(m_v+1)
        do iy = 1,nint
           fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                chebmat(1:nint,k)*chebmat(iy,l))
        end do
        qd%SV(row,col) = -sum(weights*fint)
     end do
  end do
  !
  ! Which direction is more orthogonal?
  do iy = 1,nint
     fint(iy) = sum(weights*qd%rx(1:nint,iy)*qd%sy(1:nint,iy))
  end do
  metric12 = sum(weights*fint)
  do iy = 1,nint
     fint(iy) = sum(weights*qd%ry(1:nint,iy)*qd%sx(1:nint,iy))
  end do
  metric21 = sum(weights*fint)
!  write(*,*) "Which metric? ", metric12,metric21
  if( abs(metric12) .ge. abs(metric21) ) then
     ! Replace the \phi_u = (s,0)^T equation by
     ! \int   D_2 \phi^u_{k,l,1} - D_1 \phi^u_{k,l,2}
     j = 1
     i = 0
     row = i+1 + j*(m_u+1)
     qd%extra_eq_3(1) = i
     qd%extra_eq_3(2) = j
     qd%extra_eq_3(3) = 1
  else
     j = 0
     i = 1
     row = i+1 + j*(m_u+1)
     qd%extra_eq_3(1) = i
     qd%extra_eq_3(2) = j
     qd%extra_eq_3(3) = 1
  end if
  qd%MU(row,:) = 0.d0
  do l = 0,m_u ! 2-dir u
     do k = 0,m_u ! 1-dir
        col = k + 1 + l*(m_u+1)
        do iy = 1,nint
           fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                (qd%ry(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                +qd%sy(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
        end do
        qd%MU(row,col) = sum(weights*fint)
     end do
  end do
  do l = 0,m_u ! 2-dir u
     do k = 0,m_u ! 1-dir
        col = (m_u+1)**2 + k + 1 + l*(m_u+1)
        do iy = 1,nint
           fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                (qd%rx(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                +qd%sx(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
        end do
        qd%MU(row,col) = -sum(weights*fint)
     end do
  end do
  qd%SV(row,:) = 0.d0
  do l = 0,m_v ! 2-dir u
     do k = 0,m_v ! 1-dir
        col = k + 1 + l*(m_v+1)
        do iy = 1,nint
           fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                (qd%ry(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                +qd%sy(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
        end do
        qd%SV(row,col) = -sum(weights*fint)
     end do
  end do
  do l = 0,m_v
     do k = 0,m_v
        col = (m_v+1)**2 + k + 1 + l*(m_v+1)
        do iy = 1,nint
           fint(iy) = sum(weights*qd%jac(1:nint,iy)*&
                (qd%rx(1:nint,iy)*dchebmat(1:nint,k)*chebmat(iy,l)&
                +qd%sx(1:nint,iy)*chebmat(1:nint,k)*dchebmat(iy,l)))
        end do
        qd%SV(row,col) = sum(weights*fint)
     end do
  end do

end subroutine assemble
