#!/usr/apps/bin/perl
#
# perl program to run convergence test for the harmonic vibration in a cavity
use POSIX;

# Here is the generic file
$cmdFile="./problemsetup_cavity_DKM_example.f90.T";

@upfluxlist = (".true.",".false.");

$cfl = "2.5";
for( $i=0; $i<2; $i++ ){
  for( $j=0; $j<2; $j++ ){
    $upflux = $upfluxlist[$j];
    for( $q=1; $q <= 4; $q = $q+1){
      for( $g=2; $g <= 50; $g = $g+4){
	
	$qu=$q+1;
	$qv=$q+$i;
	
	open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
	open(OUTFILE,">./problemsetup_cavity_DKM_example.f90") || die "cannot open file $file!" ;
	# Setup the outfile
	while( <FILE> )
	  {
	    $_ =~ s/\bQQQU\b/$qu/;
	    $_ =~ s/\bQQQV\b/$qv/;
	    $_ =~ s/\bUPFLUX\b/$upflux/;
	    $_ =~ s/\bCCCFFFLLL\b/$cfl/;
	    $_ =~ s/GGG/$g/;
	    print OUTFILE $_;
	  }
	close( OUTFILE );
	close( FILE );
	# Compose the output file
	system("make clean -s ; make -s; rm -rf multivar.* c0* ; ./dg_dath.x");
	system("mv err.txt err-U-$upflux-grid-$g-q-$qu-$qv-.txt");
      }
    }
  }
}

$cfl = "0.25";
for( $i=0; $i<2; $i++ ){
  for( $j=0; $j<2; $j++ ){
    $upflux = $upfluxlist[$j];
    for( $q=5; $q <= 7; $q = $q+1){
      for( $g=2; $g <= 50; $g = $g+4){
	
	$qu=$q+1;
	$qv=$q+$i;
	
	open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
	open(OUTFILE,">./problemsetup_cavity_DKM_example.f90") || die "cannot open file $file!" ;
	# Setup the outfile
	while( <FILE> )
	  {
	    $_ =~ s/\bQQQU\b/$qu/;
	    $_ =~ s/\bQQQV\b/$qv/;
	    $_ =~ s/\bUPFLUX\b/$upflux/;
	    $_ =~ s/\bCCCFFFLLL\b/$cfl/;
	    $_ =~ s/GGG/$g/;
	    print OUTFILE $_;
	  }
	close( OUTFILE );
	close( FILE );
	# Compose the output file
	system("make clean -s ; make -s; rm -rf multivar.* c0* ; ./dg_dath.x");
	system("mv err.txt err-U-$upflux-grid-$g-q-$qu-$qv-.txt");
      }
    }
  }
}
exit;

