#!/usr/apps/bin/perl
#
# perl program to run convergence test for the harmonic vibration in an annulus
use POSIX;

# Here is the generic file
$cmdFile="./problemsetup_stoneley_wave.f90.T";

# Upwind problems
$upflux = '.true.';
# Stuff to converge over
for( $q=1; $q <= 10; $q = $q+1){
  
  $qu=$q;
  $qv=$q;
  
  open(FILE,"$cmdFile") || die "cannot open file $cmdFile!" ;
  open(OUTFILE,">./problemsetup_stoneley_wave.f90") || die "cannot open file $file!" ;
  # Setup the outfile
  while( <FILE> )
    {
      $_ =~ s/\bQQQU\b/$qu/;
      $_ =~ s/\bQQQV\b/$qv/;
      $_ =~ s/GGG/$g/;
      $_ =~ s/\bUPFLUX\b/$upflux/;
      print OUTFILE $_;
    }
  close( OUTFILE );
  close( FILE );
  # Compose the output file
  system("make clean -s ; make -s; rm -rf multivar.* c0* ; ./dg_dath.x ; mv err.txt err-g-$g-q-$qu-$qv.txt");
}

exit;
