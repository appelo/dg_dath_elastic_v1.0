module problemSetup
  ! This module solves the elastic wave equation in an annulus.
  !
  use doubleprecision
  implicit none

  integer, parameter :: nvar = 2
  integer, parameter :: q_u = 3
  integer, parameter :: q_v = 3
  ! Value v1 and v2

  integer, parameter :: nint = q_u+6
  real(kind = dp) :: bc_al_be(10:99,2,nvar)

  integer, parameter :: nplot = 3
  logical, parameter :: upwind = .false.
  integer, parameter :: rkstages = 4
  logical, parameter :: plot = .true.
  integer, parameter :: plot_freq = 1000

  ! Lame parameters
  real(kind = dp), parameter :: lam_0 = dble( 100.d0 )
  real(kind = dp), parameter :: mu_0  = dble( 1.d0 )

  ! real(kind = dp), parameter :: CFL = 0.1d0/sqrt(2.d0*mu_0+lam_0)/(dble(q_u)+1.5d0)**2
  real(kind = dp), parameter :: CFL = dble( 1.5d0 )/sqrt(2.d0*mu_0+lam_0)/(dble(q_u)+1.5d0)**2
  real(kind=dp), parameter :: rb0  = 0.5d0
  real(kind=dp), parameter :: rb1  = 1.0d0

  real(kind=dp), parameter :: alpha_n =dble( 3.578028273880645d0 )
  real(kind=dp), parameter :: B0  =dble(  2.524281875355463d0 )

  real(kind = dp), parameter :: tend = 2.5d0*acos(-1.d0)/(alpha_n*sqrt(2*mu_0+lam_0))

  ! The Grid
  character(100), parameter :: element_name = 'ann_60_e.txt'
  character(100), parameter :: node_name    = 'ann_60_n.txt'
contains

  subroutine set_bc
    implicit none
    !
    ! This routine is used to set boundary conditions
    ! on boundary curve 10,11,...
    ! The boundary conditions are on the form
    ! \alpha v + \beta \Nabla u\cdot {\bf n} = 0,
    ! with \alpha^2+\beta^2 = 1.0
    !

    ! Dirichlet for all boundaries.
    bc_al_be(:,1,:) = 1.d0
    ! Neumann for curve 10
    bc_al_be(10,1,:) = 0.D0

    bc_al_be(:,2,:) = sqrt(1.d0-bc_al_be(:,1,:)**2)

  end subroutine set_bc

  subroutine compute_bc_forcing(v_bc_force,traction_bc_force,x,y,t,&
       ivar,bc_number,nint)
    use doubleprecision
    implicit none
    integer  :: nint,ivar,bc_number
    real(kind = dp) :: v_bc_force(nint),traction_bc_force(nint),x(nint),y(nint),t

    v_bc_force = 0.d0
    traction_bc_force = 0.d0

  end subroutine compute_bc_forcing


  real(kind = dp) function init_u(x,y,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y,r,j1,y1
    real(kind = dp), external :: DBESJ1
    real(kind = dp), EXTERNAL :: DBESY1
    real(kind = dp), parameter :: pi = acos(-1.d0)
    r = sqrt(x**2+y**2)
    j1 = bessel_j1(alpha_n*r)
    y1 = bessel_y1(alpha_n*r)
    if (ivar .eq. 1) then
       init_u = cos(atan2(y,x))*(j1+B0*y1)
    else
       init_u = sin(atan2(y,x))*(j1+B0*y1)
    end if
    return
  end function init_u

  real(kind = dp) function init_v(x,y,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y
    real(kind = dp), parameter :: pi = acos(-1.d0)
    if (ivar .eq. 1) then
       init_v = 0.d0
    else
       init_v = 0.d0
    end if
    return
  end function init_v

  subroutine pis(xy,s,xy_start,xy_end,curve_type)

    use doublePrecision
    implicit none
    real(kind=dp) :: xy(2),xy_start(2),xy_end(2),s
    real(kind=dp) :: theta_s, theta_e, d_theta,dy
    integer :: curve_type
    real(kind = dp), parameter :: pi = acos(-1.d0)
    ! We push the points where tha angle jumps a little bit.
    if (curve_type .eq. 11 ) then !
       ! Circle of radius 1 and center in (0,0).
       dy = 0.d0
       if(abs(xy_start(2)) .lt. 1.d-13) then
          if(xy_end(2) .gt. 0.d0) then
             dy = 1.d-13
          else
             dy = -1.d-13
          end if
       elseif(abs(xy_end(2)) .lt. 1.d-13) then
          if(xy_start(2) .gt. 0.d0) then
             dy = 1.d-13
          else
             dy = -1.d-13
          end if
       end if
       theta_s = atan2(xy_start(2)+dy,xy_start(1))
       theta_e = atan2(xy_end(2)+dy,xy_end(1))
       d_theta = (theta_e - theta_s)
       xy(1) = rb0*cos(theta_s + 0.5d0*(1.d0 + s)*d_theta)
       xy(2) = rb0*sin(theta_s + 0.5d0*(1.d0 + s)*d_theta)
       ! xy = xy_start + 0.5d0*(1.d0 + s)*(xy_end - xy_start)
    elseif (curve_type .eq. 10 ) then !
       ! Circle of radius 2 and center in (0,0).
       dy = 0.d0
       if(abs(xy_start(2)) .lt. 1.d-13) then
          if(xy_end(2) .gt. 0.d0) then
             dy = 1.d-13
          else
             dy = -1.d-13
          end if
       elseif(abs(xy_end(2)) .lt. 1.d-13) then
          if(xy_start(2) .gt. 0.d0) then
             dy = 1.d-13
          else
             dy = -1.d-13
          end if
       end if
       theta_s = atan2(xy_start(2)+dy,xy_start(1))
       theta_e = atan2(xy_end(2)+dy,xy_end(1))
       d_theta = (theta_e - theta_s)
       xy(1) = rb1*cos(theta_s + 0.5d0*(1.d0 + s)*d_theta)
       xy(2) = rb1*sin(theta_s + 0.5d0*(1.d0 + s)*d_theta)
    else
       ! Straight line for internal boundaries, used for the mapping of
       ! curved elements.
       xy = xy_start + 0.5d0*(1.d0 + s)*(xy_end - xy_start)
    end if
  end subroutine pis

end module problemSetup
