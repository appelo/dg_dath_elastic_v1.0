module silooutput
contains
  
  subroutine print_solution(qds,uplot,vplot,xplot,yplot,chebmat_plot&
       ,q_u,q_v,num_quads,nplot,it,nvar)
    use doublePrecision
    use quad_element
    implicit none
    type(quad) :: qds(num_quads)
    integer :: num_quads, nplot, q_u, q_v, nvar
    real(kind=dp) :: vplot(0:nplot-1,0:nplot-1,num_quads,nvar),&
         uplot(0:nplot-1,0:nplot-1,num_quads,nvar),&
         xplot(0:nplot-1,0:nplot-1,num_quads),&
         yplot(0:nplot-1,0:nplot-1,num_quads)
    real(kind=dp) :: chebmat_plot(0:nplot-1,0:q_u)
    real(kind=dp) :: mc
    integer :: i,l,k,iy,ix,it,ivar

    vplot = 0.d0
    uplot = 0.d0
    do ivar = 1,nvar
       do i = 1,num_quads
          do l = 0,q_u
             do k = 0,q_u
                mc = qds(i)%u(k,l,ivar)
                do iy = 0,nplot-1
                   do ix = 0,nplot-1
                      uplot(ix,iy,i,ivar) = uplot(ix,iy,i,ivar) &
                           + mc*chebmat_plot(ix,k)*chebmat_plot(iy,l)
                   end do
                end do
             end do
          end do
       end do
       do i = 1,num_quads
          do l = 0,q_v
             do k = 0,q_v
                mc = qds(i)%v(k,l,ivar)
                do iy = 0,nplot-1
                   do ix = 0,nplot-1
                      vplot(ix,iy,i,ivar) = vplot(ix,iy,i,ivar) &
                           + mc*chebmat_plot(ix,k)*chebmat_plot(iy,l)
                   end do
                end do
             end do
          end do
       end do
    end do
    call print_to_silo(uplot,vplot,xplot,yplot,nplot,num_quads,it,nvar)
  end subroutine print_solution

  subroutine print_to_silo(u,v,x,y,nplot,num_quads,it,nvar)
    use doublePrecision
    use quad_element
    implicit none
    include "silo_f9x.inc"
    integer       :: num_quads,it,nplot,nvar
    real(kind=dp) :: u(0:nplot-1,0:nplot-1,num_quads,nvar),&
         v(0:nplot-1,0:nplot-1,num_quads,nvar)
    real(kind=dp) :: x(0:nplot-1,0:nplot-1,num_quads),&
         y(0:nplot-1,0:nplot-1,num_quads)
    CHARACTER(24) :: filename
    integer :: dbfile, erro, dims(2), vardims(2), ndims,myid,ierr,ivar
    character(100) :: cmd
    character(2) :: varname

    ndims = 2
    dims(1:2) = nplot
    vardims(1:2) = nplot
    WRITE(cmd,'("mkdir c",I8.8)') it
    call system(cmd)
    do myid = 1,num_quads
       WRITE(filename,'("c",I8.8,"/multivar.",I5.5)') it, myid
       ierr = dbcreate(filename, 24, DB_CLOBBER, DB_LOCAL,"multimesh", 9, DB_PDB, dbfile)
       IF(dbfile.eq.-1) THEN
          write (6,*) "Could not create Silo file!\n"
          stop 100
       END IF
       erro = dbputqm (dbfile, "quadmesh", 8, "x", 1, "y", 1, "z", 1,&
            x(:,:,myid), y(:,:,myid), DB_F77NULL,&
            dims, ndims, DB_DOUBLE, DB_NONCOLLINEAR, DB_F77NULL, ierr)
       do ivar = 1,nvar
          WRITE(varname,'("u",I1.1)') ivar
          erro = dbputqv1(dbfile,varname, 2, "quadmesh", 8, u(:,:,myid,ivar), vardims,&
               ndims, DB_F77NULL, 0, DB_DOUBLE, DB_NODECENT, DB_F77NULL, ierr)
          WRITE(varname,'("v",I1.1)') ivar
          erro = dbputqv1(dbfile,varname, 2, "quadmesh", 8, v(:,:,myid,ivar), vardims,&
               ndims, DB_F77NULL, 0, DB_DOUBLE, DB_NODECENT, DB_F77NULL, ierr)
       end do
       ierr = dbclose(dbfile)
    end do
    call write_master(num_quads,it,nvar)

  end subroutine print_to_silo

  subroutine write_master(num_quads,it,nvar)
    implicit none
    include "silo_f9x.inc"
    integer :: num_quads, it,nvar
    integer :: err, ierr, dbfile, oldlen
    character(17) :: filename
    WRITE(filename,'("multivar.",I8.8)') it
    ! Create a new silo file
    ierr = dbcreate(filename, 17, DB_CLOBBER, DB_LOCAL,&
         "multimesh root", 14, DB_PDB, dbfile)
    IF(dbfile.eq.-1) THEN
       write (6,*) 'Could not create Silo file!\n'
       return
    END IF
    ! Set the maximum string length
    oldlen = dbget2dstrlen()
    err = dbset2dstrlen(34)
    ! Write the multimesh and multivar objects
    call write_multimesh(dbfile,num_quads,it)
    call write_multivar(dbfile,num_quads,it,nvar)
    ! Restore the previous value for maximum string length
    err = dbset2dstrlen(oldlen)
    ! Close the Silo file
    ierr = dbclose(dbfile)
  end subroutine write_master

  subroutine write_multimesh(dbfile,nmesh,it)
    implicit none
    include "silo_f9x.inc"
    integer :: err, ierr, dbfile, nmesh, i, it
    character(34) :: meshnames(nmesh)
    integer :: lmeshnames(nmesh)
    integer :: meshtypes(nmesh)
    DO i = 1,nmesh,1
       WRITE(meshnames(i),'("c",I8.8,"/multivar.",I5.5, ":quadmesh ")') it,i
       meshtypes(i)  = DB_QUAD_CURV
    END DO
    lmeshnames = 33
    err = dbputmmesh(dbfile, "quadmesh", 8, nmesh, meshnames,lmeshnames,&
         meshtypes, DB_F77NULL, ierr)
  end subroutine write_multimesh

  subroutine write_multivar(dbfile,nmesh,it,nvar)
    implicit none
    include "silo_f9x.inc"
    integer :: err, ierr, dbfile, nmesh, i , it,nvar
    character(34) :: varnames(nmesh)
    integer :: lvarnames(nmesh)
    integer :: vartypes(nmesh)
    lvarnames = 27

    DO i = 1,nmesh
       WRITE(varnames(i),'("c",I8.8,"/multivar.",I5.5, ":u1", "      ")') it, i
       vartypes(i)  = DB_QUADVAR
    END DO
    err = dbputmvar(dbfile, "u1", 2, nmesh, varnames, lvarnames,&
         vartypes, DB_F77NULL, ierr)
    DO i = 1,nmesh
       WRITE(varnames(i),'("c",I8.8,"/multivar.",I5.5, ":v1", "      ")') it, i
       vartypes(i)  = DB_QUADVAR
    END DO
    err = dbputmvar(dbfile, "v1", 2, nmesh, varnames, lvarnames,&
         vartypes, DB_F77NULL, ierr)
    DO i = 1,nmesh
       WRITE(varnames(i),'("c",I8.8,"/multivar.",I5.5, ":u2", "      ")') it, i
       vartypes(i)  = DB_QUADVAR
    END DO
    err = dbputmvar(dbfile, "u2", 2, nmesh, varnames, lvarnames,&
         vartypes, DB_F77NULL, ierr)
    DO i = 1,nmesh
       WRITE(varnames(i),'("c",I8.8,"/multivar.",I5.5, ":v2", "      ")') it, i
       vartypes(i)  = DB_QUADVAR
    END DO
    err = dbputmvar(dbfile, "v2", 2, nmesh, varnames, lvarnames,&
         vartypes, DB_F77NULL, ierr)
  end subroutine write_multivar
end module silooutput
