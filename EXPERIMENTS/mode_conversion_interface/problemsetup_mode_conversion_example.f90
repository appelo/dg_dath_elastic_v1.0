module problemSetup
  ! This module solves the elastic wave equation
  !
  use doublePrecision
  implicit none
  integer, parameter :: nvar = 2
  integer, parameter :: q_u = 9
  integer, parameter :: q_v = 9

  real(kind = dp), parameter :: pi = acos(-1.d0)
  real(kind = dp), parameter :: omega  = 4.d0*pi
!!$  real(kind = dp), parameter :: alpha_ip  = pi/4.d0
!!$  real(kind = dp), parameter :: mu_a  = 1.d0
!!$  real(kind = dp), parameter :: lam_a = 2.d0
!!$  real(kind = dp), parameter :: rho_a = 1.d0
!!$  real(kind = dp), parameter :: mu_b  = 1.d0
!!$  real(kind = dp), parameter :: lam_b = 1.d0
!!$  real(kind = dp), parameter :: rho_b = 1.d0
!!$  real(kind = dp), parameter :: arp = -1.270166537925831d-01
!!$  real(kind = dp), parameter :: ars = 0.d0
!!$  real(kind = dp), parameter :: atp = 1.008034339861825d0
!!$  real(kind = dp), parameter :: ats = 0.d0

!!$  real(kind = dp), parameter :: alpha_ip  = pi/4.d0
!!$  real(kind = dp), parameter :: mu_a  = 2.d0
!!$  real(kind = dp), parameter :: lam_a = 2.d0
!!$  real(kind = dp), parameter :: rho_a = 1.d0
!!$  real(kind = dp), parameter :: mu_b  = 1.d0
!!$  real(kind = dp), parameter :: lam_b = 1.d0
!!$  real(kind = dp), parameter :: rho_b = 1.d0
!!$  real(kind = dp), parameter :: arp = -8.030846841374971d-2
!!$  real(kind = dp), parameter :: ars =  1.431010201091182d-1
!!$  real(kind = dp), parameter :: atp =  1.040322610576486d0
!!$  real(kind = dp), parameter :: ats =  2.723879158804305d-1

!!$  real(kind = dp), parameter :: alpha_ip  = 49.d0*pi/100.d0
!!$  real(kind = dp), parameter :: mu_a  = 2.d0
!!$  real(kind = dp), parameter :: lam_a = 2.d0
!!$  real(kind = dp), parameter :: rho_a = 1.d0
!!$  real(kind = dp), parameter :: mu_b  = 1.d0
!!$  real(kind = dp), parameter :: lam_b = 1.d0
!!$  real(kind = dp), parameter :: rho_b = 1.d0
!!$  real(kind = dp), parameter :: arp = -8.804985297834597d-1
!!$  real(kind = dp), parameter :: ars =  2.171637763122439d-3
!!$  real(kind = dp), parameter :: atp =  1.118869899198324d-1
!!$  real(kind = dp), parameter :: ats =  4.615647945639129d-2

  real(kind = dp), parameter :: alpha_ip  = pi/3.d0
  real(kind = dp), parameter :: mu_a  = 2.d0
  real(kind = dp), parameter :: lam_a = 3.d0
  real(kind = dp), parameter :: rho_a = 1.d0
  real(kind = dp), parameter :: mu_b  = 1.d0
  real(kind = dp), parameter :: lam_b = 1.d0
  real(kind = dp), parameter :: rho_b = 1.d0
  real(kind = dp), parameter :: arp = -0.213706916885143d0
  real(kind = dp), parameter :: ars =  0.107922196466400d0
  real(kind = dp), parameter :: atp =  0.907550556578859d0
  real(kind = dp), parameter :: ats =  0.277359174639644d0
       
  ! Value v1 and v2
  
  integer, parameter :: nint = q_u+6
  real(kind = dp), parameter :: CFL = 0.25d0/sqrt(2.d0*mu_a+lam_a)/(dble(q_u)+1.5d0)**2	
  real(kind = dp), parameter :: tend = 10.0d0
  real(kind = dp) :: bc_al_be(10:99,2,nvar)

  integer, parameter :: nplot = 50
  logical, parameter :: upwind = .true.
  integer, parameter :: rkstages = 4
  logical, parameter :: plot = .true.
  integer, parameter :: plot_freq = 400

  ! A single element or four
  character(100), parameter :: element_name = 'modegrid_16_e.txt'
  character(100), parameter :: node_name    = 'modegrid_16_n.txt'

contains

  subroutine set_bc
    implicit none
    !
    ! This routine is used to set boundary conditions
    ! on boundary curve 10,11,...
    ! The boundary conditions are on the form
    ! \alpha v + \beta \Nabla u\cdot {\bf n} = 0,
    ! with \alpha^2+\beta^2 = 1.0
    !

    bc_al_be(:,1,:) = 1.d0  ! All Dirichlet
    bc_al_be(:,2,:) = sqrt(1.d0-bc_al_be(:,1,:)**2)

  end subroutine set_bc

  subroutine compute_bc_forcing(v_bc_force,traction_bc_force,x,y,t,&
       ivar,bc_number,nint)
    use doubleprecision
    implicit none
    integer  :: nint,ivar,bc_number,i
    real(kind = dp) :: v_bc_force(nint),traction_bc_force(nint),x(nint),y(nint),t,v(2)
    
    v_bc_force = 100.d0
    traction_bc_force = 0.d0
    
    if (ivar .eq. 1) then
       do i = 1,nint
          call pmode_conversion_v(v,x(i),y(i),t)
          v_bc_force(i) = v(1)
       end do
    else
       do i = 1,nint
          call pmode_conversion_v(v,x(i),y(i),t)
          v_bc_force(i) = v(2)
       end do
    end if
    
  end subroutine compute_bc_forcing

  real(kind = dp) function init_u(x,y,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y,t,u(2)
    t = 0.d0
    call pmode_conversion_u(u,x,y,t)
    if (ivar .eq. 1) then
       init_u = u(1)
    else
       init_u = u(2)
    end if
    return
  end function init_u

  real(kind = dp) function init_v(x,y,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y,t,v(2)
    t = 0.d0
    call pmode_conversion_v(v,x,y,t)
    if (ivar .eq. 1) then
       init_v = v(1)
    else
       init_v = v(2)
    end if
    return
  end function init_v

  subroutine pis(xy,s,xy_start,xy_end,curve_type)

    use doublePrecision
    implicit none
    real(kind=dp) :: xy(2),xy_start(2),xy_end(2),s
    real(kind=dp) :: theta_s, theta_e
    integer :: curve_type
    ! This problem only has straight lines...

    xy = xy_start + 0.5d0*(1.d0 + s)*(xy_end - xy_start)

  end subroutine pis

  subroutine pmode_conversion_u(u,x,y,t)

    use doublePrecision
    implicit none
    real(kind=dp) :: x,y,t,u(2)
    real(kind=dp) :: Vpa,Vpb,Vsa,Vsb
    real(kind=dp) :: kpa,kpb,ksa,ksb
    real(kind=dp) :: sip,cip,srp,crp,srs,crs,stp,ctp,sts,cts
    real(kind=dp) :: uip(2),urp(2),urs(2),utp(2),uts(2)
    
    Vpa = sqrt((2.d0*mu_a+lam_a)/rho_a)
    Vpb = sqrt((2.d0*mu_b+lam_b)/rho_b)
    Vsa = sqrt(mu_a/rho_a)
    Vsb = sqrt(mu_b/rho_b)
    kpa = omega / Vpa
    kpb = omega / Vpb
    ksa = omega / Vsa
    ksb = omega / Vsb

    sip = sin(alpha_ip)
    cip = sqrt(1-sip**2)

    srp = sip/Vpa*Vpa
    crp = sqrt(1.d0-srp**2)
    srs = sip/Vpa*Vsa
    crs = sqrt(1.d0-srs**2)
    stp = sip/Vpa*Vpb
    ctp = sqrt(1.d0-stp**2)
    sts = sip/Vpa*Vsb
    cts = sqrt(1.d0-sts**2)

    uip = 1.d0*cos(kpa*(x*sip-y*cip)-omega*t)*(/sip,-cip/)
    urp = arp*cos(kpa*(x*srp+y*crp)-omega*t)*(/srp, crp/)
    urs = ars*cos(ksa*(x*srs+y*crs)-omega*t)*(/crs,-srs/)
    utp = atp*cos(kpb*(x*stp-y*ctp)-omega*t)*(/stp,-ctp/)
    uts = ats*cos(ksb*(x*sts-y*cts)-omega*t)*(/cts, sts/)

    if (y.gt.0.d0) then
       u = uip + urp + urs
    else
       u = utp + uts
    end if
    
  end subroutine pmode_conversion_u

  subroutine pmode_conversion_v(v,x,y,t)

    use doublePrecision
    implicit none
    real(kind=dp) :: x,y,t,v(2)
    real(kind=dp) :: Vpa,Vpb,Vsa,Vsb
    real(kind=dp) :: kpa,kpb,ksa,ksb
    real(kind=dp) :: sip,cip,srp,crp,srs,crs,stp,ctp,sts,cts
    real(kind=dp) :: uip(2),urp(2),urs(2),utp(2),uts(2)
    
    Vpa = sqrt((2.d0*mu_a+lam_a)/rho_a)
    Vpb = sqrt((2.d0*mu_b+lam_b)/rho_b)
    Vsa = sqrt(mu_a/rho_a)
    Vsb = sqrt(mu_b/rho_b)
    kpa = omega / Vpa
    kpb = omega / Vpb
    ksa = omega / Vsa
    ksb = omega / Vsb

    sip = sin(alpha_ip)
    cip = sqrt(1.d0-sip**2)

    srp = sip/Vpa*Vpa
    crp = sqrt(1.d0-srp**2)
    srs = sip/Vpa*Vsa
    crs = sqrt(1.d0-srs**2)
    stp = sip/Vpa*Vpb
    ctp = sqrt(1.d0-stp**2)
    sts = sip/Vpa*Vsb
    cts = sqrt(1.d0-sts**2)

    uip = omega*1.d0*sin(kpa*(x*sip-y*cip)-omega*t)*(/sip,-cip/)
    urp = omega*arp*sin(kpa*(x*srp+y*crp)-omega*t)*(/srp, crp/)
    urs = omega*ars*sin(ksa*(x*srs+y*crs)-omega*t)*(/crs,-srs/)
    utp = omega*atp*sin(kpb*(x*stp-y*ctp)-omega*t)*(/stp,-ctp/)
    uts = omega*ats*sin(ksb*(x*sts-y*cts)-omega*t)*(/cts, sts/)

    if (y.gt.0.d0) then
       v = uip + urp + urs
    else
       v = utp + uts
    end if
    
  end subroutine pmode_conversion_v
  
end module problemSetup
