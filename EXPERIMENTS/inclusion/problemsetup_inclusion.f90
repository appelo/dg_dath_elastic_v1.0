module problemSetup
  ! This module solves the elastic wave equation in a square cavity.
  !
  use doublePrecision
  implicit none
  integer, parameter :: nvar = 2
  integer, parameter :: q_u = 7
  integer, parameter :: q_v = 7
  
  ! Value v1 and v2

  integer, parameter :: nint = q_u+6 

    ! Lame parameters
  real(kind = dp), parameter :: lam_0 = 200.d0
  real(kind = dp), parameter :: mu_0  = 100.d0
  real(kind = dp), parameter :: lam_1 = 2.d0
  real(kind = dp), parameter :: mu_1  = 1.d0

  real(kind = dp), parameter :: CFL = 2.0d0/sqrt(3.d0*mu_0+lam_0)/dble(q_u**2)
  real(kind = dp), parameter :: tend = 0.4d0
  real(kind = dp) :: bc_al_be(10:99,2,nvar)

  integer, parameter :: nplot = 40
  logical, parameter :: upwind = .true.
  integer, parameter :: rkstages = 4
  logical, parameter :: plot = .true.
  integer, parameter :: plot_freq = 100
  
  character(100), parameter :: element_name = 'incgrid_32_e.txt'
  character(100), parameter :: node_name    = 'incgrid_32_n.txt'

contains

  subroutine set_bc
    implicit none
    !
    ! This routine is used to set boundary conditions
    ! on boundary curve 10,11,...
    ! The boundary conditions are on the form
    ! \alpha v + \beta \Nabla u\cdot {\bf n} = 0,
    ! with \alpha^2+\beta^2 = 1.0
    !

    bc_al_be(13,1,:) = 0.d0 ! Traction bottom
    bc_al_be(10,1,:) = 0.d0 ! Traction right
    bc_al_be(12,1,:) = 0.d0 ! Traction top
    bc_al_be(11,1,:) = 1.d0 ! Dirichlet left

    bc_al_be(:,2,:) = sqrt(1.d0-bc_al_be(:,1,:)**2)

  end subroutine set_bc
  
  subroutine compute_bc_forcing(v_bc_force,traction_bc_force,x,y,t,&
       ivar,bc_number,nint)
    use doubleprecision
    implicit none
    integer  :: nint,ivar,bc_number
    real(kind = dp) :: v_bc_force(nint),traction_bc_force(nint),x(nint),y(nint),t
    real(kind = dp), parameter :: pi = acos(-1.d0)
    
    v_bc_force = 0.d0
    traction_bc_force = 0.d0

    if ((ivar.eq.1).and.(bc_number.eq.11)) then
       if(t .lt. 0.025d0 ) then
          ! v_bc_force = sin(pi*t/0.025d0)
          v_bc_force = sin(pi*t/0.075d0)
       end if
    end if

  end subroutine compute_bc_forcing

  real(kind = dp) function init_u(x,y,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y
    real(kind = dp), parameter :: pi = acos(-1.d0)
    if (ivar .eq. 1) then
       init_u = 0.d0
    else
       init_u = 0.d0
    end if
    return
  end function init_u

  real(kind = dp) function init_v(x,y,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y
    real(kind = dp), parameter :: pi = acos(-1.d0)
    if (ivar .eq. 1) then
       init_v = 0.d0
    else
       init_v = 0.d0
    end if
    return
  end function init_v

  subroutine pis(xy,s,xy_start,xy_end,curve_type)

    use doublePrecision
    implicit none
    real(kind=dp) :: xy(2),xy_start(2),xy_end(2),s
    real(kind=dp) :: theta_s, theta_e
    integer :: curve_type
    ! This problem only has straight lines...

    xy = xy_start + 0.5d0*(1.d0 + s)*(xy_end - xy_start)

  end subroutine pis

end module problemSetup
