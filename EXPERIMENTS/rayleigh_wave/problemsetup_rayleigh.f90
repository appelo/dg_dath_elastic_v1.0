module problemSetup
  ! This module solves the elastic wave equation 
  !
  use doublePrecision
  implicit none
  integer, parameter :: nvar = 2
  integer, parameter :: q_u = 5
  integer, parameter :: q_v = 5

  real(kind = dp), parameter :: pi = acos(-1.d0)
  real(kind = dp), parameter :: lam_0 = 1.d0
  real(kind = dp), parameter :: mu_0  = 0.0001d0
  real(kind = dp), parameter :: omega_0  = 2.0d0*pi
  real(kind = dp), parameter :: xi_0  = 9.553063507938881e-01
  real(kind = dp), parameter :: cr_0  = sqrt(mu_0)*xi_0

  ! Value v1 and v2

  integer, parameter :: nint = q_u+6
  real(kind = dp), parameter :: CFL = 0.5d0/sqrt(2.d0*mu_0+lam_0)/(dble(q_u)+1.5d0)**2	
  real(kind = dp), parameter :: tend = 1.d0/cr_0
  real(kind = dp) :: bc_al_be(10:99,2,nvar)

  integer, parameter :: nplot = 50
  logical, parameter :: upwind =  .true.
  integer, parameter :: rkstages = 4
  logical, parameter :: plot = .true.
  integer, parameter :: plot_freq = 1000

  ! A single element or four
  character(100), parameter :: element_name = 'raygrid_8_e.txt'
  character(100), parameter :: node_name    = 'raygrid_8_n.txt'

contains

  subroutine set_bc
    implicit none
    !
    ! This routine is used to set boundary conditions
    ! on boundary curve 10,11,...
    ! The boundary conditions are on the form
    ! \alpha v + \beta \Nabla u\cdot {\bf n} = 0,
    ! with \alpha^2+\beta^2 = 1.0
    !

    bc_al_be(:,1,:) = 1.d0  ! All Dirichlet

    ! bc_al_be(10,1,:) = 0.d0  ! All Traction
    bc_al_be(11,1,:) = 0.d0  ! Traction free
    ! bc_al_be(12,1,:) = 0.d0  ! All Traction
    ! bc_al_be(13,1,:) = 0.d0  ! All Traction

    bc_al_be(:,2,:) = sqrt(1.d0-bc_al_be(:,1,:)**2)

  end subroutine set_bc

  subroutine compute_bc_forcing(v_bc_force,traction_bc_force,x,y,t,&
       ivar,bc_number,nint)
    use doubleprecision
    implicit none
    integer  :: nint,ivar,bc_number,i
    real(kind = dp) :: v_bc_force(nint),traction_bc_force(nint),x(nint),y(nint),t
    
    v_bc_force = 0.d0
    traction_bc_force = 0.d0
    if(bc_number.ne.11) then
       if(ivar.eq.1) then
          do i = 1,nint
             v_bc_force(i) = r_wave_vel(x(i),y(i),t,1)
          end do
       else
          do i = 1,nint
             v_bc_force(i) = r_wave_vel(x(i),y(i),t,2)
          end do
       end if
    end if
  end subroutine compute_bc_forcing

  real(kind = dp) function init_u(x,y,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y
    if (ivar .eq. 1) then
       init_u = r_wave_dis(x,y,0.d0,1)
    else
       init_u = r_wave_dis(x,y,0.d0,2)
    end if
    return
  end function init_u

  real(kind = dp) function init_v(x,y,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y
    ! This is actually used for the mms forcing for v!
    if (ivar .eq. 1) then
       init_v = r_wave_vel(x,y,0.d0,1)
    else
       init_v = r_wave_vel(x,y,0.d0,2)
    end if
    return
  end function init_v

  real(kind = dp) function r_wave_dis(x,y,t,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y,t

    if (ivar .eq. 1) then
       r_wave_dis = exp(-omega_0*x*sqrt(1-xi_0**2))&
            *cos(omega_0*(y+cr_0*t))&
            + (0.5d0*xi_0**2-1.d0)&
            *exp(-omega_0*x*sqrt(1-xi_0**2*mu_0/(2.d0*mu_0+lam_0)))&
            *cos(omega_0*(y+cr_0*t))
    else 
       r_wave_dis = exp(-omega_0*x*sqrt(1-xi_0**2))&
            *sqrt(1-xi_0**2)*sin(omega_0*(y+cr_0*t))&
            + (0.5d0*xi_0**2-1.d0)&
            *exp(-omega_0*x*sqrt(1-xi_0**2*mu_0/(2.d0*mu_0+lam_0)))&
            *sin(omega_0*(y+cr_0*t))/sqrt(1-xi_0**2*mu_0/(2.d0*mu_0+lam_0))

    end if
    return
  end function r_wave_dis

  real(kind = dp) function r_wave_vel(x,y,t,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y,t
    
    if (ivar .eq. 1) then
       r_wave_vel = -omega_0*cr_0*(exp(-omega_0*x*sqrt(1-xi_0**2))&
            *sin(omega_0*(y+cr_0*t))&
            + (0.5d0*xi_0**2-1.d0)&
            *exp(-omega_0*x*sqrt(1-xi_0**2*mu_0/(2.d0*mu_0+lam_0)))&
            *sin(omega_0*(y+cr_0*t)))
    else 
       r_wave_vel = omega_0*cr_0*(exp(-omega_0*x*sqrt(1-xi_0**2))&
            *sqrt(1-xi_0**2)*cos(omega_0*(y+cr_0*t))&
            + (0.5d0*xi_0**2-1.d0)&
            *exp(-omega_0*x*sqrt(1-xi_0**2*mu_0/(2.d0*mu_0+lam_0)))&
            *cos(omega_0*(y+cr_0*t))/sqrt(1-xi_0**2*mu_0/(2.d0*mu_0+lam_0)))
       
    end if
    return
  end function r_wave_vel

  subroutine pis(xy,s,xy_start,xy_end,curve_type)

    use doublePrecision
    implicit none
    real(kind=dp) :: xy(2),xy_start(2),xy_end(2),s
    real(kind=dp) :: theta_s, theta_e
    integer :: curve_type
    ! This problem only has straight lines...

    xy = xy_start + 0.5d0*(1.d0 + s)*(xy_end - xy_start)

  end subroutine pis

end module problemSetup
