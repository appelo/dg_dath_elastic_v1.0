module problemSetup
  ! This module solves the elastic wave equation in a square cavity
  ! with manufactured solution according to example 5.2.1 in Duru, Kreiss & Mattsson
  !
  use doublePrecision
  implicit none
  integer, parameter :: nvar = 2
  integer, parameter :: q_u = 8
  integer, parameter :: q_v = 8

  real(kind = dp), parameter :: pi = acos(-1.d0)
  real(kind = dp), parameter :: lam_0 = 1.d0
  real(kind = dp), parameter :: mu_0  = 1.d0
  real(kind = dp), parameter :: omega_0  = 2.d0*pi
  real(kind = dp), parameter :: kx_0  = 2.5d0*pi
  real(kind = dp), parameter :: ky_0  = 2.0d0*pi
  real(kind = dp), parameter :: x_off_0  =  5.d0
  real(kind = dp), parameter :: y_off_0  =-10.d0
  real(kind = dp), parameter :: amp_u1_0 =  1.d0
  real(kind = dp), parameter :: amp_u2_0 = -1.d0

  ! Value v1 and v2

  integer, parameter :: nint = q_u+6
  real(kind = dp), parameter :: CFL = dble( 0.25 )/sqrt(2.d0*mu_0+lam_0)/(dble(q_u)+1.5d0)**2
  real(kind = dp), parameter :: tend = 1.d0
  real(kind = dp) :: bc_al_be(10:99,2,nvar)

  integer, parameter :: nplot = 2
  logical, parameter :: upwind =  .false.
  integer, parameter :: rkstages = 4
  logical, parameter :: plot = .true.
  integer, parameter :: plot_freq = 100

  ! A single element or four
  character(100), parameter :: element_name = '../../GEOMETRY/CART_GRIDS/cart-32-e.txt'
  character(100), parameter :: node_name    = '../../GEOMETRY/CART_GRIDS/cart-32-n.txt'

contains

  subroutine set_bc
    implicit none
    !
    ! This routine is used to set boundary conditions
    ! on boundary curve 10,11,...
    ! The boundary conditions are on the form
    ! \alpha v + \beta \Nabla u\cdot {\bf n} = 0,
    ! with \alpha^2+\beta^2 = 1.0
    !

    bc_al_be(:,1,:) = 1.d0  ! All Dirichlet
    bc_al_be(:,2,:) = sqrt(1.d0-bc_al_be(:,1,:)**2)

  end subroutine set_bc

  subroutine compute_bc_forcing(v_bc_force,traction_bc_force,x,y,t,&
       ivar,bc_number,nint)
    use doubleprecision
    implicit none
    integer  :: nint,ivar,bc_number
    real(kind = dp) :: v_bc_force(nint),traction_bc_force(nint),x(nint),y(nint),t

    traction_bc_force = 0.d0
    if(ivar.eq.1) then
       v_bc_force = -omega_0*sin(omega_0*t)*amp_u1_0*&
            sin(kx_0*x+x_off_0)*sin(ky_0*y+y_off_0)
    else
       v_bc_force = -omega_0*sin(omega_0*t)*amp_u2_0*&
            sin(kx_0*x+x_off_0)*sin(ky_0*y+y_off_0)
    end if
  end subroutine compute_bc_forcing

  real(kind = dp) function init_u(x,y,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y
    if (ivar .eq. 1) then
       init_u = amp_u1_0*sin(kx_0*x+x_off_0)*sin(ky_0*y+y_off_0)
    else
       init_u = amp_u2_0*sin(kx_0*x+x_off_0)*sin(ky_0*y+y_off_0)
    end if
    return
  end function init_u

  real(kind = dp) function init_v(x,y,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y
    ! This is actually used for the mms forcing for v!
    if (ivar .eq. 1) then
       init_v = 0.d0
    else
       init_v = 0.d0
    end if
    return
  end function init_v

  real(kind = dp) function mms_vt(x,y,ivar)
    use doubleprecision
    implicit none
    integer :: ivar
    real(kind = dp) :: x,y,utt,uxx,vxy,vyy,vxx,uxy,uyy,vtt
    ! This is actually used for the mms forcing for v!
    if (ivar .eq. 1) then
       utt = -amp_u1_0*omega_0**2
       uxx = -amp_u1_0*kx_0**2
       uyy = -amp_u1_0*ky_0**2
       vxy =  amp_u2_0*kx_0*ky_0
       mms_vt = (utt-(2.d0*mu_0+lam_0)*uxx-mu_0*uyy)*sin(kx_0*x+x_off_0)*sin(ky_0*y+y_off_0)&
            -(mu_0+lam_0)*vxy*cos(kx_0*x+x_off_0)*cos(ky_0*y+y_off_0)
    else
       vtt = -amp_u2_0*omega_0**2
       vxx = -amp_u2_0*kx_0**2
       vyy = -amp_u2_0*ky_0**2
       uxy =  amp_u1_0*kx_0*ky_0
       mms_vt = (vtt-(2.d0*mu_0+lam_0)*vyy-mu_0*vxx)*sin(kx_0*x+x_off_0)*sin(ky_0*y+y_off_0)&
            -(mu_0+lam_0)*uxy*cos(kx_0*x+x_off_0)*cos(ky_0*y+y_off_0)
    end if
    return
  end function mms_vt

  subroutine pis(xy,s,xy_start,xy_end,curve_type)

    use doublePrecision
    implicit none
    real(kind=dp) :: xy(2),xy_start(2),xy_end(2),s
    real(kind=dp) :: theta_s, theta_e
    integer :: curve_type
    ! This problem only has straight lines...

    xy = xy_start + 0.5d0*(1.d0 + s)*(xy_end - xy_start)

  end subroutine pis

end module problemSetup
